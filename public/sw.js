// Install event - No caching
self.addEventListener('install', event => {
    self.skipWaiting(); // Activate immediately
});

// Activate event - Ensure clients use the new version
self.addEventListener('activate', event => {
    event.waitUntil(self.clients.claim()); // Take control immediately
});

// Fetch event - Always fetch fresh data, no cache
self.addEventListener('fetch', event => {
    event.respondWith(fetch(event.request).catch(() => {
        // Optional: Show offline fallback if request fails
        if (event.request.destination === 'document') {
            return new Response('You are offline', { status: 503, statusText: 'Offline' });
        }
    }));
});




// self.addEventListener('install', event => {
//     event.waitUntil(
//         caches.open('app-cache').then(cache => {
//             return cache.addAll([
//                 '/', // صفحه اصلی
//                 '/assets/css/app.css',
//                 '/assets/js/scripts.js',
//                 '/images/logo.webp',
//             ]);
//         })
//     );
// });
//
// self.addEventListener('fetch', event => {
//     event.respondWith(
//         caches.match(event.request).then(response => {
//             return response || fetch(event.request).then(networkResponse => {
//                 if (event.request.url.startsWith('http')) {
//                     caches.open('app-cache').then(cache => {
//                         cache.put(event.request, networkResponse.clone());
//                     });
//                 }
//                 return networkResponse;
//             });
//         }).catch(() => {
//             if (event.request.destination === 'document') {
//                 return caches.match('/');
//             }
//         })
//     );
// });
//
