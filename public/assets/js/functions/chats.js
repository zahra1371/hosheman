let imageUrl = ''
let fileType = ''

$(document).ready(function () {
    const locale = $('#locale').val()
    scrollConversationAreaToBottom()

    $('#fileUpload').on('change', function () {
        const allowedTypes = [
            "image/png",
            "image/jpeg",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.ms-excel",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        ];

        const file = this.files[0];

        if (file) {
            if (!allowedTypes.includes(file.type)) {
                toastr.error(__('invalidFileType', locale));
                this.value = ""; // Clear the selected file
                return false;
            }

            if (file.type.startsWith('image/') && file.size > 10 * 1024 * 1024) {
                toastr.error(__('imageSizeError', locale))
                return;
            } else if (file.size > 20 * 1024 * 1024) {
                toastr.error(__('fileSizeError', locale))
                return;
            }
            const reader = new FileReader();

            // Show the loader
            $('#fileLoader').removeClass('hidden');
            $('#filePreviewContainer').addClass('hidden');

            reader.onload = function (e) {
                imageUrl = e.target.result;
                // Hide the loader
                $('#fileLoader').addClass('hidden');

                // Show the preview container
                $('#filePreviewContainer').removeClass('hidden');

                fileType = file.type;

                // Reset previews
                $('#filePreviewImage').addClass('hidden');
                $('#filePreviewIconContainer').addClass('hidden');
                $('#filePreviewText').removeClass('hidden').text(file.name);

                // Handle image preview
                if (fileType.startsWith('image/')) {
                    $('#filePreviewImage').removeClass('hidden').attr('src', e.target.result);
                } else {
                    // Handle icon preview for non-image files
                    $('#filePreviewIconContainer').removeClass('hidden');

                    // Set colorful icon based on file type
                    const fileIcon = $('#filePreviewIcon');
                    let iconUrl = '';

                    if (fileType === 'application/pdf') {
                        iconUrl = `${window.location.origin}/images/pdf-icon.png`; // Replace with actual PDF icon path
                    } else if (
                        fileType.includes('spreadsheet') ||
                        fileType.includes('excel')
                    ) {
                        iconUrl = `${window.location.origin}/images/excel-icon.png`; // Replace with actual Excel icon path
                    } else if (fileType.includes('wordprocessingml.document') || fileType.includes('msword')) {
                        iconUrl = `${window.location.origin}/images/word-icon.png`; // Replace with actual Word icon path
                    } else {
                        iconUrl = `${window.location.origin}/images/file-icon.png`; // Generic file icon
                    }

                    fileIcon.attr('src', iconUrl);
                }
            };

            reader.onerror = function () {
                // Handle errors (optional)
                $('#fileLoader').addClass('hidden');
                alert('Error loading file.');
            };

            reader.readAsDataURL(file);
        }
    });

// Remove file handler
    $('#removeFile').on('click', function () {
        deleteImage()
    });

    // Handle removing the image
    $('#removeImage').on('click', function () {
        // deleteImage()
    });

});

function sendMessage(image) {
    const chatId = $("#chat_id").val();
    const prompt = $("#prompt").val().trim();
    const file = $('#fileUpload')[0].files[0] ?? null;

    if (!prompt) {
        toastr.error(__('askQuestion', locale));
        return;
    }

    $('#prompt').val("");
    deleteImage()

    let stopBtn = $('#stop_button');
    let generateBtn = $('#send_message_button');

    // User message bubble
    const userBubbleTemplate = $('#chat_user_bubble').html();
    const userBubbleHtml = $('<div>').html(userBubbleTemplate);
    userBubbleHtml.find('.chat-content').html(prompt); // Replace placeholder with user input
    if (file) {
        userBubbleHtml.find('.image-container').removeClass('hidden')
        if (fileType.startsWith('image/')) {
            userBubbleHtml.find('.image-prompt').attr('src', imageUrl); // Replace placeholder with user input
        } else if (fileType.includes('spreadsheet') || fileType.includes('excel')) {
            userBubbleHtml.find('.image-prompt').attr('src', `${window.location.origin}/images/excel-icon.png`);
        } else if (fileType.includes('wordprocessingml.document') || fileType.includes('msword')) {
            userBubbleHtml.find('.image-prompt').attr('src', `${window.location.origin}/images/word-icon.png`);
        } else if (fileType === 'application/pdf') {
            userBubbleHtml.find('.image-prompt').attr('src', `${window.location.origin}/images/pdf-icon.png`);
        } else
            userBubbleHtml.find('.image-prompt').attr('src', `${window.location.origin}/images/icon-icon.png`);

        userBubbleHtml.find('.image-path').html(file.name); // Replace placeholder with user input
    }

    $('.chats-container').append(userBubbleHtml.html());

    // AI message bubble
    const aiBubbleTemplate = $('#chat_ai_bubble').html();
    let aiBubbleHtml = aiBubbleTemplate.replace('{{bot_image}}', `/images/bots/${image}.png`);
    $('.chats-container').append(aiBubbleHtml);

    scrollConversationAreaToBottom();

    // Show Stop button and hide Send Message button
    generateBtn.addClass('hidden');
    stopBtn.removeClass('hidden');

    let chunk = [];
    let messages = [];
    let streaming = true;

    let nIntervId = setInterval(function () {
        if (chunk.length === 0 && !streaming) {
            messages.push({
                role: 'assistant',
                content: $('.chat-content').last().html(),
            });
            if (messages.length >= 6) {
                messages.splice(1, 2);
            }

            generateBtn.prop('disabled', false).removeClass('submitting');
            stopBtn.prop('disabled', true).addClass('hidden');
            generateBtn.removeClass('hidden');

            const ai_response = $('.chat-content').last().text()
            const sanitizedResponse = ai_response
                .replace(/&/g, "&amp;")
                .replace(/'/g, "&#39;")
                .replace(/"/g, "&quot;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;");
            $('.chat-content').last().append(`<br><button
                        class="inline-flex items-center justify-center w-8 mx-2 my-2 rounded-full p-1 bg-slate-200 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white" onclick="return copyText('${sanitizedResponse}')">
                        <svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><clipPath id="clip1248_20193"><rect id="鍥惧眰_1" width="17.052675" height="17.052441" transform="translate(1.000000 1.000000)" fill="white" fill-opacity="0"></rect></clipPath><clipPath id="clip1257_20794"><rect id="复制" width="20.000000" height="20.000000" fill="white" fill-opacity="0"></rect></clipPath></defs><g clip-path="url(#clip1257_20794)"><g clip-path="url(#clip1248_20193)"><path id="path" d="M5.03 14.64C4.77 14.64 4.5 14.62 4.24 14.56C3.98 14.51 3.73 14.43 3.49 14.33C3.24 14.23 3.01 14.1 2.79 13.96C2.57 13.81 2.37 13.64 2.18 13.45C1.99 13.26 1.82 13.05 1.68 12.83C1.53 12.61 1.4 12.37 1.3 12.13C1.2 11.88 1.13 11.63 1.07 11.36C1.02 11.1 1 10.84 1 10.57L1 5.07C1 4.8 1.02 4.54 1.07 4.27C1.13 4.01 1.2 3.76 1.3 3.51C1.4 3.26 1.53 3.03 1.68 2.81C1.82 2.58 1.99 2.38 2.18 2.19C2.37 2 2.57 1.83 2.79 1.68C3.01 1.53 3.24 1.41 3.49 1.31C3.73 1.2 3.98 1.13 4.24 1.07C4.5 1.02 4.77 1 5.03 1L10.49 1C10.75 1 11.01 1.02 11.27 1.07C11.53 1.13 11.78 1.2 12.03 1.31C12.27 1.41 12.51 1.53 12.73 1.68C12.95 1.83 13.15 2 13.34 2.19C13.53 2.38 13.69 2.58 13.84 2.81C13.99 3.03 14.11 3.26 14.21 3.51C14.31 3.76 14.39 4.01 14.44 4.27C14.5 4.54 14.52 4.8 14.52 5.07L12.94 5.07C12.94 4.91 12.92 4.75 12.89 4.58C12.86 4.43 12.81 4.27 12.75 4.12C12.69 3.97 12.61 3.83 12.52 3.69C12.43 3.56 12.33 3.43 12.22 3.32C12.1 3.2 11.98 3.1 11.85 3.01C11.71 2.92 11.57 2.84 11.42 2.78C11.27 2.72 11.12 2.67 10.96 2.64C10.81 2.61 10.65 2.59 10.49 2.59L5.03 2.59C4.87 2.59 4.71 2.61 4.55 2.64C4.4 2.67 4.24 2.72 4.09 2.78C3.95 2.84 3.8 2.92 3.67 3.01C3.54 3.1 3.41 3.2 3.3 3.32C3.18 3.43 3.08 3.56 2.99 3.69C2.9 3.83 2.83 3.97 2.77 4.12C2.71 4.27 2.66 4.43 2.63 4.58C2.6 4.75 2.58 4.91 2.58 5.07L2.58 10.57C2.58 10.73 2.6 10.89 2.63 11.05C2.66 11.21 2.71 11.37 2.77 11.52C2.83 11.67 2.9 11.81 2.99 11.94C3.08 12.08 3.18 12.2 3.3 12.32C3.41 12.43 3.54 12.54 3.67 12.63C3.8 12.72 3.95 12.79 4.09 12.86C4.24 12.92 4.4 12.96 4.55 13C4.71 13.03 4.87 13.04 5.03 13.04L5.03 14.64Z" fill="currentColor" fill-opacity="1.000000" fill-rule="evenodd"></path></g><path id="path" d="M14.75 18.91L9.3 18.91C9.03 18.91 8.77 18.88 8.51 18.83C8.25 18.78 8 18.7 7.75 18.6C7.51 18.49 7.27 18.37 7.05 18.22C6.83 18.07 6.63 17.9 6.44 17.71C6.25 17.52 6.09 17.32 5.94 17.1C5.79 16.87 5.67 16.64 5.57 16.39C5.47 16.14 5.39 15.89 5.34 15.63C5.28 15.37 5.26 15.1 5.26 14.83L5.26 9.33C5.26 9.06 5.28 8.8 5.34 8.54C5.39 8.28 5.47 8.02 5.57 7.77C5.67 7.53 5.79 7.29 5.94 7.07C6.09 6.85 6.25 6.64 6.44 6.45C6.63 6.26 6.83 6.09 7.05 5.95C7.27 5.8 7.51 5.67 7.75 5.57C8 5.47 8.25 5.39 8.51 5.34C8.77 5.29 9.03 5.26 9.3 5.26L14.75 5.26C15.01 5.26 15.28 5.29 15.54 5.34C15.8 5.39 16.05 5.47 16.29 5.57C16.54 5.67 16.77 5.8 16.99 5.95C17.21 6.09 17.41 6.26 17.6 6.45C17.79 6.64 17.96 6.85 18.1 7.07C18.25 7.29 18.37 7.53 18.48 7.77C18.58 8.02 18.65 8.28 18.71 8.54C18.76 8.8 18.78 9.06 18.78 9.33L18.78 14.83C18.78 15.1 18.76 15.37 18.71 15.63C18.65 15.89 18.58 16.14 18.48 16.39C18.37 16.64 18.25 16.87 18.1 17.1C17.96 17.32 17.79 17.52 17.6 17.71C17.41 17.9 17.21 18.07 16.99 18.22C16.77 18.37 16.54 18.49 16.29 18.6C16.05 18.7 15.8 18.78 15.54 18.83C15.28 18.88 15.01 18.91 14.75 18.91ZM9.3 6.86C9.13 6.86 8.97 6.87 8.82 6.91C8.66 6.94 8.51 6.98 8.36 7.05C8.21 7.11 8.07 7.18 7.93 7.28C7.8 7.37 7.68 7.47 7.56 7.58C7.45 7.7 7.35 7.82 7.26 7.96C7.17 8.09 7.09 8.24 7.03 8.38C6.97 8.54 6.92 8.69 6.89 8.85C6.86 9.01 6.84 9.17 6.84 9.33L6.84 14.83C6.84 15 6.86 15.16 6.89 15.32C6.92 15.48 6.97 15.63 7.03 15.78C7.09 15.93 7.17 16.07 7.26 16.21C7.35 16.34 7.45 16.47 7.56 16.58C7.68 16.7 7.8 16.8 7.93 16.89C8.07 16.98 8.21 17.06 8.36 17.12C8.51 17.18 8.66 17.23 8.82 17.26C8.97 17.29 9.13 17.31 9.3 17.31L14.75 17.31C14.91 17.31 15.07 17.29 15.23 17.26C15.38 17.23 15.54 17.18 15.69 17.12C15.83 17.06 15.98 16.98 16.11 16.89C16.24 16.8 16.37 16.7 16.48 16.58C16.59 16.47 16.7 16.34 16.79 16.21C16.87 16.07 16.95 15.93 17.01 15.78C17.07 15.63 17.12 15.48 17.15 15.32C17.18 15.16 17.2 15 17.2 14.83L17.2 9.33C17.2 9.17 17.18 9.01 17.15 8.85C17.12 8.69 17.07 8.54 17.01 8.38C16.95 8.24 16.87 8.09 16.79 7.96C16.7 7.82 16.59 7.7 16.48 7.58C16.37 7.47 16.24 7.37 16.11 7.28C15.98 7.19 15.83 7.11 15.69 7.05C15.54 6.98 15.38 6.94 15.23 6.91C15.07 6.87 14.91 6.86 14.75 6.86L9.3 6.86Z" fill="currentColor" fill-opacity="1.000000" fill-rule="nonzero"></path></g></svg>
                    </button>`)
            scrollConversationAreaToBottom();
            clearInterval(nIntervId);
        }

        const text = chunk.shift();
        if (text) {
            $('.chat-content').removeClass('loader');
            $('.chat-content').last().append(text);
            scrollConversationAreaToBottom();
        }
    }, 20);

    let formData = new FormData();
    formData.append('_token', $("input[name='_token']").val());
    formData.append('chat_id', chatId);
    formData.append('prompt', prompt);
    formData.append('file', file);

    $.ajax({
        url: "/user/chat/send-message",
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data.status == 422) {
                generateBtn.removeClass('hidden');
                stopBtn.addClass('hidden');
                $('.chat-content').removeClass('loader');
                toastr.error(data.errors)
                return false
            }

            const eventSource = new EventSource(
                '/user/chat/send-message?chat_id=' +
                chatId +
                '&message_id=' +
                data
            );

            eventSource.onmessage = function (e) {
                if (e.data === '[DONE]') {
                    generateBtn.removeClass('hidden');
                    stopBtn.addClass('hidden');
                    streaming = false;
                    eventSource.close();
                } else {
                    let txt = e.data;
                    if (txt) {
                        chunk.push(txt.split('/**')[0]);
                    }
                }
            };

            eventSource.onerror = function () {
                eventSource.close();
                clearInterval(nIntervId);
                stopBtn.addClass('hidden');
                generateBtn.removeClass('hidden');
                $('.chat-content').removeClass('loader');
                streaming = false;
                toastr.error(__('serverError', locale))
            };
        },
        error: function (error) {
            generateBtn.removeClass('hidden');
            stopBtn.addClass('hidden');
            $('.chat-content').removeClass('loader');
            if (error.status == 422)
                toastr.error(error.responseJSON.error)
            else
                toastr.error(__('serverError', locale))
        }
    });
}

function deleteImage() {
    $('#filePreviewContainer').addClass('hidden');
    $('#filePreviewImage').attr('src', '');
    $('#filePreviewText').text('');

    // Remove the placeholder text from the textarea
    const promptContent = $('#prompt').val();
    const updatedContent = promptContent.replace(/\[Image Attached:.*\]\n?/, '');
    $('#prompt').val(updatedContent);

    // Clear the file input
    $('#fileUpload').val('');
}

function startNewChat(chat_bot_id, image) {
    let formData = new FormData();
    formData.append('id', chat_bot_id);
    let link = `new-chat/${chat_bot_id}`;

    return $.ajax({
        type: 'get',
        url: link,
        contentType: false,
        processData: false,
        success: function (data) {
            updateChatPage(data, image)
            toastr.success(__('newConversation', locale));
        },
        error: function (data) {
            toastr.error(__('serverError', locale));
        },
    });
}

function updateChatPage(data, image, is_new = true) {
    $('#chat_id').val(data.id)
    let chat_container = $('.chats-container')
    $(chat_container).empty();

    data.messages.forEach(function (message) {
        let image_path = null
        if (message.image_path) {
            image_path = message.image_path.replace('chats/', '');
            image_path = image_path.split('-')[2]
        }
        if (message.input != null) {
            const userBubbleTemplate = document
                .querySelector('#chat_user_bubble')
                .content.cloneNode(true);
            userBubbleTemplate.querySelector('.chat-content').innerHTML =
                message.main_input;
            userBubbleTemplate.querySelector('.image-prompt').src =
                'https://content.hosheman.com/' + message.image_path;
            userBubbleTemplate.querySelector('.image-path').innerHTML =
                image_path;
            chat_container.append(userBubbleTemplate);
        }
        if (message.response != null) {
            const aiBubbleTemplate = document
                .querySelector('#chat_ai_bubble')
                .content.cloneNode(true);
            aiBubbleTemplate.querySelector('.chat-content').classList.remove('loader')
            aiBubbleTemplate.querySelector('.chat-content').innerHTML =
                message.response;
            aiBubbleTemplate.querySelector('#bot_image').src = `${window.location.origin}/images/bots/${image}.png`;
            chat_container.append(aiBubbleTemplate);
        }
    })

    scrollConversationAreaToBottom();

    $('.chat-list div.active').removeClass('active');
    if (is_new) {
        const newChat = `<a href="#" onclick="return openChatAreaContainer(${data.id},'${data.chat_bot.image}');">
                                    <div id="chat_${data.id}"
                                         class="active relative isolate flex cursor-pointer items-center before:absolute before:-inset-2 before:-z-10 before:rounded-md before:transition-all before:duration-300 before:content-[''] hover:before:bg-slate-50 hover:before:dark:bg-slate-800 [&.active]:before:bg-slate-100 [&.active]:before:dark:bg-slate-800">
                                        <img src="${window.location.origin}/images/chat.png" height="40" width="30" alt="">
                                        <div class="ms-4">
                                            <h4 class="line-clamp-1 text-sm text-slate-600 dark:text-slate-200">
                                                ${data.messages[0].response}
                                            </h4>
                                            <div
                                                class="mt-1 text-xs text-slate-500 dark:text-slate-400"> چند لحظه پیش</div>
                                        </div>
                                    </div>
                                </a>`
        $('.chat-list').prepend(newChat);
    } else {
        $(`#chat_${data.id}`).addClass('active')
    }
}

function openChatAreaContainer(chat_id, image) {
    $.ajax({
        url: `/user/chat/get-chat?chat_id=${chat_id}`,
        method: "get",
        success: function (data) {
            updateChatPage(data, image, false)
        },
        error: function (error) {
            toastr.error(error);
        }
    });
}

function scrollConversationAreaToBottom() {
    const el = document.querySelector('.conversation-area');
    if (!el) return;
    el.scrollTo({
        top: el.scrollHeight + 200,
        left: 0
    });
}

function copyText(text) {
    navigator.clipboard.writeText(text).then(() => {
        toastr.success(__('copied', locale));
    }).catch(() => {
        toastr.error(__('copyError', locale));
    });
}
