function convertToPersianLetters(number) {
    const units = [
        "", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه"
    ];
    const tens = [
        "", "ده", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود"
    ];
    const hundreds = [
        "", "صد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد"
    ];
    const thousands = [
        "", "هزار", "میلیون", "میلیارد", "تریلیون"
    ];

    // Split number into groups of three digits
    function splitToGroups(num) {
        const groups = [];
        while (num > 0) {
            groups.unshift(num % 1000);
            num = Math.floor(num / 1000);
        }
        return groups;
    }

    // Convert a three-digit number to Persian
    function convertThreeDigits(num) {
        let result = "";
        const h = Math.floor(num / 100);
        const t = Math.floor((num % 100) / 10);
        const u = num % 10;

        if (h > 0) result += hundreds[h];
        if (t > 1) {
            if (result) result += " و ";
            result += tens[t];
            if (u > 0) result += " و " + units[u];
        } else if (t === 1) {
            // Special case for 11-19
            if (result) result += " و ";
            const teens = [
                "ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده"
            ];
            result += teens[u];
        } else if (u > 0) {
            if (result) result += " و ";
            result += units[u];
        }

        return result.trim();
    }

    // Main function logic
    function convertToWords(num) {
        if (num === 0) return "صفر";
        const groups = splitToGroups(num);
        let result = "";
        for (let i = 0; i < groups.length; i++) {
            const groupValue = groups[i];
            if (groupValue > 0) {
                if (result) result += " و ";
                result += convertThreeDigits(groupValue) + " " + thousands[groups.length - i - 1];
            }
        }
        return result.trim();
    }

    // Add currency suffix
    const words = convertToWords(number);
    return words + " تومان";
}

function getTranslation(word) {
    switch (word) {
        case 'Post Title Generator':
            return "تولید کننده عنوان پست"
        case 'Summarize Text':
            return "خلاصه نویس"
        case 'Product Description':
            return "توضیحات محصول"
        case 'Article Generator':
            return "مقاله نویس"
        case 'Product Name Generator':
            return "تولید کننده نام محصول"
        case 'Testimonial Review':
            return "توصیه نامه"
        case 'Problem Agitate Solution':
            return "حل مشکل"
        case 'Blog Section':
            return "وبلاگ نویس"
        case 'Blog Post Ideas':
            return "ایده وبلاگ"
        case 'Blog Intros':
            return "مقدمه وبلاگ"
        case 'Blog Conclusion':
            return "نتیجه وبلاگ"
        case 'Facebook Ads':
            return "تبلیغات فیسبوک"
        case 'Youtube Video Description':
            return "کپشن ویدئوی یوتیوب"
        case 'Youtube Video Title':
            return "عنوان برای یوتیوب"
        case 'Youtube Video Tag':
            return "تگ برای یوتیوب"
        case 'Instagram Captions':
            return "کپشن اینستاگرام"
        case 'Instagram Hashtags':
            return "هشتگ اینستاگرام"
        case 'Social Media Post Tweet':
            return "توییت پست رسانه های اجتماعی"
        case 'Social Media Post Business':
            return "متن رسانه های اجتماعی"
        case 'Facebook Headlines':
            return "تولید کننده عنوان پست"
        case 'Google Ads Headlines':
            return "تولید کننده عنوان پست"
        case 'Google Ads Description':
            return "تولید کننده عنوان پست"
        case 'Paragraph Generator':
            return "تولیدکننده پاراگراف"
        case 'Pros & Cons':
            return "جوانب مثبت و منفی"
        case 'Meta Description':
            return "توضیحات متا"
        case 'FAQ Generator (All Datas)':
            return "ساخت سوالات متداو"
        case 'Email Generator':
            return "ساخت متن ایمیل"
        case 'Email Answer Generator':
            return "ساخت پاسخ ایمیل"
        case 'Newsletter Generator':
            return "تولید کننده خبرنامه"
        case 'Grammar Correction':
            return "تصحیح گرامر"
        case 'TL;DR Summarization':
            return "خلاصه نویس حرفه ای"
        case 'AI Image Generator':
            return "ساخت عکس"
        case 'File Analyzer':
            return "تجزیه و تحلیل فایل"
        case 'Custom Generation':
            return "تولید کننده عنوان پست"
        case 'AI Speech to Text':
            return "تبدیل صدا به متن"
        case 'AI Web Chat':
            return "چت وب"
        case 'AI Video':
            return "ساخت ویدئو"
        case 'AI Code Generator':
            return "کدنویس"
        case 'AI Article Wizard Generator':
            return "مقاله نویس حرفه ای"
        case 'AI Vision':
            return "هوش مصنوعی دیداری"
        case 'AI Voiceover':
            return "صداگذاری"
        case 'AI YouTube':
            return "یوتیوبر"
        case 'Instagram Story':
            return "استوری اینستاگرام"
        case 'AI RSS':
            return "AI RSS"
        case 'Instagram Reel':
            return "ریلز اینستاگرام"
        case 'Chat Image':
            return "چت با عکس"
        case 'AI ReWriter':
            return "تربات بازنویس"
        case 'Invalid file type. Please upload a valid image or document.':
            return 'فرمت فایل نامعتبر است. لطفا یک فایل معتبر آپلود کنید.(فایل png,jpeg,word و excel قابل قبول است.)';
    }
}

function copyText(text, index) {
    const locale = $('#locale').val();

    let isiOSDevice = navigator.userAgent.match(/ipad|ipod|iphone/i)
    if (isiOSDevice) {
        let input1 = $('<input>').attr({
            type: 'hidden',
            value: text,
        }).appendTo('.copy')

        let input = input1[0]
        let editable = input.contentEditable
        let readOnly = input.readOnly

        input.contentEditable = true
        input.readOnly = false

        let range = document.createRange()
        range.selectNodeContents(input)

        let selection = window.getSelection()
        selection.removeAllRanges()
        selection.addRange(range)

        input.setSelectionRange(0, 999999)
        input.contentEditable = editable
        input.readOnly = readOnly
        document.execCommand('copy')
        copyToClipBoard(`tooltiptop-${index}`)
    } else {
        let textField = document.createElement('textarea')
        textField.innerText = text
        document.body.appendChild(textField)
        textField.select()
        textField.focus()
        document.execCommand('copy')
        textField.remove()
        $(index).text(__('copied', locale))

        setTimeout(function () {
            $(index).text('کپی لینک')
        }, 1500)

    }
}
