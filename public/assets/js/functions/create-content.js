$(document).ready(function () {
    const locale = $('#locale').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.file').on('change', function () {
        const file = this.files[0]; // Get the selected file for the current input

        if (file) {
            $(this)
                .closest('.lqd-filepicker-label')
                .find('.file-name')
                .empty();
            const fileName = file.name;
            console.log(file.type)
            const sizeInMB = file.size / (1024 * 1024); // Convert size to MB
            if (file.type.includes('audio/') || file.type.includes('video/') || file.type.includes('doc') || file.type.includes('application/msword')) {
                if (sizeInMB > 20) {
                    toastr.error(__('fileSizeError', locale))
                    $(this).val(''); // Clear the file input
                    return false;
                } else {
                    $(this)
                        .closest('.lqd-filepicker-label')
                        .find('.file-name')
                        .text(fileName);
                }
            } else if (file.type.includes('image/')) {
                if (sizeInMB > 10) {
                    toastr.error(__('imageSizeError', locale))
                    $(this).val('');
                    return false;
                } else {
                    $(this)
                        .closest('.lqd-filepicker-label')
                        .find('.file-name')
                        .text(fileName);
                }
            }
        }

    });

    $('.result').delegate('.quality', 'click', function () {
        let data = new FormData()
        data.append('id', $(this).data('id'));
        data.append('quality', $(this).data('quality'));

        $.ajax({
            url: '/user/content/quality',
            type: 'post',
            data,
            processData: false, // Disable processData for FormData
            contentType: false,
            success: function (data) {
                toastr.success(__('userOpinion', locale))
            },
            error: function (error) {
                toastr.error(__('serverError', locale))
            }
        });
    });

    //برای بخش نمونه های ساخت بکگراند
    $('#toggle-button').on('click', function () {
        const additionalItems = $('#additional-items');
        const button = $('#toggle-button');

        if (additionalItems.hasClass('hidden')) {
            additionalItems.hide().removeClass('hidden').fadeIn();  // Fade in the content
            button.text(__('close', locale));
        } else {
            additionalItems.fadeOut(function () {  // Fade out the content, then hide it
                additionalItems.addClass('hidden');
            });
            button.text(__('showMore', locale));
        }
    });
})

let checking = false;
let intervalId = -1;

//آماده سازی فرم برای ارسال درخواست ajax
function createContent(form) {
    // Create a FormData object to handle form data including files
    let data = new FormData(form[0]);

    // Pass action and method as additional data if needed
    let ajaxData = {
        url: form.attr('action'),
        method: form.attr('method'),
        data: data,
        processData: false,
        contentType: false
    };
    ajax(ajaxData)
}

// ارسال درخواست برای سرور
function ajax(form) {
    let action = typeof form === 'object' ? form.url : form.attr('action');
    let method = typeof form === 'object' ? form.method : form.attr('method');
    let data = typeof form === 'object' ? form.data : form.serialize();

    let button = $('.generator_button')
    if (button) {
        $(button).removeClass('bg-blue-600')
        $(button).addClass('bg-blue-200')
        $(button).attr('disabled', true)
        $(button).text(__('pleaseWait', locale))
    }

    $('span[class*="error-"]').text('');
    $('.result-text').text(__('preparing', locale))

    // Detect if the data is FormData
    let isFormData = data instanceof FormData;

    $.ajax({
        url: action,
        type: method,
        data: data,
        processData: !isFormData,
        contentType: isFormData ? false : 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            if (data.status == 500 || data.status == 422) {
                toastr.error(data.errors)
                prepareSendRequestAgain(button)
                $('.result-text').text("")
                return false
            }
            if (data.type === 'audio') {
                $('.content-area').addClass('hidden')
                $('.result').removeClass('hidden')
                typeText('.result', data.post.output, 'description', data.post.output);
                prepareSendRequestAgain(button)
            } else if (data.type === 'image') {
                toastr.success(__('imageIsReady', locale));
                $('.main-image-area').removeClass('hidden');
                $('.waiting-area').addClass('hidden');
                prepareSendRequestAgain(button)
                const url = window.location.href;
                const server = url.split('/')[0];
                const imageContainer = document.querySelector('.results');
                const imageResultTemplate = document.querySelector('#image_result').content
                    .cloneNode(true);
                const download_url = `${server}/user/content/download/${data.post.id}`;

                imageResultTemplate.querySelector('#image-result').setAttribute('data-id', data.post.id);
                imageResultTemplate.querySelector('#image-result-img')
                    .setAttribute('src', `https://content.hosheman.com/${data.post.output}`);
                imageResultTemplate.querySelector('#image_prompt')
                    .textContent = data.post.input;
                imageResultTemplate.querySelector('#view-image')
                    .setAttribute('href', `https://content.hosheman.com/${data.post.output}`);
                imageResultTemplate.querySelector('#image-result-download')
                    .setAttribute('href', download_url);
                imageResultTemplate.querySelector('#image-result-delete')
                    .setAttribute('data-slug', data.post.id);


                $('.main-image').attr('src', `https://content.hosheman.com/${data.post.output}`)
                $('.image-result-download').attr('href', download_url)
                imageContainer.insertBefore(imageResultTemplate, imageContainer
                    .firstChild);
            } else if (data.type === 'video') {
                checking = false;
                intervalId = setInterval(() => checkVideoDone(data.video_id, button), 10000);
            } else if (data.type === 'code') {
                toastr.success(__('codeIsReady', locale));
                $('.result-container').removeClass('hidden');
                $('.content-area').addClass('hidden');
                prepareSendRequestAgain(button)
                const $container = $(".result");
                $container.empty(); // Clear previous content
                typeChunksSequentially(data.chunks, $container, 0);
            } else {
                const chunk = [];
                let streaming = true;
                let result = '';
                const $responseContainer = $('.result');
                $responseContainer.html('');
                // Simulate typing effect
                const typingEffect = setInterval(() => {
                    if (chunk.length === 0 && !streaming) {
                        clearInterval(typingEffect);
                        data.data.post.credits = result.split(/\s+/).filter(word => word.trim().length > 0).length
                        showDateAndWords(data.data.post, result);
                    }
                    const text = chunk.shift();
                    if (text) {
                        result += text;
                        $responseContainer.html(result); // Append to container
                        scrollConversationAreaToBottom()
                    }
                }, 30); // Typing speed in milliseconds

                const eventSource = new EventSource(`/user/content/generate?post_id=${data.data.post.id}`);
                eventSource.addEventListener('data', function (event) {
                    $('.content-area').addClass('hidden')
                    $('.result-container').removeClass('hidden')
                    const data = JSON.parse(event.data);
                    if (data.message !== null) {
                        chunk.push(data.message);
                    }
                });

                // finished eventSource
                eventSource.addEventListener('stop', function (event) {
                    prepareSendRequestAgain(button)
                    streaming = false;
                    eventSource.close();
                });
            }
        },
        error: function (error) {
            prepareSendRequestAgain(button)
            if (error.status == 422)
                toastr.error(error.responseJSON.error)
            else
                toastr.error(__('serverError', locale))
        }
    });
}

//تایپ بخش کدنویسی
function typeChunksSequentially(chunks, container, index) {
    if (index >= chunks.length) return; // Stop if all chunks are processed

    const chunk = chunks[index];
    if (chunk.type === 'description') {
        // Typing effect for description with <br> handling
        typeText(container, chunk.content, 'description', 15, () => {
            typeChunksSequentially(chunks, container, index + 1);
        });
    } else if (chunk.type === 'code') {
        // Typing effect for code with syntax highlighting
        const cleanedCode = chunk.content.replace(/```.*?\n|```/g, '');
        typeText(container, cleanedCode, 'code', 15, () => {
            typeChunksSequentially(chunks, container, index + 1);
        });
    }
}

function typeText(container, content, type, typingSpeed = 15, onComplete = () => {
}) {
    let index = 0;
    let $element;

    if (type === 'description') {
        $element = $("<p></p>").appendTo(container);
    } else if (type === 'code') {
        // Try to detect the language from the code block markers (e.g., ```javascript)
        const languageMatch = content.match(/^```(\w+)/);
        const language = languageMatch ? languageMatch[1] : detectLanguage(content); // Use detectLanguage if no explicit language is provided
        const cleanedCode = content.replace(/```.*?\n|```/g, '');

        // Create a container for the code block
        const $codeContainer = $("<div class='relative'></div>").appendTo(container);

        // Add the copy button at the top right
        $("<button class='absolute top-2 right-2 p-2 bg-slate-700 text-white rounded-md text-sm hover:bg-slate-600'>Copy</button>")
            .appendTo($codeContainer)
            .on('click', () => {
                navigator.clipboard.writeText(cleanedCode).then(() => {
                    toastr.success(__('copied', locale));
                }).catch(() => {
                    toastr.error(__('copyError', locale));
                });
            });

        // Add the code block with "language-plaintext" explicitly
        $element = $("<pre style='padding-top: 3rem' class='rounded-xl overflow-x-auto lg:scrollbar-none bg-slate-900 text-sm' dir='ltr'></pre>")
            .append($(`<code class='language-${language} p-5 overflow-x-auto scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-600'></code>`))
            .appendTo($codeContainer);
    }

    function typeCharacter() {
        if (index < content.length) {
            scrollConversationAreaToBottom()
            if (type === 'description') {
                // Detect <br> tags and variations using regex
                const brMatch = content.substring(index).match(/^<br\s*\/?>/i);
                if (brMatch) {
                    $element.append($('<br>')); // Append a <br> element
                    index += brMatch[0].length; // Move past the matched <br> tag
                } else {
                    $element.append(content.charAt(index)); // Append the next character
                    index++;
                }
            } else if (type === 'code') {
                $element.find('code').text(content.substring(0, index + 1)); // Append characters for code
                index++;
            }
            setTimeout(typeCharacter, typingSpeed);
        } else {
            // Apply syntax highlighting (Prism.js will apply "plaintext" styling)
            if (type === 'code') {
                Prism.highlightElement($element.find('code')[0]);
            }
            onComplete(); // Call the onComplete callback
        }
    }

    typeCharacter();
}

function showDateAndWords(post, result) {
    const sanitizedResponse = result
        .replace(/&/g, "&amp;")
        .replace(/'/g, "&#39;")
        .replace(/"/g, "&quot;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;");
    const metadataHtml = `<div class="flex items-center mt-5">
                                    <p class="text-xs text-slate-500 dark:text-slate-300 words_count">${new Date(post.updated_at).toLocaleString('fa-IR')} - (${post.credits} کلمه)</p>
                                    <button
                        class="inline-flex items-center justify-center w-8 mx-2 rounded-full p-1 bg-slate-200 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white" onclick="return copyText('${sanitizedResponse}')">
                        <svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><clipPath id="clip1248_20193"><rect id="鍥惧眰_1" width="17.052675" height="17.052441" transform="translate(1.000000 1.000000)" fill="white" fill-opacity="0"></rect></clipPath><clipPath id="clip1257_20794"><rect id="复制" width="20.000000" height="20.000000" fill="white" fill-opacity="0"></rect></clipPath></defs><g clip-path="url(#clip1257_20794)"><g clip-path="url(#clip1248_20193)"><path id="path" d="M5.03 14.64C4.77 14.64 4.5 14.62 4.24 14.56C3.98 14.51 3.73 14.43 3.49 14.33C3.24 14.23 3.01 14.1 2.79 13.96C2.57 13.81 2.37 13.64 2.18 13.45C1.99 13.26 1.82 13.05 1.68 12.83C1.53 12.61 1.4 12.37 1.3 12.13C1.2 11.88 1.13 11.63 1.07 11.36C1.02 11.1 1 10.84 1 10.57L1 5.07C1 4.8 1.02 4.54 1.07 4.27C1.13 4.01 1.2 3.76 1.3 3.51C1.4 3.26 1.53 3.03 1.68 2.81C1.82 2.58 1.99 2.38 2.18 2.19C2.37 2 2.57 1.83 2.79 1.68C3.01 1.53 3.24 1.41 3.49 1.31C3.73 1.2 3.98 1.13 4.24 1.07C4.5 1.02 4.77 1 5.03 1L10.49 1C10.75 1 11.01 1.02 11.27 1.07C11.53 1.13 11.78 1.2 12.03 1.31C12.27 1.41 12.51 1.53 12.73 1.68C12.95 1.83 13.15 2 13.34 2.19C13.53 2.38 13.69 2.58 13.84 2.81C13.99 3.03 14.11 3.26 14.21 3.51C14.31 3.76 14.39 4.01 14.44 4.27C14.5 4.54 14.52 4.8 14.52 5.07L12.94 5.07C12.94 4.91 12.92 4.75 12.89 4.58C12.86 4.43 12.81 4.27 12.75 4.12C12.69 3.97 12.61 3.83 12.52 3.69C12.43 3.56 12.33 3.43 12.22 3.32C12.1 3.2 11.98 3.1 11.85 3.01C11.71 2.92 11.57 2.84 11.42 2.78C11.27 2.72 11.12 2.67 10.96 2.64C10.81 2.61 10.65 2.59 10.49 2.59L5.03 2.59C4.87 2.59 4.71 2.61 4.55 2.64C4.4 2.67 4.24 2.72 4.09 2.78C3.95 2.84 3.8 2.92 3.67 3.01C3.54 3.1 3.41 3.2 3.3 3.32C3.18 3.43 3.08 3.56 2.99 3.69C2.9 3.83 2.83 3.97 2.77 4.12C2.71 4.27 2.66 4.43 2.63 4.58C2.6 4.75 2.58 4.91 2.58 5.07L2.58 10.57C2.58 10.73 2.6 10.89 2.63 11.05C2.66 11.21 2.71 11.37 2.77 11.52C2.83 11.67 2.9 11.81 2.99 11.94C3.08 12.08 3.18 12.2 3.3 12.32C3.41 12.43 3.54 12.54 3.67 12.63C3.8 12.72 3.95 12.79 4.09 12.86C4.24 12.92 4.4 12.96 4.55 13C4.71 13.03 4.87 13.04 5.03 13.04L5.03 14.64Z" fill="currentColor" fill-opacity="1.000000" fill-rule="evenodd"></path></g><path id="path" d="M14.75 18.91L9.3 18.91C9.03 18.91 8.77 18.88 8.51 18.83C8.25 18.78 8 18.7 7.75 18.6C7.51 18.49 7.27 18.37 7.05 18.22C6.83 18.07 6.63 17.9 6.44 17.71C6.25 17.52 6.09 17.32 5.94 17.1C5.79 16.87 5.67 16.64 5.57 16.39C5.47 16.14 5.39 15.89 5.34 15.63C5.28 15.37 5.26 15.1 5.26 14.83L5.26 9.33C5.26 9.06 5.28 8.8 5.34 8.54C5.39 8.28 5.47 8.02 5.57 7.77C5.67 7.53 5.79 7.29 5.94 7.07C6.09 6.85 6.25 6.64 6.44 6.45C6.63 6.26 6.83 6.09 7.05 5.95C7.27 5.8 7.51 5.67 7.75 5.57C8 5.47 8.25 5.39 8.51 5.34C8.77 5.29 9.03 5.26 9.3 5.26L14.75 5.26C15.01 5.26 15.28 5.29 15.54 5.34C15.8 5.39 16.05 5.47 16.29 5.57C16.54 5.67 16.77 5.8 16.99 5.95C17.21 6.09 17.41 6.26 17.6 6.45C17.79 6.64 17.96 6.85 18.1 7.07C18.25 7.29 18.37 7.53 18.48 7.77C18.58 8.02 18.65 8.28 18.71 8.54C18.76 8.8 18.78 9.06 18.78 9.33L18.78 14.83C18.78 15.1 18.76 15.37 18.71 15.63C18.65 15.89 18.58 16.14 18.48 16.39C18.37 16.64 18.25 16.87 18.1 17.1C17.96 17.32 17.79 17.52 17.6 17.71C17.41 17.9 17.21 18.07 16.99 18.22C16.77 18.37 16.54 18.49 16.29 18.6C16.05 18.7 15.8 18.78 15.54 18.83C15.28 18.88 15.01 18.91 14.75 18.91ZM9.3 6.86C9.13 6.86 8.97 6.87 8.82 6.91C8.66 6.94 8.51 6.98 8.36 7.05C8.21 7.11 8.07 7.18 7.93 7.28C7.8 7.37 7.68 7.47 7.56 7.58C7.45 7.7 7.35 7.82 7.26 7.96C7.17 8.09 7.09 8.24 7.03 8.38C6.97 8.54 6.92 8.69 6.89 8.85C6.86 9.01 6.84 9.17 6.84 9.33L6.84 14.83C6.84 15 6.86 15.16 6.89 15.32C6.92 15.48 6.97 15.63 7.03 15.78C7.09 15.93 7.17 16.07 7.26 16.21C7.35 16.34 7.45 16.47 7.56 16.58C7.68 16.7 7.8 16.8 7.93 16.89C8.07 16.98 8.21 17.06 8.36 17.12C8.51 17.18 8.66 17.23 8.82 17.26C8.97 17.29 9.13 17.31 9.3 17.31L14.75 17.31C14.91 17.31 15.07 17.29 15.23 17.26C15.38 17.23 15.54 17.18 15.69 17.12C15.83 17.06 15.98 16.98 16.11 16.89C16.24 16.8 16.37 16.7 16.48 16.58C16.59 16.47 16.7 16.34 16.79 16.21C16.87 16.07 16.95 15.93 17.01 15.78C17.07 15.63 17.12 15.48 17.15 15.32C17.18 15.16 17.2 15 17.2 14.83L17.2 9.33C17.2 9.17 17.18 9.01 17.15 8.85C17.12 8.69 17.07 8.54 17.01 8.38C16.95 8.24 16.87 8.09 16.79 7.96C16.7 7.82 16.59 7.7 16.48 7.58C16.37 7.47 16.24 7.37 16.11 7.28C15.98 7.19 15.83 7.11 15.69 7.05C15.54 6.98 15.38 6.94 15.23 6.91C15.07 6.87 14.91 6.86 14.75 6.86L9.3 6.86Z" fill="currentColor" fill-opacity="1.000000" fill-rule="nonzero"></path></g></svg>
                    </button>
                                </div>
                                <div class="flex items-center justify-center bg-slate-100 rounded mt-5">
                                    <p class="text-xs text-slate-500 dark:text-slate-300 words_count">${__('howWasThisAnswer', locale)}</p>
                                    <button data-id="${post.id}" data-quality="is-ok"
                                        class="quality inline-flex items-center justify-center mx-2 rounded-full bg-slate-200 p-1 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path
                                                d="M20.9752 12.1852L20.2361 12.0574L20.9752 12.1852ZM20.2696 16.265L19.5306 16.1371L20.2696 16.265ZM6.93777 20.4771L6.19056 20.5417L6.93777 20.4771ZM6.12561 11.0844L6.87282 11.0198L6.12561 11.0844ZM13.995 5.22142L14.7351 5.34269V5.34269L13.995 5.22142ZM13.3323 9.26598L14.0724 9.38725V9.38725L13.3323 9.26598ZM6.69814 9.67749L6.20855 9.10933H6.20855L6.69814 9.67749ZM8.13688 8.43769L8.62647 9.00585H8.62647L8.13688 8.43769ZM10.5181 4.78374L9.79208 4.59542L10.5181 4.78374ZM10.9938 2.94989L11.7197 3.13821V3.13821L10.9938 2.94989ZM12.6676 2.06435L12.4382 2.77841L12.4382 2.77841L12.6676 2.06435ZM12.8126 2.11093L13.042 1.39687L13.042 1.39687L12.8126 2.11093ZM9.86195 6.46262L10.5235 6.81599V6.81599L9.86195 6.46262ZM13.9047 3.24752L13.1787 3.43584V3.43584L13.9047 3.24752ZM11.6742 2.13239L11.3486 1.45675V1.45675L11.6742 2.13239ZM3.9716 21.4707L3.22439 21.5353L3.9716 21.4707ZM3 10.2342L3.74721 10.1696C3.71261 9.76945 3.36893 9.46758 2.96767 9.4849C2.5664 9.50221 2.25 9.83256 2.25 10.2342H3ZM20.2361 12.0574L19.5306 16.1371L21.0087 16.3928L21.7142 12.313L20.2361 12.0574ZM13.245 21.25H8.59635V22.75H13.245V21.25ZM7.68498 20.4125L6.87282 11.0198L5.3784 11.149L6.19056 20.5417L7.68498 20.4125ZM19.5306 16.1371C19.0238 19.0677 16.3813 21.25 13.245 21.25V22.75C17.0712 22.75 20.3708 20.081 21.0087 16.3928L19.5306 16.1371ZM13.2548 5.10015L12.5921 9.14472L14.0724 9.38725L14.7351 5.34269L13.2548 5.10015ZM7.18773 10.2456L8.62647 9.00585L7.64729 7.86954L6.20855 9.10933L7.18773 10.2456ZM11.244 4.97206L11.7197 3.13821L10.2678 2.76157L9.79208 4.59542L11.244 4.97206ZM12.4382 2.77841L12.5832 2.82498L13.042 1.39687L12.897 1.3503L12.4382 2.77841ZM10.5235 6.81599C10.8354 6.23198 11.0777 5.61339 11.244 4.97206L9.79208 4.59542C9.65573 5.12107 9.45699 5.62893 9.20042 6.10924L10.5235 6.81599ZM12.5832 2.82498C12.8896 2.92342 13.1072 3.16009 13.1787 3.43584L14.6307 3.05921C14.4252 2.26719 13.819 1.64648 13.042 1.39687L12.5832 2.82498ZM11.7197 3.13821C11.7548 3.0032 11.8523 2.87913 11.9998 2.80804L11.3486 1.45675C10.8166 1.71309 10.417 2.18627 10.2678 2.76157L11.7197 3.13821ZM11.9998 2.80804C12.1345 2.74311 12.2931 2.73181 12.4382 2.77841L12.897 1.3503C12.3873 1.18655 11.8312 1.2242 11.3486 1.45675L11.9998 2.80804ZM14.1537 10.9842H19.3348V9.4842H14.1537V10.9842ZM4.71881 21.4061L3.74721 10.1696L2.25279 10.2988L3.22439 21.5353L4.71881 21.4061ZM3.75 21.5127V10.2342H2.25V21.5127H3.75ZM3.22439 21.5353C3.2112 21.3828 3.33146 21.25 3.48671 21.25V22.75C4.21268 22.75 4.78122 22.1279 4.71881 21.4061L3.22439 21.5353ZM14.7351 5.34269C14.8596 4.58256 14.8241 3.80477 14.6307 3.0592L13.1787 3.43584C13.3197 3.97923 13.3456 4.54613 13.2548 5.10016L14.7351 5.34269ZM8.59635 21.25C8.12244 21.25 7.72601 20.887 7.68498 20.4125L6.19056 20.5417C6.29852 21.7902 7.3427 22.75 8.59635 22.75V21.25ZM8.62647 9.00585C9.30632 8.42 10.0392 7.72267 10.5235 6.81599L9.20042 6.10924C8.85404 6.75767 8.3025 7.30493 7.64729 7.86954L8.62647 9.00585ZM21.7142 12.313C21.9695 10.8365 20.8341 9.4842 19.3348 9.4842V10.9842C19.9014 10.9842 20.3332 11.4959 20.2361 12.0574L21.7142 12.313ZM3.48671 21.25C3.63292 21.25 3.75 21.3684 3.75 21.5127H2.25C2.25 22.1953 2.80289 22.75 3.48671 22.75V21.25ZM12.5921 9.14471C12.4344 10.1076 13.1766 10.9842 14.1537 10.9842V9.4842C14.1038 9.4842 14.0639 9.43901 14.0724 9.38725L12.5921 9.14471ZM6.87282 11.0198C6.8474 10.7258 6.96475 10.4378 7.18773 10.2456L6.20855 9.10933C5.62022 9.61631 5.31149 10.3753 5.3784 11.149L6.87282 11.0198Z"
                                                fill="#505d71"/>
                                        </svg>
                                    </button>
                                    <button data-id="${post.id}" data-quality="not-ok"
                                        class="quality inline-flex items-center justify-center mx-2 rounded-full bg-slate-200 p-1 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                fill="none">
                                                <path
                                                    d="M20.9752 11.8148L20.2361 11.9426L20.9752 11.8148ZM20.2696 7.73505L19.5306 7.86285L20.2696 7.73505ZM6.93777 3.52293L6.19056 3.45832L6.93777 3.52293ZM6.12561 12.9156L6.87282 12.9802L6.12561 12.9156ZM13.995 18.7786L14.7351 18.6573V18.6573L13.995 18.7786ZM13.3323 14.734L14.0724 14.6128V14.6128L13.3323 14.734ZM6.69814 14.3225L6.20855 14.8907H6.20855L6.69814 14.3225ZM8.13688 15.5623L8.62647 14.9942H8.62647L8.13688 15.5623ZM10.5181 19.2163L9.79208 19.4046L10.5181 19.2163ZM10.9938 21.0501L11.7197 20.8618V20.8618L10.9938 21.0501ZM12.6676 21.9356L12.4382 21.2216L12.4382 21.2216L12.6676 21.9356ZM12.8126 21.8891L13.042 22.6031L13.042 22.6031L12.8126 21.8891ZM9.86195 17.5374L10.5235 17.184V17.184L9.86195 17.5374ZM13.9047 20.7525L13.1787 20.5642V20.5642L13.9047 20.7525ZM11.6742 21.8676L11.3486 22.5433V22.5433L11.6742 21.8676ZM3.9716 2.52928L3.22439 2.46467L3.9716 2.52928ZM3 13.7658L3.74721 13.8304C3.71261 14.2306 3.36893 14.5324 2.96767 14.5151C2.5664 14.4978 2.25 14.1674 2.25 13.7658H3ZM20.2361 11.9426L19.5306 7.86285L21.0087 7.60724L21.7142 11.687L20.2361 11.9426ZM13.245 2.75H8.59635V1.25H13.245V2.75ZM7.68498 3.58754L6.87282 12.9802L5.3784 12.851L6.19056 3.45832L7.68498 3.58754ZM19.5306 7.86285C19.0238 4.93226 16.3813 2.75 13.245 2.75V1.25C17.0712 1.25 20.3708 3.91895 21.0087 7.60724L19.5306 7.86285ZM13.2548 18.8998L12.5921 14.8553L14.0724 14.6128L14.7351 18.6573L13.2548 18.8998ZM7.18773 13.7544L8.62647 14.9942L7.64729 16.1305L6.20855 14.8907L7.18773 13.7544ZM11.244 19.0279L11.7197 20.8618L10.2678 21.2384L9.79208 19.4046L11.244 19.0279ZM12.4382 21.2216L12.5832 21.175L13.042 22.6031L12.897 22.6497L12.4382 21.2216ZM10.5235 17.184C10.8354 17.768 11.0777 18.3866 11.244 19.0279L9.79208 19.4046C9.65573 18.8789 9.45699 18.3711 9.20042 17.8908L10.5235 17.184ZM12.5832 21.175C12.8896 21.0766 13.1072 20.8399 13.1787 20.5642L14.6307 20.9408C14.4252 21.7328 13.819 22.3535 13.042 22.6031L12.5832 21.175ZM11.7197 20.8618C11.7548 20.9968 11.8523 21.1209 11.9998 21.192L11.3486 22.5433C10.8166 22.2869 10.417 21.8137 10.2678 21.2384L11.7197 20.8618ZM11.9998 21.192C12.1345 21.2569 12.2931 21.2682 12.4382 21.2216L12.897 22.6497C12.3873 22.8135 11.8312 22.7758 11.3486 22.5433L11.9998 21.192ZM14.1537 13.0158H19.3348V14.5158H14.1537V13.0158ZM4.71881 2.59389L3.74721 13.8304L2.25279 13.7012L3.22439 2.46467L4.71881 2.59389ZM3.75 2.48726V13.7658H2.25V2.48726H3.75ZM3.22439 2.46467C3.2112 2.61722 3.33146 2.75 3.48671 2.75V1.25C4.21268 1.25 4.78122 1.8721 4.71881 2.59389L3.22439 2.46467ZM14.7351 18.6573C14.8596 19.4174 14.8241 20.1952 14.6307 20.9408L13.1787 20.5642C13.3197 20.0208 13.3456 19.4539 13.2548 18.8998L14.7351 18.6573ZM8.59635 2.75C8.12244 2.75 7.72601 3.11302 7.68498 3.58754L6.19056 3.45832C6.29852 2.20975 7.3427 1.25 8.59635 1.25V2.75ZM8.62647 14.9942C9.30632 15.58 10.0392 16.2773 10.5235 17.184L9.20042 17.8908C8.85404 17.2423 8.3025 16.6951 7.64729 16.1305L8.62647 14.9942ZM21.7142 11.687C21.9695 13.1635 20.8341 14.5158 19.3348 14.5158V13.0158C19.9014 13.0158 20.3332 12.5041 20.2361 11.9426L21.7142 11.687ZM3.48671 2.75C3.63292 2.75 3.75 2.63156 3.75 2.48726H2.25C2.25 1.80474 2.80289 1.25 3.48671 1.25V2.75ZM12.5921 14.8553C12.4344 13.8924 13.1766 13.0158 14.1537 13.0158V14.5158C14.1038 14.5158 14.0639 14.561 14.0724 14.6128L12.5921 14.8553ZM6.87282 12.9802C6.8474 13.2742 6.96475 13.5622 7.18773 13.7544L6.20855 14.8907C5.62022 14.3837 5.31149 13.6247 5.3784 12.851L6.87282 12.9802Z"
                                                    fill="#505d71"/>
                                            </svg>
                                    </button>
                                </div>`;
    const resultDiv = $('.result');
    if (resultDiv.length) {
        resultDiv.append(metadataHtml).children().hide().fadeIn(500);
    } else {
        console.error('.result div not found in the DOM.');
    }
    scrollConversationAreaToBottom()
}

function scrollConversationAreaToBottom() {
    const el = document.querySelector('.result-container');
    if (!el) return;
    el.scrollTo({
        top: el.scrollHeight + 200,
        left: 0
    });
}

function deleteImage(elm) {
    Swal.fire({
            title: __('warning', locale),
            text: __('sureToDelete', locale),
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: __('itIsOk', locale),
            confirmButtonText: __('yes', locale)
        },
    ).then((result) => {
        if (result.isConfirmed) {
            const url = window.location.href;
            const server = url.split('/')[0];
            const delete_url =
                `${server}/user/content/delete/${$(elm).data('slug')}`;

            $.ajax({
                type: "get",
                url: delete_url,
                success: function () {
                    Swal.fire({
                        title: __('ok', locale),
                        text: __('deleteImage', locale),
                        icon: "success",
                        confirmButtonColor: "#3085d6",
                        confirmButtonText: __('thanks', locale)
                    }).then(() => {
                        location.reload()
                    });
                },
                error: function () {
                    Swal.fire({
                        title: __('sorry', locale),
                        text: __('deleteError', locale),
                        icon: "error",
                        confirmButtonColor: "#3085d6",
                        confirmButtonText: __('thanks', locale)
                    });
                }
            });
        }
    })
}

function copyText(text) {
    navigator.clipboard.writeText(text).then(() => {
        toastr.success(__('copied', locale));
    }).catch(() => {
        toastr.error(__('copyError', locale));
    });
}

function prepareSendRequestAgain(button) {
    $(button).addClass('bg-blue-600')
    $(button).removeClass('bg-blue-200')
    $(button).attr('disabled', false)
    $(button).text(__('letsGo', locale))
}

// Function to guess the language dynamically
function detectLanguage(code) {
    // Simple keyword-based detection for common languages
    if (/function|const|let|var|=>/.test(code)) return 'javascript';
    if (/def|print|import|self|lambda/.test(code)) return 'python';
    if (/class|public|static|void|System\./.test(code)) return 'java';
    if (/<html|<div|<\/|class=/.test(code)) return 'html';
    if (/#include|int\s+main|std::|cout|cin/.test(code)) return 'cpp';
    if (/SELECT|INSERT|UPDATE|DELETE|FROM|WHERE/.test(code)) return 'sql';
    if (/echo|->|::|\$[a-zA-Z_]+|function|namespace|new\s+[A-Za-z_]+/.test(code)) return 'php'; // Enhanced PHP detection
    if (/def\s+[a-zA-Z_]+|end|puts|yield/.test(code)) return 'ruby';
    if (/using\s+System|Console\.WriteLine|namespace|public\s+class/.test(code)) return 'csharp';
    if (/fmt\.Print|package\s+main|func\s+main/.test(code)) return 'go';
    if (/let\s+\w+|func\s+|import\s+Foundation/.test(code)) return 'swift';
    if (/fun\s+\w+|val\s+|var\s+|class\s+[A-Za-z]+/.test(code)) return 'kotlin';
    if (/ggplot|library\(|<-|print\(/.test(code)) return 'r';
    if (/#!\/bin\/bash|echo\s+|grep|awk|sed/.test(code)) return 'bash';

    // Default to plaintext if no match is found
    return 'plaintext';
}

function checkVideoDone(videoId, button) {
    if (checking) return;
    checking = true;

    let formData = new FormData();
    formData.append('id', videoId);

    $.ajax({
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        url: "/user/video-check",
        data: formData,
        contentType: false,
        processData: false,
        success: function (res) {
            checking = false;
            if (res.status === 200) {
                clearInterval(intervalId);
                intervalId = -1;
                toastr.success(__('videoIsReady', locale));
                $('.main-video-area').removeClass('hidden');
                $('.waiting-area').addClass('hidden');
                prepareSendRequestAgain(button)
                const url = window.location.href;
                const server = url.split('/')[0];
                const imageContainer = document.querySelector('.results');
                const videoResultTemplate = document.querySelector('#video_result').content
                    .cloneNode(true);
                const download_url = `${server}/user/content/download/${res.post.id}`;

                videoResultTemplate.querySelector('#video-result').setAttribute('data-id', res.post.id);
                videoResultTemplate.querySelector('#video-result-source')
                    .setAttribute('src', `https://content.hosheman.com/videos/${res.post.output}`);
                // videoResultTemplate.querySelector('#image_prompt')
                //     .textContent = data.post.input;
                videoResultTemplate.querySelector('#view-video')
                    .setAttribute('href', `https://content.hosheman.com/videos/${res.post.output}`);
                videoResultTemplate.querySelector('#video-result-download')
                    .setAttribute('href', download_url);
                videoResultTemplate.querySelector('#video-result-delete')
                    .setAttribute('data-slug', res.post.id);


                $('.main-video').attr('src', `https://content.hosheman.com/videos/${res.post.output}`)
                $('.video-result-download').attr('href', download_url)
                imageContainer.insertBefore(videoResultTemplate, imageContainer
                    .firstChild);
            }
        },
        error: function (data) {
            checking = false;
            clearInterval(intervalId);
            prepareSendRequestAgain(button)
            toastr.error(__('serverError', locale))
        }
    });
}


