const locale=$('#locale').val()

function registerForm(form, button) {
    let data = $(form).serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {})

    let formData = {
        action: form.attr('action'),
        method: form.attr('method'),
        data,
        button: button.attr('id')
    }

    ajax(formData)
}

function loginForm(form, button) {
    let data = $(form).serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {})

    let formData = {
        action: form.attr('action'),
        method: form.attr('method'),
        data,
        formId: $(form).attr('id'),
        button: button.attr('id')
    }

    ajax(formData)
}

function ajaxRequests(form, button, func_name = null) {
    // Create a FormData object to handle form data including files
    let data = new FormData(form[0]);

    // Add additional metadata to the FormData object
    data.append('formId', $(form).attr('id'));
    data.append('buttonId', button.attr('id'));

    // Pass action and method as additional data if needed
    let ajaxData = {
        url: form.attr('action'),
        method: form.attr('method'),
        data: data,
        processData: false,
        contentType: false
    };

    ajax(ajaxData,func_name);
}

function ajax(form, func_name = null, params = {}) {
    let action = typeof form === 'object' ? form.url : form.attr('action');
    let method = typeof form === 'object' ? form.method : form.attr('method');
    let data = typeof form === 'object' ? form.data : form.serialize();
    let button = typeof form === 'object' && form.button ? form.button : null;

    let button_text = $(`#${button}`).text();
    if (button) {
        $(`#${button}`).removeClass('bg-blue-600')
        $(`#${button}`).addClass('bg-blue-200')
        $(`#${button}`).attr('disabled', true)
        $(`#${button}`).text(__('sending',locale))
    }

    $('.help-block').remove();
    $('span[class*="error-"]').text('');

    // Detect if the data is FormData
    let isFormData = data instanceof FormData;

    $.ajax({
        url: action,
        type: method,
        data: data,
        processData: !isFormData, // Disable processData for FormData
        contentType: isFormData ? false : 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (response) {
            if (func_name != null)
                func_name(response, params);

            if (response.status === 200 || response.status === 100) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', true)
                    $(`#${button}`).text(__(button_text,locale))
                }
                Swal.fire({
                    title: __('done',locale),
                    text: response.message,
                    icon: "success",
                    confirmButtonText: __('ok',locale),
                }).then(() => {
                    if (response.reload)
                        location.reload()
                    if (response.url) {
                        if (response.url.includes('http'))
                            window.location.href = response.url
                        else
                            window.location.href = window.location.origin + response.url
                    }
                })

            } else if (response.status === 422) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', false)
                    $(`#${button}`).text(__(button_text,locale))
                }
                showValidationErrors(response.errors)
            } else if (response.status == 500) {
                if (button != null) {
                    $(`#${button}`).removeClass('bg-blue-200')
                    $(`#${button}`).addClass('bg-blue-600')
                    $(`#${button}`).attr('disabled', false)
                    $(`#${button}`).text(__(button_text,locale))
                }
                toastr.error(response.errors);
            } else if (response.status === 300)
                window.location.href = window.location.origin + response.url
        },
        error: function (xhr) {
            if (button != null) {
                $(`#${button}`).removeClass('bg-blue-200')
                $(`#${button}`).addClass('bg-blue-600')
                $(`#${button}`).attr('disabled', false)
                $(`#${button}`).text(__(button_text,locale))
            }
            if (func_name != null)
                func_name(xhr, params);
            Swal.fire({
                title: __('error',locale),
                text: __('serverError',locale),
                icon: "error",
                confirmButtonText: __('ok',locale),
            }).then(() => {
                location.reload()
            })
        }
    });
}

function showValidationErrors(errors, formId) {
    $('.error').html('')
    Object.keys(errors).forEach((key, index) => {
        $(`.error-${key}`).text(errors[key])
    });
}
