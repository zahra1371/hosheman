@extends('layout.landing.app')

@section('title')
{{__('HosheMan')}} | {{__('Terms Of Use')}}
@endsection
@section('content')
    <section class="relative bg-slate-100 py-10 dark:bg-slate-950">
        <div class="container px-3">
            <div class="flex flex-col items-center justify-center">
                <div class="w-full md:w-5/12">
                    <div class="text-center">
                        <h2 class="mb-2 text-3xl font-bold text-slate-700 dark:text-white"> {{__('Terms Of Use')}} </h2>
                        <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                            <li>{{__('Terms Of Use')}}</li>
                            <li class="mt-0.5 inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="h-2 w-2">
                                    <path fill="currentColor" d="M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z" />
                                </svg>
                            </li>
                            <li>
                                <a href="{{route('home')}}" class="text-blue-500 hover:text-blue-700"> {{__('Home')}} </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- container -->
        <div class="absolute bottom-0 start-0 h-[1px] w-full animate-[animateGradient_5s_ease-in-out_2] bg-gradient-to-r from-transparent from-10% via-blue-100 to-transparent to-90% dark:via-blue-950"></div>
    </section><!-- section -->
    <section class="overflow-hidden bg-white dark:bg-slate-900">
        <div class="container px-3">
            <div class="relative rounded-t-xl bg-gradient-to-b from-white to-slate-50 p-6 !pb-0 dark:border-gray-800 dark:from-slate-900 dark:to-slate-950 sm:p-10">
                <div class="-m-4 flex flex-wrap items-center">
                    <div class="w-full p-4">
                        <div>
                            <h1 class="pb-2 text-2xl">{{__('Rules and regulations for using HosheMan site')}}</h1>
                            <p class="mb-6 text-base/7 text-justify text-slate-500 dark:text-slate-300">
                                {{__("HosheMan is an Iranian platform based on AI technology, offering its services to users by leveraging advanced models and global standards. To create a secure and efficient environment, the use of this site's services is subject to the acceptance and adherence to the following rules:")}}
                            </p>
                            <h2 class="mb-2 text-xl text-justify text-slate-700 dark:text-slate-300">
                                {{__('1. Prohibited uses of HosheMan services:')}}
                            </h2>
                            <p class="mb-2 text-base/7 text-justify text-slate-500 dark:text-slate-300">
                                {{__('Users are required to use this platform responsibly. It is forbidden to use HosheMan for the following:')}}
                            </p>
                            <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem" class="mb-6">
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('Preparing instructions for creating drugs or illegal equipment.')}}</li>
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('Teaching or providing methods that lead to the violation of national laws or illegal activities.')}}</li>
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('Producing, publishing, or sharing inappropriate content that violates ethical principles or is contrary to the laws of the country.')}}</li>
                            </ul>
                            <h2 class="mb-2 text-xl text-justify text-slate-700 dark:text-slate-300">
                                {{__('2. Responsibility for the quality and accuracy of responses:')}}
                            </h2>
                            <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem" class="mb-6">
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('HosheMan uses advanced AI models, but the completeness and perfection of responses are not guaranteed.')}}</li>
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('The quality of responses depends on how users phrase their questions. Therefore, users are responsible for the use of the generated information.')}}</li>
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('The technical team at HosheMan is continuously optimizing and improving the quality of services, but it does not accept responsibility for the inaccuracy or insufficiency of the information provided.')}}</li>
                            </ul>

                            <h2 class="mb-2 text-xl text-justify text-slate-700 dark:text-slate-300">
                                {{__('3. Privacy and sending notification messages:')}}
                            </h2>
                            <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem" class="mb-6">
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('User information is confidential at HosheMan and is protected with the highest security standards.')}}</li>
                            </ul>
                            <h2 class="mb-2 text-xl text-justify text-slate-700 dark:text-slate-300">
                                {{__('4. Use of generated content:')}}
                            </h2>
                            <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem" class="mb-6">
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('The use of content generated by HosheMan in digital and offline media is free, but the responsibility for its use lies with the user.')}}</li>
                            </ul>
                            <h2 class="mb-2 text-xl text-justify text-slate-700 dark:text-slate-300">
                                {{__('5. Registration and acceptance of terms:')}}
                            </h2>
                            <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem" class="mb-6">
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('Registering on HosheMan and using the services of this platform constitutes full acceptance of the terms and conditions outlined on this page.')}}</li>
                                <li class="text-base/7 text-justify text-slate-500 dark:text-slate-300">{{__('Users are required to read and fully comply with the rules.')}}</li>
                            </ul>

                            <p class="text-base/7 mb-6 text-justify text-slate-500 dark:text-slate-300">
                                {{__('Thank you for choosing HosheMan!
We are constantly striving to provide advanced services tailored to your needs, delivering a better AI experience.')}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
@endsection
