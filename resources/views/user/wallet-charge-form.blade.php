@extends('layout.landing.app')

@section('title')
    هوش من |  شارژ کیف پول
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                        <li>
                            <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white">
                                <a href="{{route('wallet.charge')}}" class="text-blue-500 hover:text-blue-700">
                                    {{__('wallet charge')}} </a>
                            </h2>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Head -->
            <div class="-m-3 flex flex-wrap">
                <div class="flex-grow-1 w-full md:ms-6 lg:ms-8">
                    <div
                        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap">
                        <div class="w-full lg:w-1/2">
                            <div class="flex h-full w-full flex-col px-6">
                                <small class="lg:hidden pt-2 text-rose-600 text-center">{{__("To connect to the payment gateway, please turn off your VPN.")}}</small>
                                <div
                                    class="flex h-full w-full flex-col justify-center py-10 px-6">
                                    <div class="flex items-center pb-2">
                                        <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('credit increase')}}</h5>
                                    </div>
                                    <form id="payment-form" action="{{route('wallet.payment')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="gateway" value="zibal">
                                        <div class="-my-2 flex flex-wrap">
                                            <div class="py-2 w-full">
                                                <label for="mobile"
                                                       class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('amount')}} </label>
                                                <small class="bg-rose-100 text-rose-600 rounded px-2">
                                                    {{__('The minimum amount for top-up is 50,000 Toman.')}}
                                                </small>
                                                <div class="relative isolate flex w-full">
                                                    <input
                                                        class="z-10 text-center w-full rounded-md border border-slate-200 bg-white py-2 px-3 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                                        value="{{number_format(50000)}}" inputmode="numeric" id="amount" name="amount"
                                                        autocomplete="off"/>
                                                </div>
                                                @error('amount')<span
                                                    class="text-sm text-rose-600">{{ $message }}</span>@enderror
                                                <div>
                                                    <div class="py-4 text-center">
                                                    <span class="rounded-md bg-slate-200 text-slate-600 text-base/7 px-2 prepare">{{__('250,000')}}</span>
                                                    <span class="rounded-md bg-slate-200 text-slate-600 text-base/7 px-2 prepare">{{__('500,000')}}</span>
                                                    <span class="rounded-md bg-slate-200 text-slate-600 text-base/7 px-2 prepare">{{__('1,000,000')}}</span>
                                                    </div>
                                                </div>
                                                <div class="py-4">
                                                    <span
                                                        class="rounded-md bg-slate-200 text-slate-600 text-sm px-2">{{__('in letters:')}}</span>
                                                    <span id="amount-letters" class="text-slate-600 text-sm">{{__('Fifty thousand Toman')}}</span>
                                                </div>
                                                <div class="pb-2">
                                                    <span
                                                        class="rounded-md bg-slate-200 text-slate-600 text-sm px-2">{{__('Payable amount including tax:')}}</span>
                                                    <span id="total-amount" class="text-slate-600 text-sm">55,605</span>
                                                    <span class="text-slate-600 text-sm">{{__('Toman')}}</span>
                                                </div>
                                                <div class="pt-3">
                                                    <button type="submit" id="payment-btn"
                                                            class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                                        {{__('pay')}} </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="w-full lg:w-1/2">
                            <div class="flex h-full w-full flex-col px-6">
                                <div
                                    class="flex h-full w-full flex-col items-center justify-center py-10 px-6 text-center">
                                    <div class="mb-3 flex items-center justify-center">
                                        <h4 class="text-slate-600">{{__('To connect to the payment gateway, please turn off your VPN.')}}</h4>
                                    </div>
                                    <div class="flex items-center justify-center">
                                        <img class="h-32" src="{{asset('images/wallet.png')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
@endsection

@section('scripts')
    <script>
        $('#amount').on('keyup', function () {
            // Remove existing commas and non-numeric characters
            let value = $(this).val().replace(/,/g, '');
            // Add commas using a regex
            let formattedValue = value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            // Update the input field with the formatted value
            $(this).val(formattedValue);
            const amount = value === '' ? 0 : value
            const tax = 11.21;
            const total = parseFloat(amount) + parseFloat(amount * tax / 100)
            $('#total-amount').text(total.toLocaleString())
            $('#amount-letters').empty()
            $('#amount-letters').text(convertToPersianLetters(amount))
        })

        $('.prepare').click(function () {
            // Remove existing commas and non-numeric characters
            let value = $(this).text().replace(/,/g, '');
            // Add commas using a regex
            let formattedValue = value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            // Update the input field with the formatted value
            $('#amount').val(formattedValue);
            const amount = value === '' ? 0 : value
            const tax = 11.21;
            const total = parseFloat(amount) + parseFloat(amount * tax / 100)
            $('#total-amount').text(total.toLocaleString())
            $('#amount-letters').empty()
            $('#amount-letters').text(convertToPersianLetters(amount))
        })
    </script>
@endsection
