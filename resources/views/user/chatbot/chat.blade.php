@extends('layout.landing.app')

@section('title')
    هوش من |  {{__('AI Chat Bot')}}
@endsection
@section('styles')
    <style>
        #prompt:focus {
            border: none !important; /* Remove the border */
            outline: none !important; /* Remove the outline */
            box-shadow: none !important; /* Remove any box shadow */
        }

        .loader {
            border: 4px solid transparent;
            border-radius: 50%;
            border-top: 4px solid #3498db;
            width: 20px;
            height: 20px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }

    </style>
@endsection
@section('content')
    <div class="relative px-1 pt-10 pb-5">
        <div class="container px-3">
            <div
                class="flex-grow-1 relative isolate flex max-h-[calc(100vh-theme(space.52))] min-h-[calc(100vh-theme(space.52))] w-full overflow-hidden rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                <div id="convoAside"
                     class="peer absolute z-20 flex h-full w-72 -translate-x-full flex-col border-e border-slate-200 bg-white duration-300 dark:border-slate-800 dark:bg-slate-950 max-lg:transition-all lg:static lg:h-auto lg:w-1/4 lg:!translate-x-0 rtl:translate-x-full [&.active]:translate-x-0">
                    <div class="flex h-16 items-center border-b border-slate-200 px-6 py-4 dark:border-slate-800">
                        <h3 class="text-lg font-bold text-slate-700 dark:text-white"> {{__('Previous Chats')}} </h3>
                    </div>
                    <div
                        class="h-full max-h-full flex-grow overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                        <div class="grid grid-cols-1 gap-5 chat-list">
                            @php $i=0 @endphp
                            @foreach($previous_chats as $item)
                                @php
                                    $message=count($item->messages)>0?count($item->messages)>1?$item->messages[1]->main_input:$item->messages[0]->response:'';
                                @endphp
                                <a href="#"
                                   onclick="return openChatAreaContainer({{$item->id}},'{{$item->chatBot->image}}');">
                                    <div id="chat_{{$item->id}}"
                                         class="@if(++$i ===1) active @endif relative isolate flex cursor-pointer items-center before:absolute before:-inset-2 before:-z-10 before:rounded-md before:transition-all before:duration-300 before:content-[''] hover:before:bg-slate-50 hover:before:dark:bg-slate-800 [&.active]:before:bg-slate-100 [&.active]:before:dark:bg-slate-800">
                                        <img src="{{asset('images/chat.png')}}" height="40" width="30" alt="">
                                        <div class="ms-4 w-full">
                                            <h4 class="line-clamp-1 text-sm text-slate-600 dark:text-slate-200">
                                                {{$message}}
                                            </h4>
                                            <div
                                                class="mt-1 text-xs text-slate-500 dark:text-slate-400"> {{$item->created_at->diffForHumans()}}</div>
                                        </div>
                                        <img src="{{asset('images/delete.png')}}" class="ms-4" alt="" height="35" width="25" data-id="{{$item->id}}" onclick="deleteDocument(this)">
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="mt-auto px-6 pb-6 pt-4">
                        <button type="button" onclick="startNewChat({{$chatBot->id}},'{{$chatBot->image}}')"
                                class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M2.25 12.76c0 1.6 1.123 2.994 2.707 3.227 1.068.157 2.148.279 3.238.364.466.037.893.281 1.153.671L12 21l2.652-3.978c.26-.39.687-.634 1.153-.67 1.09-.086 2.17-.208 3.238-.365 1.584-.233 2.707-1.626 2.707-3.228V6.741c0-1.602-1.123-2.995-2.707-3.228A48.394 48.394 0 0 0 12 3c-2.392 0-4.744.175-7.043.513C3.373 3.746 2.25 5.14 2.25 6.741v6.018Z"></path>
                            </svg>
                            <span>{{__('New Conversation')}}</span>
                        </button>
                    </div>
                </div>
                <!-- chat-aside -->
                <div data-target="#convoAside"
                     class="class-toggle absolute inset-0 z-10 hidden bg-slate-950 bg-opacity-20 peer-[.active]:block lg:!hidden"></div>
                <!-- chat-aside-overlay -->
                <div class="z-0 flex w-full flex-col lg:w-3/4">
                    <div class="flex h-full flex-col justify-stretch">
                        <div
                            class="h-15 flex justify-between border-b border-slate-200 px-6 py-3 gap-2 dark:border-slate-800">
                            <div class="flex items-center">
                                <div
                                    class="inline-flex h-9 w-9 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-950">
                                    <img src="{{asset("images/bots/$chatBot->image.png")}}" alt="General Bot"/>
                                </div>
                                <div class="ms-3">
                                    <h4 class="line-clamp-1 text-xs font-bold text-slate-600 dark:text-slate-200">
                                        {{__($chatBot->description)}} </h4>
                                </div>
                            </div>
                            <div class="flex items-center">
                                <button type="button" onclick="startNewChat({{$chatBot->id}},'{{$chatBot->image}}')"
                                        class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                         viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M12 5l0 14"></path>
                                        <path d="M5 12l14 0"></path>
                                    </svg>
                                </button>
                                <button
                                    class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">

                                    <svg stroke-width="1.5" class="size-5" xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path d="M21 12a9 9 0 1 0 -9 9"></path>
                                        <path d="M3.6 9h16.8"></path>
                                        <path d="M3.6 15h8.4"></path>
                                        <path d="M11.578 3a17 17 0 0 0 0 18"></path>
                                        <path d="M12.5 3c1.719 2.755 2.5 5.876 2.5 9"></path>
                                        <path d="M18 14v7m-3 -3l3 3l3 -3"></path>
                                    </svg>
                                </button>
                                <button type="button" data-id="{{$chatBot->id}}" onclick="deleteDocument(this)"
                                        class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg stroke-width="1.5" class="size-6" xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor" fill="none"
                                         stroke-linecap="round" stroke-linejoin="round">
                                        <path
                                            d="M9 5h-2a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h3m9 -9v-5a2 2 0 0 0 -2 -2h-2"></path>
                                        <path
                                            d="M13 17v-1a1 1 0 0 1 1 -1h1m3 0h1a1 1 0 0 1 1 1v1m0 3v1a1 1 0 0 1 -1 1h-1m-3 0h-1a1 1 0 0 1 -1 -1v-1"></path>
                                        <path
                                            d="M9 3m0 2a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v0a2 2 0 0 1 -2 2h-2a2 2 0 0 1 -2 -2z"></path>
                                    </svg>
                                </button>
                                <button data-target="#convoAside"
                                        class="class-toggle -my-1 inline-flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-slate-400 transition-all hover:bg-slate-200 hover:text-slate-600 lg:hidden [&.active]:bg-slate-200 [&.active]:text-slate-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5" stroke="currentColor" class="h-5">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                                    </svg><!-- bars-3 - outline - heroicons  -->
                                </button>
                            </div>
                        </div>
                        <div
                            class="conversation-area flex-grow overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                            <div class="chats-container grid grid-cols-1 gap-4">
                                @foreach ($last_chat->messages as $message)
                                    @php
                                        $bot_image=$last_chat->chatBot->image;
                                    @endphp
                                    @if($message->main_input !== null)
                                        <div class="flex-row-reverse flex items-end gap-2">
                                            <div
                                                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-950">
                                                <img src="{{asset('images/nouser.webp')}}" alt="user image"/>
                                            </div>
                                            <div>
                                                @if($message->image_path)
                                                    @php
                                                        $image_path=basename($message->image_path);
                                                        $image_path=explode('-',$image_path)[2];
                                                        $extension=pathinfo($image_path,PATHINFO_EXTENSION);
                                                        $icon=null;
                                                        switch ($extension){
                                                            case 'pdf':
                                                                $icon='pdf-icon.png';
                                                                break;
                                                            case 'xlsx':
                                                            case 'xls':
                                                                $icon='excel-icon.png';
                                                                break;
                                                            case 'docx':
                                                            case 'doc':
                                                                $icon='word-icon.png';
                                                                break;
                                                        }
                                                    @endphp
                                                    <div
                                                        class="flex justify-end mb-1 h-16 flex-shrink-0 overflow-hidden rounded border-2 border-slate-600 max-w-md dark:border-slate-700">
                                                        <small
                                                            class="my-auto ms-1 text-slate-500">{{$image_path}}</small>
                                                        @if($icon)
                                                            <img class="py-2 px-1 w-14"
                                                                 src="{{asset("images/$icon")}}"
                                                                 alt="">
                                                        @else
                                                            <img class="py-2 px-1 w-14"
                                                                 src="https://content.hosheman.com/{{$message->image_path}}"
                                                                 alt="">
                                                        @endif

                                                    </div>
                                                @endif
                                                <div class="bg-blue-50 dark:bg-blue-950  max-w-md rounded-md px-4 py-3">
                                                    <p class="text-sm text-slate-500 dark:text-slate-300">{{$message->main_input}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if($message->response != null)
                                        <div class=" flex items-end gap-2">
                                            <div
                                                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-950">
                                                <img src="{{asset("images/bots/$bot_image.png")}}"
                                                     alt="{{"$last_chat->$last_chat->image robot image"}}"/>
                                            </div>
                                            <div class=" bg-slate-100 dark:bg-slate-900 max-w-md rounded-md px-4 py-3">
                                                <p class="bot-chat-content text-sm text-slate-500 dark:text-slate-300"> {{ $message->response }} </p>
                                                <button
                                                    class="inline-flex items-center justify-center w-8 mx-2 my-2 rounded-full p-1 bg-slate-200 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white"
                                                    onclick="return copyText({{json_encode($message->response)}})">
                                                    <svg viewBox="0 0 20 20" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        <defs>
                                                            <clipPath id="clip1248_20193">
                                                                <rect id="鍥惧眰_1" width="17.052675" height="17.052441"
                                                                      transform="translate(1.000000 1.000000)"
                                                                      fill="white" fill-opacity="0"></rect>
                                                            </clipPath>
                                                            <clipPath id="clip1257_20794">
                                                                <rect id="复制" width="20.000000" height="20.000000"
                                                                      fill="white" fill-opacity="0"></rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g clip-path="url(#clip1257_20794)">
                                                            <g clip-path="url(#clip1248_20193)">
                                                                <path id="path"
                                                                      d="M5.03 14.64C4.77 14.64 4.5 14.62 4.24 14.56C3.98 14.51 3.73 14.43 3.49 14.33C3.24 14.23 3.01 14.1 2.79 13.96C2.57 13.81 2.37 13.64 2.18 13.45C1.99 13.26 1.82 13.05 1.68 12.83C1.53 12.61 1.4 12.37 1.3 12.13C1.2 11.88 1.13 11.63 1.07 11.36C1.02 11.1 1 10.84 1 10.57L1 5.07C1 4.8 1.02 4.54 1.07 4.27C1.13 4.01 1.2 3.76 1.3 3.51C1.4 3.26 1.53 3.03 1.68 2.81C1.82 2.58 1.99 2.38 2.18 2.19C2.37 2 2.57 1.83 2.79 1.68C3.01 1.53 3.24 1.41 3.49 1.31C3.73 1.2 3.98 1.13 4.24 1.07C4.5 1.02 4.77 1 5.03 1L10.49 1C10.75 1 11.01 1.02 11.27 1.07C11.53 1.13 11.78 1.2 12.03 1.31C12.27 1.41 12.51 1.53 12.73 1.68C12.95 1.83 13.15 2 13.34 2.19C13.53 2.38 13.69 2.58 13.84 2.81C13.99 3.03 14.11 3.26 14.21 3.51C14.31 3.76 14.39 4.01 14.44 4.27C14.5 4.54 14.52 4.8 14.52 5.07L12.94 5.07C12.94 4.91 12.92 4.75 12.89 4.58C12.86 4.43 12.81 4.27 12.75 4.12C12.69 3.97 12.61 3.83 12.52 3.69C12.43 3.56 12.33 3.43 12.22 3.32C12.1 3.2 11.98 3.1 11.85 3.01C11.71 2.92 11.57 2.84 11.42 2.78C11.27 2.72 11.12 2.67 10.96 2.64C10.81 2.61 10.65 2.59 10.49 2.59L5.03 2.59C4.87 2.59 4.71 2.61 4.55 2.64C4.4 2.67 4.24 2.72 4.09 2.78C3.95 2.84 3.8 2.92 3.67 3.01C3.54 3.1 3.41 3.2 3.3 3.32C3.18 3.43 3.08 3.56 2.99 3.69C2.9 3.83 2.83 3.97 2.77 4.12C2.71 4.27 2.66 4.43 2.63 4.58C2.6 4.75 2.58 4.91 2.58 5.07L2.58 10.57C2.58 10.73 2.6 10.89 2.63 11.05C2.66 11.21 2.71 11.37 2.77 11.52C2.83 11.67 2.9 11.81 2.99 11.94C3.08 12.08 3.18 12.2 3.3 12.32C3.41 12.43 3.54 12.54 3.67 12.63C3.8 12.72 3.95 12.79 4.09 12.86C4.24 12.92 4.4 12.96 4.55 13C4.71 13.03 4.87 13.04 5.03 13.04L5.03 14.64Z"
                                                                      fill="currentColor" fill-opacity="1.000000"
                                                                      fill-rule="evenodd"></path>
                                                            </g>
                                                            <path id="path"
                                                                  d="M14.75 18.91L9.3 18.91C9.03 18.91 8.77 18.88 8.51 18.83C8.25 18.78 8 18.7 7.75 18.6C7.51 18.49 7.27 18.37 7.05 18.22C6.83 18.07 6.63 17.9 6.44 17.71C6.25 17.52 6.09 17.32 5.94 17.1C5.79 16.87 5.67 16.64 5.57 16.39C5.47 16.14 5.39 15.89 5.34 15.63C5.28 15.37 5.26 15.1 5.26 14.83L5.26 9.33C5.26 9.06 5.28 8.8 5.34 8.54C5.39 8.28 5.47 8.02 5.57 7.77C5.67 7.53 5.79 7.29 5.94 7.07C6.09 6.85 6.25 6.64 6.44 6.45C6.63 6.26 6.83 6.09 7.05 5.95C7.27 5.8 7.51 5.67 7.75 5.57C8 5.47 8.25 5.39 8.51 5.34C8.77 5.29 9.03 5.26 9.3 5.26L14.75 5.26C15.01 5.26 15.28 5.29 15.54 5.34C15.8 5.39 16.05 5.47 16.29 5.57C16.54 5.67 16.77 5.8 16.99 5.95C17.21 6.09 17.41 6.26 17.6 6.45C17.79 6.64 17.96 6.85 18.1 7.07C18.25 7.29 18.37 7.53 18.48 7.77C18.58 8.02 18.65 8.28 18.71 8.54C18.76 8.8 18.78 9.06 18.78 9.33L18.78 14.83C18.78 15.1 18.76 15.37 18.71 15.63C18.65 15.89 18.58 16.14 18.48 16.39C18.37 16.64 18.25 16.87 18.1 17.1C17.96 17.32 17.79 17.52 17.6 17.71C17.41 17.9 17.21 18.07 16.99 18.22C16.77 18.37 16.54 18.49 16.29 18.6C16.05 18.7 15.8 18.78 15.54 18.83C15.28 18.88 15.01 18.91 14.75 18.91ZM9.3 6.86C9.13 6.86 8.97 6.87 8.82 6.91C8.66 6.94 8.51 6.98 8.36 7.05C8.21 7.11 8.07 7.18 7.93 7.28C7.8 7.37 7.68 7.47 7.56 7.58C7.45 7.7 7.35 7.82 7.26 7.96C7.17 8.09 7.09 8.24 7.03 8.38C6.97 8.54 6.92 8.69 6.89 8.85C6.86 9.01 6.84 9.17 6.84 9.33L6.84 14.83C6.84 15 6.86 15.16 6.89 15.32C6.92 15.48 6.97 15.63 7.03 15.78C7.09 15.93 7.17 16.07 7.26 16.21C7.35 16.34 7.45 16.47 7.56 16.58C7.68 16.7 7.8 16.8 7.93 16.89C8.07 16.98 8.21 17.06 8.36 17.12C8.51 17.18 8.66 17.23 8.82 17.26C8.97 17.29 9.13 17.31 9.3 17.31L14.75 17.31C14.91 17.31 15.07 17.29 15.23 17.26C15.38 17.23 15.54 17.18 15.69 17.12C15.83 17.06 15.98 16.98 16.11 16.89C16.24 16.8 16.37 16.7 16.48 16.58C16.59 16.47 16.7 16.34 16.79 16.21C16.87 16.07 16.95 15.93 17.01 15.78C17.07 15.63 17.12 15.48 17.15 15.32C17.18 15.16 17.2 15 17.2 14.83L17.2 9.33C17.2 9.17 17.18 9.01 17.15 8.85C17.12 8.69 17.07 8.54 17.01 8.38C16.95 8.24 16.87 8.09 16.79 7.96C16.7 7.82 16.59 7.7 16.48 7.58C16.37 7.47 16.24 7.37 16.11 7.28C15.98 7.19 15.83 7.11 15.69 7.05C15.54 6.98 15.38 6.94 15.23 6.91C15.07 6.87 14.91 6.86 14.75 6.86L9.3 6.86Z"
                                                                  fill="currentColor" fill-opacity="1.000000"
                                                                  fill-rule="nonzero"></path>
                                                        </g>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card -->
        </div>
        <!-- container -->
    </div>

    <div class="container px-3">
        <form action="">
            @csrf
            <input id="chat_id" type="hidden" value="{{ isset($last_chat) ? $last_chat->id : null }}"/>
            <div class="flex items-center gap-2">
                <div
                    class="bg-white w-full rounded-md border border-slate-200 p-1 dark:bg-slate-950 dark:border-slate-800">
                    <!-- Loader Section -->
                    <div id="fileLoader" class="hidden flex items-center justify-center mb-2">
                        <div
                            class="loader border-t-transparent border-4 border-blue-500 rounded-full w-6 h-6 animate-spin"></div>
                        <span class="ml-2 text-sm text-slate-500 dark:text-slate-300">Uploading...</span>
                    </div>

                    <!-- File preview section -->
                    <div id="filePreviewContainer" style="margin: 0.15rem" class="hidden">
                        <div class="relative flex items-center space-x-2 border rounded-md p-2">
                            <button type="button" id="removeFile"
                                    class="absolute top-1 right-1 h-6 w-6 bg-rose-500 text-base/7 text-white rounded-full hover:bg-rose-500">
                                &times;
                            </button>
                            <img id="filePreviewImage" src="" alt="File Preview" class="w-16 h-16 object-cover hidden">
                            <div id="filePreviewIconContainer"
                                 class="hidden flex items-center justify-center w-16 h-16 bg-gray-100 rounded-md">
                                <img id="filePreviewIcon" class="w-12 h-12" alt="File Icon">
                            </div>
                            <span id="filePreviewText" class="block text-sm text-slate-600 dark:text-slate-300"></span>
                        </div>
                    </div>

                    <div class="flex items-center">
                        <button type="button"
                                class="items-center h-8 justify-center rounded-full bg-blue-600 p-1 text-white transition-all hover:bg-blue-800"
                                id="send_message_button" onclick="return sendMessage('{{$chatBot->image}}')">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6" viewBox="0 0 24 24" fill="none">
                                <path d="M12 5V19M12 5L6 11M12 5L18 11" stroke="#ffffff" stroke-width="2"
                                      stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                        </button>
                        <button type="button"
                                class="items-center h-8 justify-center rounded-full bg-blue-600 p-1 text-white transition-all hover:bg-blue-800 hidden"
                                id="stop_button">
                            <svg stroke-width="1.5" class="h-6" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 24 24" stroke="currentColor" fill="none" stroke-linecap="round"
                                 stroke-linejoin="round">
                                <path d="M8 13v-7.5a1.5 1.5 0 0 1 3 0v6.5"></path>
                                <path d="M11 5.5v-2a1.5 1.5 0 1 1 3 0v8.5"></path>
                                <path d="M14 5.5a1.5 1.5 0 0 1 3 0v6.5"></path>
                                <path
                                    d="M17 7.5a1.5 1.5 0 0 1 3 0v8.5a6 6 0 0 1 -6 6h-2h.208a6 6 0 0 1 -5.012 -2.7a69.74 69.74 0 0 1 -.196 -.3c-.312 -.479 -1.407 -2.388 -3.286 -5.728a1.5 1.5 0 0 1 .536 -2.022a1.867 1.867 0 0 1 2.28 .28l1.47 1.47"></path>
                            </svg>
                        </button>
                        <div class="relative flex w-full flex-grow sm:w-auto">
            <textarea
                class="z-10 h-12 w-full rounded-full border-white bg-white text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-950 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                placeholder="{{__('Write your question here.')}}" id="prompt"></textarea>
                        </div>
                        <label
                            class="items-center justify-center h-6 w-8 rounded-full text-white transition-all"
                            id="attach_file_button">
                            <input type="file" id="fileUpload" name="file" class="hidden"
                                   accept=".png, .jpg, .jpeg, .xlsx, .xls, .doc, .docx"/>
                            <img src="{{asset('images/attach-file.png')}}" class="h-full" alt="">
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <template id="chat_user_bubble">
        <div class="flex-row-reverse flex items-end gap-2">
            <div
                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-950">
                <img src="{{asset('images/nouser.webp')}}" alt="User Avatar"/>
            </div>
            <div>
                <div
                    class="flex justify-end mb-1 h-16 flex-shrink-0 overflow-hidden rounded border-2 border-slate-600 max-w-md dark:border-slate-700 image-container hidden">
                    <small class="my-auto mx-2 text-slate-500 image-path" dir="ltr"></small>
                    <img src="" alt="" class="image-prompt py-2 px-1 w-14">
                </div>
                <div class="bg-blue-50 dark:bg-blue-950  max-w-md rounded-md px-4 py-3">
                    <p class="chat-content text-sm text-slate-500 dark:text-slate-300"></p>
                </div>
            </div>
        </div>
    </template>

    <template id="chat_ai_bubble">
        <div class=" flex items-end gap-2">
            <div
                class="inline-flex h-10 w-10 flex-shrink-0 overflow-hidden rounded-full border-2 border-white dark:border-slate-700">
                <img id="bot_image" src="" alt=""/>
            </div>
            <div class=" bg-slate-100 dark:bg-slate-900 max-w-md rounded-md px-4 py-3">
                <p class="chat-content text-sm text-slate-500 dark:text-slate-300 loader"></p>
            </div>
        </div>
    </template>
@endsection

@section('scripts')
    <script src="{{asset('assets/js/functions/chats.js')}}"></script>

    <script>
        function deleteDocument(elm) {
            const currenturl = window.location.href;
            const server = currenturl.split('/')[0];
            Swal.fire({
                    title: __('warning', locale),
                    text: __('sureToDelete', locale),
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    cancelButtonText: __('itIsOk', locale),
                    confirmButtonText: __('yes', locale)
                },
            ).then((result) => {
                if (result.isConfirmed) {
                    const delete_url =
                        `${server}/user/chat/delete/${$(elm).data('id')}`;

                    $.ajax({
                        type: "get",
                        url: delete_url,
                        success: function (data) {
                            Swal.fire({
                                title: __('ok', locale),
                                text: __('contentDeleted', locale),
                                icon: "success",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: __('thanks', locale)
                            }).then(() => {
                                window.location.reload()
                            });
                        },
                        error: function () {
                            Swal.fire({
                                title: __('sorry', locale),
                                text: __('deleteError', locale),
                                icon: "error",
                                confirmButtonColor: "#3085d6",
                                confirmButtonText: __('thanks', locale)
                            });
                        }
                    });
                }
            })
        }
    </script>
@endsection
