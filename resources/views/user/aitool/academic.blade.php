<div class="tab-panel active hidden [&.active]:block" id="summarize_text">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center pb-2">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Summarize Text')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-md p-1 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('For every 1,000 input words, 500 Tomans will be deducted from your wallet.')}}</small>
                <form action="{{route('aitools.generate')}}" id="summarize_text_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="summarize_text">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="text"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Text')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your text here to summarize')}}"
                                id="text"
                                rows="4" name="text"></textarea>
                        </div>
                        <p class="mb-2 ms-3 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__('Or upload word file here')}}</p>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="product_image">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the Word file from here.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600" dir="ltr"></p>
                                </div>

                                <input class="hidden file" name="file" id="product_image" type="file"
                                       accept=".doc, .docx">
                            </label>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full"
                             id="maximum_length_label-input">
                            <label for="maximum_length" id="maximum_length_label"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Number of words') }} </label>
                            <div class="relative">
                                <input
                                    class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                    id="maximum_length" type="text" inputmode="numeric" max="2000"
                                    value="500" name="maximum_length"/>
                            </div>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="summarize_text"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div
                class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="content-area flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="article_generator">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center pb-2">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Article Generator')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-md p-1 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('In this section, it is possible to write articles of up to 10,000 words')}}  {{__('The structure consists of an abstract, an introduction, a main body with three sections, a conclusion, and references.')}}</small>
                <form action="{{route('aitools.generate')}}" id="article_generator_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="article_generator">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full"
                             id="maximum_length_label-input">
                            <label for="number_of_results"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Title') }} </label>
                            <div class="relative">
                                <input
                                    class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                    id="title" type="text" inputmode="numeric" name="title"
                                    placeholder="{{__('Write your article title here')}}"/>
                            </div>
                        </div>

                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="degree"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Degree')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="degree" name="degree">
                                <option value="bc">{{__('Bachelor')}}</option>
                                <option value="ms">{{__('Master')}}</option>
                                <option value="phd">{{__('Phd')}}</option>
                            </select>
                        </div>

                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full"
                             id="maximum_length_label-input">
                            <label for="total_words"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Number of words') }} </label>
                            <div class="relative">
                                <input
                                    class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                    id="total_words" type="text" inputmode="numeric" name="total_words" value="2000"/>
                            </div>
                        </div>

                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="px-3 py-2">
                            <button type="button" data-slug="article_generator"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div
                class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="content-area flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
