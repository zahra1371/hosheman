@extends('layout.landing.app')

@section('title')
    {{__('HosheMan')}} |  {{__('AI List')}}
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="mb-7 flex flex-col items-center">
                <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white">{{__('AI List')}} </h2>
            </div>
            <div class="grid gap-6 xs:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4">
                @foreach($tool_categories as $item)
                    @php
                        $url='images/tools/'.$item->name.'.png'
                    @endphp
                    <a href="{{!in_array($item->name,['file',])?route('aitools',$item->name):'#'}}" class="rounded-lg border border-slate-200 bg-white p-5 transition-all hover:-translate-y-1 dark:border-slate-800 dark:bg-slate-950">
                        <div class="mb-2 h-10">
                            <img src="{{asset($url)}}" alt="" class="h-10">
                        </div>
                        @if(session()->has('locale') && session()->get('locale') === 'en')
                            <h6 class="mb-2 text-lg font-bold text-slate-700 dark:text-white">{{__($item->name)}} {{in_array($item->name,['file','general'])?__('(Coming soon)'):''}} </h6>
                        @else
                            <h6 class="mb-2 text-lg font-bold text-slate-700 dark:text-white">{{__($item->fa_name)}} {{in_array($item->name,['file','general'])?__('(Coming soon)'):''}} </h6>
                        @endif
                        <p class="text-sm text-slate-500 dark:text-slate-400"> {{__($item->description)}} </p>
                    </a><!-- grid-item -->
                @endforeach
            </div><!-- grid -->
        </div><!-- container -->
    </div>
@endsection
