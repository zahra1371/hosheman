<div id="instagram_captions" class="tab-panel active hidden [&.active]:block">
    <div
        class="flex flex-wrap h-full rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram Captions')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('Writes interactive captions for you. just describe what is your content about.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_captions_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_captions">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="title"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Topic')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your topic here')}}"
                                id="title"
                                rows="4" name="title"></textarea>
                            <span class="error error-title text-sm text-rose-600"></span>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') == 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full"
                             id="maximum_length_label-input">
                            <label for="number_of_words"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Number of words') }} </label>
                            <div class="relative">
                                <input id="number_of_words"
                                       class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                       type="text" inputmode="numeric" max="400" value="200" name="number_of_words"/>
                            </div>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full" id="tone-input">
                            <label for="tone"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Tone of Voice')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="tone" name="tone_of_voice">
                                @foreach($voice_tones as $tone)
                                    <option
                                        value="{{$tone}}">{{__($tone)}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_captions" id="instagram_captions_btn"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center content-area">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="result-text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instagram_hashtags" class="tab-panel hidden [&.active]:block">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram Hashtags')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('Write the topic of the post or rails for her to write the hashtags related to it.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_hashtag_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_hashtags">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="keywords"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Keywords')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your keywords here and separate them with a comma (،).')}}"
                                id="keywords"
                                rows="4" name="keywords"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="number_of_results"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Number of Results') }} </label>
                            <div class="relative">
                                <input
                                    class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                    id="number_of_results" type="text" inputmode="numeric" max="20"
                                    value="5" name="number_of_results"/>
                            </div>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_hashtag"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center content-area">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text"> {{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instagram_stories" class="tab-panel hidden [&.active]:block">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram Story')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('Tell the subject and purpose of the story and she will write a scenario for you.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_stories_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_stories">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your story topic or description here')}}"
                                rows="4" name="description"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="cta"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('CTA')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="cta" name="cta">
                                <option value="Follow page">{{__('Follow page')}}</option>
                                <option value="Buy Now">{{__('Buy Now')}}</option>
                                <option value="Share">{{__('Share')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="number_of_results"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{ __('Number of stories') }} </label>
                            <div class="relative">
                                <input
                                    class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                    id="number_of_results" type="text" inputmode="numeric" max="15"
                                    value="5" name="number_of_results"/>
                            </div>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_stories"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center content-area">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instagram_reels" class="tab-panel hidden [&.active]:block">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram Reel')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('Write the topic of your page so that will write the reel scenario for you.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_reels_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_reels">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your reel topic or description here')}}"
                                rows="4" name="description"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="target"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Duration')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="duration" name="duration">
                                <option value="15">{{__('15')}}</option>
                                <option value="30">{{__('30')}}</option>
                                <option value="45">{{__('45')}}</option>
                                <option value="60">{{__('60')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full"
                             id="main_goal-input hidden">
                            <label for="main_goal"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('CTA')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="main_goal" name="main_goal">
                                <option value="save">{{__('Save')}}</option>
                                <option value="comment">{{__('Comment')}}</option>
                                <option value="share">{{__('Share')}}</option>
                                <option value="like">{{__('Like')}}</option>
                                <option value="follow">{{__('Follow page')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="style"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Style')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="style" name="style">
                                <option value="چالشی">{{__('challenge')}}</option>
                                <option value="آموزشی">{{__('educational')}}</option>
                                <option value="برندسازی">{{__('branding')}}</option>
                                <option value="اعتماد سازی">{{__('trust')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="players"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Players Count')}} </label>
                            <input
                                class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                placeholder="{{__("If you don't have an actor, write 0.")}}" name="player">
                        </div>
                        <div class="w-full px-3 py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Details')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write additional details here. For example, if something is trending, briefly mention it here so the reel can be written accordingly')}}"
                                rows="4" name="reel_details"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_reels"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center content-area">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instagram_calendar" class="tab-panel hidden [&.active]:block">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center pb-2">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram Calendar')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('By specifying the topic of the page and the number of target followers, it will write a 30-day content calendar for you.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_calendar_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_calendar">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write your field of work here')}}"
                                rows="4" name="description"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="follower_target"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('number of absorption')}} </label>
                            <input
                                class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                placeholder="{{__('Target follower count, for example, 10k')}}" name="follower_target">
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_calendar"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div class="flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center content-area">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="instagram_campaign" class="tab-panel hidden [&.active]:block">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <div class="flex items-center pb-2">
                    <div class="me-3 h-6">
                        <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                             class="h-full">
                            <path class="fill-blue-300"
                                  d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                            <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                            <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                        </svg>
                    </div>
                    <h5 class="text-lg font-bold text-slate-700 dark:text-white">{{__('Instagram campaign')}}</h5>
                </div>
                <small
                    class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('Write your field of work to her so that she will write the advertising campaign for you.')}}</small>
                <form action="{{route('aitools.generate')}}" id="instagram_campaign_form" method="post" class="mt-3">
                    <input type="hidden" name="post_type" value="instagram_campaign">
                    @csrf
                    <div class="-mx-3 -my-2 flex flex-wrap">
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="follower_target"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Brand')}} </label>
                            <input
                                class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                placeholder="{{__('Write the name of your brand or page here')}}" name="brand_name">
                        </div>
                        <div class="w-full px-3 py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Describe your field of work')}}"
                                rows="4" name="description"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Gender Target')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="gender_target" name="gender_target">
                                <option value="men">{{__('Men')}}</option>
                                <option value="women">{{__('Women')}}</option>
                                <option value="both">{{__('Both gender')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="follower_target"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Campaign Duration')}} </label>
                            <input
                                class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"
                                placeholder="{{__('How many days is your campaign?')}}" name="campagin_duration">
                        </div>
                        <div class="w-full px-3 py-2">
                            <label for="details"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Details')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Additional details (if needed, for example, if you want a specific feature to be highlighted or if you have a unique competitive advantage, etc.)')}}"
                                rows="4" name="details"></textarea>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="language"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Language')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="language" name="language">
                                @foreach($languages as $item)
                                    @if(session()->has('locale') && session()->get('locale') === 'en')
                                        <option value="{{$item->name}}">{{__($item->name)}}</option>
                                    @else
                                        <option value="{{$item->name}}">{{__($item->fa_name)}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" data-slug="instagram_campaign"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="w-full lg:w-3/5">
            <div class="h-full overflow-x-auto p-6 lg:scrollbar-none scrollbar-track-transparent scrollbar-thumb-slate-400 dark:scrollbar-track-slate-800 hidden result-container">
                <div class="prose dark:prose-invert p-5 text-sm text-justify result">
                </div>
            </div>
            <div
                class="content-area flex h-full w-full flex-col items-center justify-center px-6 py-20 text-center">
                <div class="mb-3 h-16">
                    <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                         class="h-full">
                        <path class="fill-blue-300"
                              d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                        <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                        <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                              d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                    </svg>
                </div>
                <div class="font-medium text-slate-500 dark:text-slate-400">
                    <p class="content_text">{{__("Fill out the form and press the 'Let's Go' button")}}</p>
                </div>
            </div>
        </div>
    </div>
</div>

