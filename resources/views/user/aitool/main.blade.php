@php
    $voice_tones = ['Professional', 'Funny', 'Casual', 'Excited', 'Witty', 'Sarcastic', 'Feminine', 'Masculine', 'Bold', 'Dramatic'];
@endphp

@extends('layout.landing.app')

@section('title')
    {{__('HosheMan')}} |  {{__('ALL Writers List')}}
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="flex flex-wrap items-start gap-8 xl:flex-nowrap">
                <div
                    class="w-full rounded-lg border border-slate-200 bg-white px-7 py-6 dark:border-slate-800 dark:bg-slate-950 xl:w-96">
                    <ul class="tab-nav -mx-4 flex flex-wrap xl:mx-0">
                        @php $i=0 @endphp
                        @foreach($tools as $tool)
                            <li class="tab-item w-full px-4 xs:w-1/2 xs:py-1 sm:w-1/3 lg:w-1/4 xl:w-full xl:px-0 xl:py-0">
                                <a href="#" data-target="#{{$tool->slug}}" data-slug="{{$tool->slug}}"
                                   data-title="{{__($tool->name)}}" data-id="{{$tool->id}}"
                                   class="{{$i==0?'active':''}} template-slug tab-toggle relative isolate flex text-slate-500 before:content-[''] dark:text-slate-400 [&.active]:text-blue-600 [&.active]:before:absolute [&.active]:before:-inset-x-3 [&.active]:before:inset-y-0 [&.active]:before:-z-10 [&.active]:before:rounded-md [&.active]:before:bg-blue-100 [&.active]:before:dark:bg-blue-950">
                                    <div class="flex items-center py-2">
                                        <div class="me-3 h-5">
                                            <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                 class="h-full">
                                                <path class="fill-blue-300"
                                                      d="M13.7087 3.30729C12.6049 2.90591 11.395 2.90591 10.2913 3.30729L3.24266 5.87042C1.48732 6.50872 1.4873 8.99128 3.24266 9.62959L10.2913 12.1927C11.395 12.5941 12.6049 12.5941 13.7087 12.1927L20.7573 9.62959C22.5126 8.99128 22.5126 6.50873 20.7573 5.87042L13.7087 3.30729Z"/>
                                                <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M2.05781 12.395C2.24283 11.8746 2.81467 11.6028 3.33504 11.7878L11.33 14.6304C11.7634 14.7845 12.2367 14.7845 12.67 14.6304L20.665 11.7878C21.1854 11.6028 21.7572 11.8746 21.9422 12.395C22.1273 12.9154 21.8554 13.4872 21.335 13.6722L13.3401 16.5149C12.4733 16.8231 11.5268 16.8231 10.66 16.5149L2.66502 13.6722C2.14465 13.4872 1.87279 12.9154 2.05781 12.395Z"/>
                                                <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M2.05781 16.645C2.24283 16.1246 2.81467 15.8528 3.33504 16.0378L11.33 18.8804C11.7634 19.0345 12.2367 19.0345 12.67 18.8804L20.665 16.0378C21.1854 15.8528 21.7572 16.1246 21.9422 16.645C22.1273 17.1654 21.8554 17.7372 21.335 17.9222L13.3401 20.7649C12.4733 21.0731 11.5268 21.0731 10.66 20.7649L2.66502 17.9222C2.14465 17.7372 1.87279 17.1654 2.05781 16.645Z"/>
                                            </svg>
                                        </div>
                                        <span class="text-sm font-medium"> {{__($tool->name)}} </span>
                                    </div>
                                </a>
                            </li>
                            @php $i++ @endphp
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content flex-grow-1 w-full">
                    @if($first_toos->category->name === 'instagram')
                        @include('user.aitool.instagram')
                    @elseif($first_toos->category->name === 'youtube')
                        @include('user.aitool.youtube')
                    @elseif($first_toos->category->name === 'website')
                        @include('user.aitool.website')
                    @elseif($first_toos->category->name === 'general')
                        @include('user.aitool.general')
                    @elseif($first_toos->category->name === 'voiceover')
                        @include('user.aitool.voiceover')
                    @elseif($first_toos->category->name === 'graphics')
                        @include('user.aitool.graphics')
                    @elseif($first_toos->category->name === 'development')
                        @include('user.aitool.development')
                    @elseif($first_toos->category->name === 'academic')
                        @include('user.aitool.academic')
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if(request()->is('user/ai-tools/graphics'))
        <div class="relative px-3 py-10">
            <div class="container px-3">
                <h5 class="mb-4 text-lg font-bold text-slate-700 dark:text-white results-title"> عکس هایی که ساختی </h5>
                <div
                    class="rounded-lg border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950 xs:p-7">
                    <div class="js-lightbox-gallery grid grid-cols-2 gap-4 xs:grid-cols-3 md:grid-cols-5 results">
                        @foreach($graphics as $graphic)
                            @if($graphic->type === 'image')
                                <div class="image-result group relative">
                                    <img
                                        class="image-result-img aspect-[4/3] rounded-md border border-slate-200 object-cover dark:border-slate-800"
                                        src="https://content.hosheman.com/{{$graphic->output}}">
                                    <div
                                        class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                                        <a href="https://content.hosheman.com/{{$graphic->output}}" data-pswp-width="1000" data-pswp-height="1000"
                                           class="view-image js-lightbox-toggle flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                 stroke-width="2"
                                                 stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0"></path>
                                                <path
                                                    d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6"></path>
                                            </svg>
                                        </a>
                                        <a class="flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                           href="{{route('content.download',$graphic->id)}}"
                                           download="">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                 stroke="currentColor" class="h-4">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                                            </svg><!-- arrow-down-tray - outline - heroicons -->
                                        </a>
                                        <a class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                           href="#" data-slug="{{$graphic->id}}" onclick="return deleteImage(this)">
                                            <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <div
                                        class="absolute bottom-0 w-full rounded-b-md bg-gradient-to-b from-transparent via-slate-900 via-70% to-slate-900 p-3 opacity-0 transition-all group-hover:opacity-100">
                                        <span class="image_prompt line-clamp-2 text-xs font-bold text-white">{{$graphic->input}}</span>
                                    </div>
                                </div>
                            @else
                                <div class="video-result group relative">
                                    <figure
                                        class="relative mb-3 aspect-[4/3] overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
                                        data-lqd-skeleton-el
                                    >
                                        <video
                                            class="h-full w-full object-cover object-center"
                                            loading="lazy"
                                        >
                                            <source
                                                    src="https://content.hosheman.com/videos/{{$graphic->output}}"
                                                    loading="lazy"
                                                    type="video/mp4"
                                            >
                                        </video>
                                        <div
                                            class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                                            <a class="lqd-video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                               href="{{route('content.download',$graphic->id)}}" download="" title="دانلود فیلم">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                     viewBox="0 0 24 24"
                                                     stroke-width="1.5" stroke="currentColor" class="h-4">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                                                </svg><!-- arrow-down-tray - outline - heroicons -->
                                            </a>
                                            <a class="lqd-video-result-play flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                               href="https://content.hosheman.com/videos/{{$graphic->output}}" data-fslightbox="video-gallery" title="پخش فیلم">
                                                <svg class="h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2"
                                                     stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path
                                                        d="M6 4v16a1 1 0 0 0 1.524 .852l13 -8a1 1 0 0 0 0 -1.704l-13 -8a1 1 0 0 0 -1.524 .852z"
                                                        stroke-width="0" fill="currentColor"></path>
                                                </svg>
                                            </a>
                                            <a class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                                               href="#" data-slug="{{$graphic->id}}" onclick="return deleteImage(this)">
                                                <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                                                </svg>
                                            </a>
                                        </div>
                                    </figure>
                                </div>
                            @endif

                        @endforeach
                    </div>
                    <!-- grid -->
                </div>
                <!-- card -->
            </div>
            <!-- container -->
        </div>
    @endif
@endsection

@section('scripts')
    <script src="{{asset('assets/js/functions/create-content.js')}}"></script>
    @if(request()->is('user/ai-tools/development'))
        <!-- Include Prism.js for syntax highlighting -->
        <link href="{{asset('assets/css/plugins/prism.css')}}" rel="stylesheet" />
        <script src="{{asset('assets/js/plugins/prism.js')}}"></script>
        <script src="{{asset('assets/js/plugins/prism-css.min.js')}}"></script>
    @endif
    <script>
        $('.generator_button').click(function () {
            const formId=$(this).data('slug')
            createContent($(`#${formId}_form`))
        })

        $('#model').on('change',function (){
            if ($(this).find(':selected').val() === 'std'){
                $('#style').addClass('hidden')
                $('#lighting').addClass('hidden')
                $('#mood').addClass('hidden')
            }else {
                $('#style').removeClass('hidden')
                $('#lighting').removeClass('hidden')
                $('#mood').removeClass('hidden')
            }
        })
    </script>
@endsection

