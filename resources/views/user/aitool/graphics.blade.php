<div class="mb-7">
    <div class="p-3 bg-rose-100 text-center">
        <p class="mb-2 text-sm text-rose-600 dark:text-white">{{__("Photos will remain in your panel for only two weeks and will be deleted afterward. It's better to download any photo you like immediately.")}}</p>
    </div>
</div>

<div class="tab-panel active hidden [&.active]:block" id="ai_image_generator">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Image Generator')}} </h5>
                <small
                    class="inline-flex rounded px-2 py-1 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__('For generate real image choose stable diffusion model.')}}</small>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_image_generator_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_image_generator">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="description"
                                   class="mb-4 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Idea text')}}</label>
                            <div
                                class="rounded-md border border-slate-200 p-1 dark:border-slate-800 sm:flex-nowrap">
                                <div class="relative flex w-full flex-grow sm:w-auto">
                                            <textarea
                                                class="mb-0 w-full resize-none rounded-md border-0 bg-white px-3 py-3 text-sm text-slate-600 placeholder:text-slate-400 focus:border-0 focus:shadow-none focus:outline-none focus:ring-0 disabled:bg-slate-100 disabled:text-slate-400 dark:bg-slate-950 dark:text-slate-200"
                                                rows="1" placeholder="{{__('You need to write your idea here :)')}}"
                                                id="description" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="model"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('AI Model')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="model" name="model">
                                <option value="std">{{__('std')}}</option>
                                <option value="dalle-3">{{__('Dalle-3')}}</option>
                            </select>
                        </div>
                        <div id="style" class="w-full px-3 py-2 sm:w-1/2 lg:w-full hidden">
                            <label for="image_style"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Art Style')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="image_style" name="image_style">

                                <option value="">{{__('None')}}</option>
                                <option value="3d_render">{{__('3D Render')}}</option>
                                <option value="realistic">{{__('Realistic')}}</option>
                                <option value="anime">{{__('Anime')}}</option>
                                <option value="ballpoint_pen">{{__('Ballpoint Pen Drawing')}}</option>
                                <option value="cartoon">{{__('Cartoon')}}</option>
                                <option value="isometric">{{__('Isometric')}}</option>
                                <option value="minimalism">{{__('Minimalism')}}</option>
                                <option value="modern">{{__('Modern')}}</option>
                                <option value="origami">{{__('Origami')}}</option>
                                <option value="pencil">{{__('Pencil Drawing')}}</option>
                                <option value="pixel">{{__('Pixel')}}</option>
                                <option value="sticker">{{__('Sticker')}}</option>
                                <option value="vector">{{__('Vector')}}</option>
                                <option value="watercolor">{{__('Watercolor')}}</option>
                            </select>
                        </div>
                        <div id="lighting" class="w-full px-3 py-2 sm:w-1/2 lg:w-full hidden">
                            <label for="image_lighting"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Lightning Style')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="image_lighting" name="image_lighting">
                                <option value="">{{__('None')}}</option>
                                <option value="natural">{{__('Natural')}}</option>
                                <option value="warm">{{__('Warm')}}</option>
                                <option value="cold">{{__('Cold')}}</option>
                                <option value="neon">{{__('Neon')}}</option>
                                <option value="ambient">{{__('Ambient')}}</option>
                                <option value="cinematic">{{__('Cinematic')}}</option>
                                <option value="dramatic">{{__('Dramatic')}}</option>
                                <option value="foggy">{{__('Foggy')}}</option>
                                <option value="studio">{{__('Studio')}}</option>
                            </select>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="size"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Image size')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="size" name="size">
                                <option value="1024x1024">{{__('Post')}}</option>
                                <option value="1792x1024">{{__('youtube')}}</option>
                                <option value="1024x1792">{{__('Story')}}</option>
                            </select>
                        </div>
                        <div id="mood" class="w-full px-3 py-2 sm:w-1/2 lg:w-full hidden">
                            <label for="image_mood"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Mood')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="image_mood" name="image_mood">
                                <option value="">{{__('None')}}</option>
                                <option value="aggressive">{{__('Aggressive')}}</option>
                                <option value="angry">{{__('Angry')}}</option>
                                <option value="boring">{{__('Boring')}}</option>
                                <option value="bright">{{__('Bright')}}</option>
                                <option value="calm">{{__('Calm')}}</option>
                                <option value="cheerful">{{__('Cheerful')}}</option>
                                <option value="chilling">{{__('Chilling')}}</option>
                                <option value="colorful">{{__('Colorful')}}</option>
                                <option value="dark">{{__('Dark')}}</option>
                                <option value="neutral">{{__('Neutral')}}</option>
                            </select>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button" id="ai_image_generator"
                                    data-slug="ai_image_generator"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Describe the image you want and press the 'Let's Go' button")}}
                        </p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_video">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Video Generator')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_video_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_video">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="delete_background">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the image here. The image must be in png, jpg, or jpeg format.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="delete_background" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>

                        <div class="w-full py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Describe in detail what should appear in the output (at most in 512 words).')}}"
                                rows="4" id="product_description" name="description"></textarea>
                        </div>

                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="duration"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Video duration')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="duration" name="duration">
                                <option value="5">{{__('5 seconds')}}</option>
                                <option value="10">{{__('10 seconds')}}</option>
                            </select>
                        </div>

                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="size"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Video Size')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="size" name="size">
                                <option value="1280:768">{{__('Landscape')}}</option>
                                <option value="768:1280">{{__('Portrait')}}</option>
                            </select>
                        </div>

                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_video" id="ai_video"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Select the image and press the 'Let's Go' button.")}}</p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-video-area hidden">
                <div class="relative group">
                    <figure
                        class="lqd-video-result-fig relative mb-3 aspect-square overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
                        data-lqd-skeleton-el>
                        <video
                            class="lqd-video-result-video h-full w-full object-cover object-center"
                            loading="lazy">
                            <source class="main-video"
                                    src=""
                                    loading="lazy"
                                    type="video/mp4">
                        </video>
                    </figure>
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود فیلم">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_delete_background">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('Remove Image Background')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_delete_background_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_delete_background">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="delete_background">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the image here. The image must be in png, jpg, or jpeg format.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="delete_background" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                            <small class="text-sm text-rose-600">{{__('The dimensions of the image must be between 64 and 2048 pixels. Otherwise, you will encounter an error')}}</small>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_delete_background" id="ai_delete_background"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Select the image and press the 'Let's Go' button.")}}</p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_generate_background">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('Product Image Generator')}} </h5>
                <small
                    class="inline-flex mb-4 p-1 rounded text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">
                    {{__('Here, you can upload photos of your products and create attractive backgrounds for them. You can either create a custom background for the image or use pre-made backgrounds. Note that you can`t do both (write a description and select an image) at the same time.')}}
                </small>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_generate_background_form" method="post"
                      class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_generate_background">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="product_image">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the image here. The image must be in png, jpg, or jpeg format, and the file size should be less than 10 MB.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" name="file" id="product_image" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="w-full py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('Write here what your background should be and how it should look.')}}"
                                rows="4" id="product_description" name="description"></textarea>
                        </div>
                        <p class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                            {{__('Or select a sample background from here:')}}</p>
                        <div class="grid grid-cols-3 gap-3 sm:grid-cols-3 mt-5">
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="surprise-me">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="surprise-me" value="Surprise me">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/surprise-me.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('surprise me')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Studio">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Studio" value="Studio">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Studio.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Studio')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Silk">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Silk" value="Silk">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Silk.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Silk')}} </span>
                            </label>
                        </div>
                        <div id="additional-items"
                             class="hidden grid grid-cols-3 gap-3 sm:grid-cols-3 mt-5">
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Cafe">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Cafe" value="Cafe">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Cafe.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Cafe')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Tabletop">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Tabletop" value="Tabletop">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Tabletop.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Tabletop')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Light-Wood">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Light-Wood" value="Light Wood">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Light-Wood.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Light Wood')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Dark-Wood">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Dark-Wood" value="Dark Wood">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Dark-Wood.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Dark Wood')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Marble">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Marble" value="Marble">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Marble.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Marble')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Park-Bench">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Park-Bench" value="Park Bench">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Park-Bench.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Park Bench')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Outdoors">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Outdoors" value="Outdoors">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Outdoors.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Outdoors')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Roses">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Roses" value="Roses">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Roses.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Roses')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Lavender">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Lavender" value="Lavender">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Lavender.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Lavender')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Meadow">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Meadow" value="Meadow">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Meadow.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Meadow')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Dried-Flowers">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Dried-Flowers"
                                       value="Dried Flowers">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Dried-Flowers.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Dried Flowers')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Baby's-Breath">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Baby's-Breath"
                                       value="Baby's Breath">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Babys-Breath.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Baby`s Breath')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Kitchen">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Kitchen" value="Kitchen">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Kitchen.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Kitchen')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Fruits">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Fruits" value="Fruits">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Fruits.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Fruits')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Nature">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Nature" value="Nature">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Nature.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Nature')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Beach">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Beach" value="Beach">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Beach.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Beach')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Bathroom">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Bathroom" value="Bathroom">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Bathroom.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Bathroom')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Skyscraper">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Skyscraper" value="Skyscraper">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Skyscraper.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Skyscraper')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Gifts">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Gifts" value="Gifts">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Gifts.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Gifts')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Christmas">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Christmas" value="Christmas">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Christmas.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Christmas')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Wedding">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Wedding" value="Wedding">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Wedding.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Wedding')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Halloween">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Halloween" value="Halloween">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Halloween.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Halloween')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Paint">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Paint" value="Paint">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Paint.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Paint')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Fire">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Fire" value="Fire">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Fire.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Fire')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Water">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Water" value="Water">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Water.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Water')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Gold">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Gold" value="Gold">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Gold.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Gold')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Pebbles">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Pebbles" value="Pebbles">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Pebbles.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Pebbles')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Snow">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Snow" value="Snow">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Snow.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Snow')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Necklace">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Necklace" value="Necklace">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Necklace.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Necklace')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Modern-Interior">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Modern-Interior"
                                       value="Modern Interior">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Modern-Interior.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Modern Interior')}} </span>
                            </label>

                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Minimalist-Interior">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Minimalist-Interior"
                                       value="Minimalist Interior">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Minimalist-Interior.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Minimalist Interior')}} </span>
                            </label>
                            <label
                                class="relative flex h-full cursor-pointer flex-col has-[:checked]:cursor-default"
                                for="Luxury-Interior">
                                <input class="peer absolute h-0 w-0 opacity-0" type="radio"
                                       name="imageStyle" id="Luxury-Interior"
                                       value="Luxury Interior">
                                <img alt="" class="flex-shrink-0 rounded-t-md"
                                     src="{{asset('images/backgrounds/Luxury-Interior.webp')}}">
                                <span
                                    class="flex flex-grow items-center justify-center rounded-b-md border border-t-0 border-slate-200 p-2 text-center text-xs font-bold text-slate-600 transition-all duration-500 peer-checked:border-blue-200 peer-checked:bg-blue-100 peer-checked:text-slate-700 dark:border-slate-800 dark:text-slate-200 peer-checked:dark:border-blue-900 peer-checked:dark:bg-blue-950 peer-checked:dark:text-white"> {{__('Luxury Interior')}} </span>
                            </label>
                        </div>
                        <!-- دکمه نمایش بیشتر -->
                        <div class="text-center mt-4 w-full">
                            <button type="button" id="toggle-button"
                                    class="px-4 py-2 text-xs border border-slate-200 bg-white text-gray-400 rounded">
                                {{__('Show more')}}
                            </button>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="size"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Image size')}} </label>
                            <select
                                class="js-select z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                id="product-size" name="size">
                                <option value="1024x1024">{{__('Post')}}</option>
                                <option value="1792x1024">{{__('youtube')}}</option>
                                <option value="1024x1792">{{__('Story')}}</option>
                            </select>
                        </div>

                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_generate_background"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">{{__("Select your product image and press the 'Let's Go' button. Make sure the image has no background.")}}</p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_sketch">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Sketch')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_sketch_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_sketch">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="sketch">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select your sketch or drawing image here. The image must be in png, jpg, or jpeg format.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="sketch" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="w-full py-2">
                            <label for="description"
                                   class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">
                                {{__('Description')}}
                            </label>
                            <textarea
                                class="mb-0 w-full rounded-md border-slate-200 bg-white py-2 text-sm text-slate-600 focus:border-slate-200 focus:shadow-none focus:outline-none dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200"
                                placeholder="{{__('What you wish to see in the output image. A strong, descriptive prompt that clearly defines elements, colors, and subjects will lead to better results.')}}"
                                rows="4" id="ai_sketch" name="description"></textarea>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_sketch" id="ai_sketch"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Select your drawing or sketch image and press the 'Let's Go' button.")}}
                        </p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_outpaint">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Outpaint')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_outpaint_form" method="post" class="mt-3">
                    @csrf
                    <small class="inline-flex rounded mb-2 px-2 py-1 text-[11px] font-bold capitalize bg-rose-100 dark:bg-rose-950 text-rose-500">{{__("The aspect ratio of the image must be between 1:2.5 and 2.5:1 (1:2.5, 1:2, 1:1.5, 1:1, 1.5:1, 2:1, 2.5:1)")}}</small>
                    <input type="hidden" name="post_type" value="ai_outpaint">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="outpaint">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the image here. The image must be in png, jpg, or jpeg format.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="outpaint" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="description"
                                   class="mb-4 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__("How many pixels should it expand? (Enter a number from 0 to 2000. If you don't want it to expand from one side, enter 0.)")}}</label>
                            <div class="flex gap-x-3" dir="ltr">
                                <input name="up"
                                       class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                       id="passwordCR1" type="text" inputmode="numeric" placeholder="{{__('Up')}}"/>
                                <input name="down"
                                       class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                       id="passwordCR2" type="text" inputmode="numeric" placeholder="{{__('Down')}}"/>
                                <input name="left"
                                       class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                       id="passwordCR3" type="text" inputmode="numeric" placeholder="{{__('Left')}}"/>
                                <input name="right"
                                       class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-center text-sm text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 focus:dark:border-slate-800 disabled:dark:bg-slate-900"
                                       id="passwordCR4" type="text" inputmode="numeric" placeholder="{{__('Right')}}"/>
                            </div>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_outpaint" id="ai_outpaint"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Select the image and press the 'Let's Go' button.")}}
                        </p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_recolor">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Recolor')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_recolor_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_recolor">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="recolor">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        {{__('Select the image here. The image must be in png, jpg, or jpeg format.')}}
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="recolor" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="prompt" class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('describe color')}} </label>
                            <div class="relative isolate flex w-full">
                                <input class="z-10 w-full rounded-md border border-slate-200 bg-white py-2 px-3 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500" placeholder="{{__('For example, a red mug.')}}" id="prompt" name="prompt" />
                            </div>
                        </div>
                        <div class="w-full px-3 py-2 sm:w-1/2 lg:w-full">
                            <label for="select_prompt" class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200"> {{__('selected prompt')}} </label>
                            <div class="relative isolate flex w-full">
                                <input class="z-10 w-full rounded-md border border-slate-200 bg-white py-2 px-3 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500" placeholder="{{__('Name the object you want to change the color of (e.g., mug).')}}" id="select_prompt" name="select_prompt" />
                            </div>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_recolor" id="ai_recolor"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            {{__("Describe the image you want and press the 'Let's Go' button")}}
                        </p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="tab-panel hidden [&.active]:block" id="ai_erase_objects">
    <div
        class="flex flex-wrap rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950 lg:flex-nowrap xl:max-h-[calc(100vh-theme(space.52))]">
        <div
            class="w-full border-b border-slate-200 dark:border-slate-800 lg:w-2/5 lg:border-b-0 lg:border-e">
            <div
                class="h-full overflow-auto p-6 scrollbar-thin scrollbar-track-transparent scrollbar-thumb-slate-400">
                <h5 id="title"
                    class="mb-3 text-lg font-bold text-slate-700 dark:text-white"> {{__('AI Erase Objects')}} </h5>
                <!-- col -->
                <form action="{{route('aitools.generate')}}" id="ai_erase_objects_form" method="post" class="mt-3">
                    @csrf
                    <input type="hidden" name="post_type" value="ai_erase_objects">
                    <div class="-mx-3 flex flex-wrap">
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="erase_objects">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        تصویر اتود یا نقاشیت رو اینجا انتخاب کن. عکس باید با
                                        فرمت png یا jpg یا jpeg باشه.
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="erase_objects" name="file" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="flex w-full flex-col gap-5">
                            <label
                                class="lqd-filepicker-label min-h-64 flex w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-foreground/10 bg-background text-center transition-colors hover:bg-background/80"
                                for="mask_img">
                                <div class="flex flex-col items-center justify-center py-6">
                                    <svg stroke-width="1.5" class="size-11 mb-4"
                                         xmlns="http://www.w3.org/2000/svg" width="24"
                                         height="24" viewBox="0 0 24 24" stroke="currentColor"
                                         fill="none" stroke-linecap="round"
                                         stroke-linejoin="round">
                                        <path
                                            d="M7 18a4.6 4.4 0 0 1 0 -9a5 4.5 0 0 1 11 2h1a3.5 3.5 0 0 1 0 7h-1"></path>
                                        <path d="M9 15l3 -3l3 3"></path>
                                        <path d="M12 12l0 9"></path>
                                    </svg>
                                    <p class="mb-1 text-sm font-semibold">
                                        تصویر اتود یا نقاشیت رو اینجا انتخاب کن. عکس باید با
                                        فرمت png یا jpg یا jpeg باشه.
                                    </p>
                                    <p class="file-name mb-0 text-2xs text-rose-600"></p>
                                </div>

                                <input class="hidden file" id="mask_img" name="mask" type="file"
                                       accept=".png, .jpg, .jpeg">
                            </label>
                        </div>
                        <div class="px-3 py-2">
                            <button type="button"
                                    data-slug="ai_erase_objects" id="ai_erase_objects"
                                    class="generator_button inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Lets go')}} </button>
                        </div>
                    </div>
                </form>
                <!-- col -->
            </div>
        </div>
        <div class="w-full p-6 lg:w-3/5 lg:p-10">
            <div
                class="flex h-full w-full items-center justify-center text-center waiting-area">
                <div class="flex flex-col items-center py-2">
                    <div class="h-36">
                        <img class="h-full"
                             src="{{asset('images/blank-image.svg')}}"
                             alt=""/>
                    </div>
                    <div class="mt-5 max-w-xs">
                        <p class="result-text text-xl text-slate-500 dark:text-slate-400">
                            عکس نقاشی یا اتودت رو انتخاب کن و دکمه بزن بریم رو فشار بده.
                        </p>
                    </div>
                </div>
            </div>

            <div class="js-lightbox-gallery gap-7 main-image-area hidden">
                <div class="relative group">
                    <img class="rounded-lg main-image"
                         src=""
                         alt="">
                    <div
                        class="absolute bottom-4 end-4 flex translate-x-2 translate-y-2 flex-col gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                        <a class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                           href="" download="" title="دانلود عکس">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor" class="h-4">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                            </svg><!-- arrow-down-tray - outline - heroicons -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<template id="image_result">
    <div id="image-result" class="image-result group relative">
        <img id="image-result-img"
            class="image-result-img aspect-[4/3] rounded-md border border-slate-200 object-cover dark:border-slate-800"
            src="" alt="">
        <div
            class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
            <a href="" data-pswp-width="1000" data-pswp-height="1000" id="view-image"
               class="view-image js-lightbox-toggle flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                     stroke-width="2"
                     stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0"></path>
                    <path
                        d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6"></path>
                </svg>
            </a>
            <a id="image-result-download" class="image-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
               href="../images/generated/hyper-realistic-modern-sofa-with-pastel-colors-and-white-background-cinematic-light.jpg"
               download="">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                     stroke="currentColor" class="h-4">
                    <path stroke-linecap="round" stroke-linejoin="round"
                          d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                </svg><!-- arrow-down-tray - outline - heroicons -->
            </a>
            <a id="image-result-delete" class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
               href="#" onclick="return deleteImage(this)">
                <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                </svg>
            </a>
        </div>
        <div
            class="absolute bottom-0 w-full rounded-b-md bg-gradient-to-b from-transparent via-slate-900 via-70% to-slate-900 p-3 opacity-0 transition-all group-hover:opacity-100">
            <span id="image_prompt" class="image_prompt line-clamp-2 text-xs font-bold text-white"></span>
        </div>
    </div>
</template>

<template id="video_result">
    <div id="video-result" class="video-result group relative">
        <figure
            class="relative mb-3 aspect-[4/3] overflow-hidden rounded-lg shadow-md transition-all group-hover:-translate-y-1 group-hover:scale-105 group-hover:shadow-lg"
            data-lqd-skeleton-el
        >
            <video
                class="lqd-video-result-video h-full w-full object-cover object-center"
                loading="lazy"
            >
                <source id="video-result-source"
                    src=""
                    loading="lazy"
                    type="video/mp4"
                >
            </video>
            <div
                class="absolute bottom-7 start-4 flex translate-x-2 translate-y-2 gap-px rounded-md bg-slate-200 opacity-0 transition-all duration-300 group-hover:translate-x-0 group-hover:translate-y-0 group-hover:opacity-100 dark:bg-slate-700">
                <a id="video-result-download" class="lqd-video-result-download flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                   href="" download="" title="دانلود فیلم">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                         viewBox="0 0 24 24"
                         stroke-width="1.5" stroke="currentColor" class="h-4">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"></path>
                    </svg><!-- arrow-down-tray - outline - heroicons -->
                </a>
                <a id="view-video" class="lqd-video-result-play flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                   href="" data-fslightbox="video-gallery" title="پخش فیلم">
                    <svg class="h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2"
                         stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                        <path
                            d="M6 4v16a1 1 0 0 0 1.524 .852l13 -8a1 1 0 0 0 0 -1.704l-13 -8a1 1 0 0 0 -1.524 .852z"
                            stroke-width="0" fill="currentColor"></path>
                    </svg>
                </a>
                <a id="video-result-delete" class="image-result-delete flex h-8 w-8 items-center justify-center bg-white text-slate-600 transition-all first:rounded-t-md last:rounded-b-md hover:bg-green-500 hover:text-white dark:bg-slate-900 dark:text-white hover:dark:bg-green-600"
                   href="#" onclick="return deleteImage(this)">
                    <svg width="10" height="9" viewBox="0 0 10 9" fill="var(--lqd-heading-color)"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M9.08789 1.49609L5.80664 4.75L9.08789 8.00391L8.26758 8.82422L4.98633 5.57031L1.73242 8.82422L0.912109 8.00391L4.16602 4.75L0.912109 1.49609L1.73242 0.675781L4.98633 3.92969L8.26758 0.675781L9.08789 1.49609Z"></path>
                    </svg>
                </a>
            </div>
        </figure>
    </div>
</template>

