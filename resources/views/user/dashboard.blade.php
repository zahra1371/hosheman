@extends('layout.landing.app')

@section('title')
    {{__('HosheMan')}} |  {{__('Dashboard')}}
@endsection

@section('content')
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="-mx-3 mb-7 flex items-center justify-between">
                <div class="px-3">
                    <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                        <li>
                            <h2 class="mb-2 text-xl font-bold text-slate-700 dark:text-white">
                                <a href="{{route('user.dashboard')}}" class="text-blue-500 hover:text-blue-700">
                                    {{__('Dashboard')}} </a>
                            </h2>
                        </li>
                    </ul>
                </div>
                <div class="px-3">
                    <a href="{{route('aitools.list')}}"
                       class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                        {{__('AI List')}} </a>
                </div>
            </div>
            <!-- Page Head -->
            <div class="-m-3 flex flex-wrap">
                <div class="w-full p-3 xs:w-1/2 lg:w-1/4">
                    <div class="h-full rounded-md border border-slate-800 bg-slate-800 p-5">
                        <div class="relative isolate h-full flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="mb-auto">
                                @if(session()->has('locale') && session()->get('locale') === 'en')
                                    <p class="mt-2 text-sm font-bold text-white inline-block">{{__('Dear')}} {{auth()->user()->name}} </p>
                                @else
                                    <p class="mt-2 text-sm font-bold text-white inline-block"> {{auth()->user()->name}} {{__('Dear')}}</p>
                                @endif
                                <h6 class="w-max bg-gradient-to-r from-blue-300 to-pink-500 bg-clip-text text-xl font-bold text-transparent inline-block">
                                    {{__('Welcome')}} </h6>
                            </div>
                            <div class="mt-4 gap-x-6">
                                <div>
                                    <div
                                        class="mt-1 text-xs text-slate-300 inline-block">{{__('Wallet Balance:')}}</div>
                                    <div
                                        class="mt-1 text-base font-bold text-slate-100 inline-block"> {{number_format((int) Auth::user()->wallet_balance)}} {{__('Toman')}}</div>
                                </div>
                            </div>
                            <a href="{{route('wallet.charge')}}"
                               class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-1 text-sm font-medium text-white transition-all hover:bg-blue-800 mt-1">{{__('recharge wallet')}}</a>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
                <div class="w-full p-3 xs:w-1/2 lg:w-1/4">
                    <div
                        class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="h-full">
                                    <path class="fill-blue-300"
                                          d="M3 5C3 3.34315 4.34315 2 6 2H15.7574C16.553 2 17.3161 2.31607 17.8787 2.87868L20.1213 5.12132C20.6839 5.68393 21 6.44699 21 7.24264V19C21 20.6569 19.6569 22 18 22H6C4.34315 22 3 20.6569 3 19V5Z"/>
                                    <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7 11C7 10.4477 7.44772 10 8 10H16C16.5523 10 17 10.4477 17 11C17 11.5523 16.5523 12 16 12H8C7.44772 12 7 11.5523 7 11Z"/>
                                    <path class="fill-blue-600" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M7 15C7 14.4477 7.44772 14 8 14H12C12.5523 14 13 14.4477 13 15C13 15.5523 12.5523 16 12 16H8C7.44772 16 7 15.5523 7 15Z"/>
                                    <path class="fill-blue-600"
                                          d="M17.7071 2.70711L20.2929 5.29289C20.7456 5.74565 21 6.35971 21 7H18C16.8954 7 16 6.10457 16 5V2C16.6403 2 17.2544 2.25435 17.7071 2.70711Z"/>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">{{__('Robot')}}</span>
                                    <span
                                        class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">{{__('AI Chat')}}</span>
                                </p>
                                <a href="{{route('chatbot','hosheman')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">{{__("I will answer all your questions!")}}</a>
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
                <div class="w-full p-3 xs:w-1/2 lg:w-1/4">
                    <div
                        class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
                                     class="h-full">
                                    <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M19.7866 14C20.9581 14 22 13.1714 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C13.1714 22 14 20.9581 14 19.7866V18C14 15.7909 15.7909 14 18 14H19.7866Z"/>
                                    <circle class="fill-blue-600" cx="11.5" cy="6" r="1.5"/>
                                    <circle class="fill-blue-600" cx="17.5" cy="9.5" r="1.5"/>
                                    <circle class="fill-blue-600" cx="6.5" cy="10" r="1.5"/>
                                    <circle class="fill-blue-600" cx="8" cy="16.5" r="1.5"/>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">{{__('Robot')}}</span>
                                    <span
                                        class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">{{__('Image Creator')}}</span>
                                </p>

                                <a href="{{route('aitools','graphics')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">{{__('I turn your ideas into images!')}}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 xs:w-1/2 lg:w-1/4">
                    <div
                        class="h-full rounded-md border border-slate-200 bg-white p-5 dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="absolute end-0 top-0"></div>
                            <div class="relative -z-10 -mb-8 h-16 opacity-30">
                                <svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" class="h-full">
                                    <path class="fill-blue-300" fill-rule="evenodd" clip-rule="evenodd"
                                          d="M13 5H16V7H13V5ZM18 7V5H19C19.5523 5 20 5.44771 20 6V7H18ZM13 9H17H20V15H17H13V9ZM16 17H13V19H16V17ZM18 19V17H20V18C20 18.5523 19.5523 19 19 19H18ZM11 5H8V7H11V5ZM11 9H7H4V15H7H11V9ZM11 17H8V19H11V17ZM12 21H19C20.6569 21 22 19.6569 22 18V16V8V6C22 4.34315 20.6569 3 19 3H5C3.34315 3 2 4.34315 2 6V8V16V18C2 19.6569 3.34315 21 5 21H12ZM6 5H5C4.44772 5 4 5.44772 4 6V7H6V5ZM4 18V17H6V19H5C4.44772 19 4 18.5523 4 18Z"></path>
                                    <rect class="fill-blue-600" x="4" y="9" width="7" height="6"></rect>
                                    <rect class="fill-blue-600" x="13" y="9" width="7" height="6"></rect>
                                </svg>
                            </div>
                            <div class="ms-1 mt-2">
                                <p class="-ms-0.5 flex items-baseline gap-x-2">
                                    <span class="text-4xl font-bold tracking-tight text-blue-500">{{__('Robot')}}</span>
                                    <span
                                        class="text-sm font-semibold leading-6 tracking-wide text-slate-500 dark:text-slate-400">{{__('Video Creator')}}</span>
                                </p>

                                <a href=""
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">{{__('I turn your photos into videos! (Coming soon)')}}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full p-3 ">
                    <div
                        class="h-full rounded-md border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                        <div class="relative isolate flex flex-col">
                            <div class="flex items-center justify-between gap-x-4 p-5">
                                <h6 class="text-md font-bold text-slate-700 dark:text-white"> {{__('Recent Documents')}} </h6>
                                <a href="{{route('documents')}}"
                                   class="text-sm font-medium text-blue-600 hover:text-blue-800">{{__('View All')}}</a>
                            </div>
                            <div
                                class="overflow-x-auto scrollbar-thin scrollbar-track-slate-200 scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                                <table
                                    class="w-full table-auto border-collapse border-t border-slate-200 text-sm dark:border-slate-800">
                                    <thead class="text-slate-600 dark:text-slate-200">
                                    <tr>
                                        <th class="px-5 py-2 text-start">{{__('Document')}}</th>
                                        <th class="px-5 py-2 text-start">{{__('Date')}}</th>
                                        <th class="sticky end-0 bg-white dark:bg-slate-950"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($documents)>0)
                                        @foreach ($documents as $entry)
                                            @php
                                                $url='images/'.$entry->tool->category->name.'.png';
                                                $url=''
                                            @endphp
                                            @if ($entry->tool != null)
                                                <tr>
                                                    <td class="border-t border-slate-200 px-5 py-3 dark:border-slate-800">
                                                        <div class="flex items-center">
                                                            <span class="block h-6 w-6">
                                                                <img src="{{asset($url)}}" alt="">
                                                            </span>
                                                            <div class="ms-3">
                                                            <span
                                                                class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{ __($entry->tool->name) }} </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 px-5 py-3 dark:border-slate-800">
                                                        @if(session()->has('locale') && session()->get('locale') === 'en')
                                                            <span
                                                                class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200">{{date('Y-m-d',strtotime($entry->created_at))}}</span>
                                                            <span
                                                                class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">{{date('H:i:s',strtotime($entry->created_at))}}</span>
                                                        @else
                                                            <span
                                                                class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200">{{jdate_from_gregorian($entry->created_at)}}</span>
                                                            <span
                                                                class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">{{jdate_from_gregorian($entry->created_at,'H:i:s')}}</span>
                                                        @endif
                                                    </td>
                                                    <td class="sticky end-0 border-t border-slate-200 bg-white px-5 py-3 dark:border-slate-800 dark:bg-slate-950">
                                                        <ul class="flex justify-end gap-2">
                                                            <li>
                                                                <a href="{{route('document.view',$entry->id)}}"
                                                                   class="inline-flex items-center justify-center rounded-full bg-slate-200 p-2 text-slate-600 transition-all hover:bg-blue-600 hover:text-white dark:bg-slate-800 dark:text-slate-200 hover:dark:bg-blue-600 hover:dark:text-white">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                                                                         viewBox="0 0 24 24" stroke-width="1.5"
                                                                         stroke="currentColor" class="h-3 w-3">
                                                                        <path stroke-linecap="round"
                                                                              stroke-linejoin="round"
                                                                              d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z"/>
                                                                        <path stroke-linecap="round"
                                                                              stroke-linejoin="round"
                                                                              d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                                                    </svg><!-- eye - outline - heroicons  -->
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="2">هنوز تجربه ای از کار با سایت نداشتی :)</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!-- table -->
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!-- col -->
            </div>
        </div>
        <!-- container -->
    </div>

    <div id="firstLoginModal" class="modal hidden fixed z-10 inset-0 overflow-y-auto"
         style="background-color: rgba(96,96,96,0.6)">
        <div class="relative z-10 mt-8" aria-labelledby="modal-title" role="dialog" aria-modal="true">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

            <div class="fixed inset-0 z-10 w-screen overflow-y-auto">
                <div
                    class="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
                    <div
                        class="lg:w-1/2 w-full mt-32 relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all sm:my-8">
                        <div class="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                            <div class="sm:flex sm:items-start">
                                <div class="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                                    <p> {{auth()->user()?auth()->user()->name:''}} عزیز، به سایت <span
                                            class="text-danger font-bold"> هوش من</span> خوش
                                        اومدی ❤️</p>
                                    <p>برای اینکه تجربه بهتری داشته باشی و بتونی سایت رو تست کنی 15 هزار تومان کیف پولت
                                        رو شارژ کردیم.</p>
                                    <p>امیدوارم تجربه خوبی برات باشه.</p>
                                    <button
                                        class="inline-flex w-full items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800 mt-4"
                                        id="close-first-login" type="button" data-bs-dismiss="modal"
                                        aria-label="Close">بزن بریم!
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        @if(auth()->user() && !auth()->user()->has_seen_popup)
        $('#firstLoginModal').removeClass('hidden')
        @endif
        $('#close-first-login').click(function () {
            $.ajax({
                type: "get",
                url: '/user/change-first-login',
                success: function (response) {
                    if (response.status === 200 || response.status === 100) {
                        window.location.href=response.url
                    } else if (response.status == 500)
                        console.log(response)
                        swal(__('error',$('#locale').val()), response['msg'], 'error');
                },
                error: function (xhr) {

                }
            })
        })
    </script>
@endsection
