@extends('layout.landing.app')

@section('title')
    {{__('HosheMan')}} | {{__('Profile')}}
@endsection

@section('content')
    <style>
        /*Overrides for Tailwind CSS */

        /*Form fields*/
        .dataTables_wrapper select,
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;
            /*text-gray-700*/
            padding-left: 1rem;
            /*pl-4*/
            padding-right: 1rem;
            /*pl-4*/
            padding-top: .5rem;
            /*pl-2*/
            padding-bottom: .5rem;
            /*pl-2*/
            line-height: 1.25;
            /*leading-tight*/
            border-width: 2px;
            /*border-2*/
            border-radius: .25rem;
            border-color: #edf2f7;
            /*border-gray-200*/
            background-color: #edf2f7;
            /*bg-gray-200*/
        }

        /*Row Hover*/
        table.dataTable.hover tbody tr:hover,
        table.dataTable.display tbody tr:hover {
            background-color: #ebf4ff;
            /*bg-indigo-100*/
        }

        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 700;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Add padding to bottom border */
        table.dataTable.no-footer {
            border-bottom: 1px solid #e2e8f0;
            /*border-b-1 border-gray-300*/
            margin-top: 0.75em;
            margin-bottom: 0.75em;
        }

        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before,
        table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before {
            background-color: #667eea !important;
            /*bg-indigo-500*/
        }
    </style>
    <div class="relative px-3 py-10">
        <div class="container px-3">
            <div class="flex">
                <div class="hidden flex-shrink-0 md:block md:w-48 lg:w-72">
                    <div
                        class="relative inline-flex w-full flex-shrink-0 overflow-hidden rounded-lg outline outline-2 outline-offset-2 outline-slate-300 dark:outline-slate-700">
                        <img src="{{asset('images/nouser.webp')}}" alt=""/>
                    </div>
                </div>
                <div class="flex-grow-1 w-full md:ms-6 lg:ms-8">
                    <h2 class="mb-4 mt-2 text-xl font-bold text-slate-700 dark:text-white"> {{__('My Profile')}} </h2>
                    <div
                        class="w-full rounded-lg border border-slate-200 bg-white dark:border-slate-800 dark:bg-slate-950">
                        <ul class="tab-nav flex border-b border-slate-200 px-3 dark:border-slate-800">
                            <li class="tab-item">
                                <a href="#" data-target="#Overview"
                                   class="tab-toggle active mx-3 -mb-[1px] flex items-center border-b-2 border-b-transparent py-3 text-sm font-medium text-slate-600 focus-visible:outline-none dark:text-slate-200 [&.active]:border-blue-600 [&.active]:text-blue-600 [&.active]:dark:text-blue-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5" stroke="currentColor" class="h-5 px-2 xs:px-0">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                    </svg><!-- user-circle - outline - heroicons -->
                                    <span class="ms-2 hidden xs:block"> {{__('My info')}} </span>
                                </a>
                            </li>
                            <li class="tab-item">
                                <a href="#" data-target="#Billings"
                                   class="tab-toggle mx-3 -mb-[1px] flex items-center border-b-2 border-b-transparent py-3 text-sm font-medium text-slate-600 focus-visible:outline-none dark:text-slate-200 [&.active]:border-blue-600 [&.active]:text-blue-600 [&.active]:dark:text-blue-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-5">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h3.75M9 15h3.75M9 18h3.75m3 .75H18a2.25 2.25 0 0 0 2.25-2.25V6.108c0-1.135-.845-2.098-1.976-2.192a48.424 48.424 0 0 0-1.123-.08m-5.801 0c-.065.21-.1.433-.1.664 0 .414.336.75.75.75h4.5a.75.75 0 0 0 .75-.75 2.25 2.25 0 0 0-.1-.664m-5.8 0A2.251 2.251 0 0 1 13.5 2.25H15c1.012 0 1.867.668 2.15 1.586m-5.8 0c-.376.023-.75.05-1.124.08C9.095 4.01 8.25 4.973 8.25 6.108V8.25m0 0H4.875c-.621 0-1.125.504-1.125 1.125v11.25c0 .621.504 1.125 1.125 1.125h9.75c.621 0 1.125-.504 1.125-1.125V9.375c0-.621-.504-1.125-1.125-1.125H8.25ZM6.75 12h.008v.008H6.75V12Zm0 3h.008v.008H6.75V15Zm0 3h.008v.008H6.75V18Z"></path>
                                    </svg>
                                    <span class="ms-2 hidden xs:block"> {{__('Orders')}} </span>
                                </a>
                            </li>
                            <li class="tab-item">
                                <a href="#" data-target="#Affiliates"
                                   class="tab-toggle mx-3 -mb-[1px] flex items-center border-b-2 border-b-transparent py-3 text-sm font-medium text-slate-600 focus-visible:outline-none dark:text-slate-200 [&.active]:border-blue-600 [&.active]:text-blue-600 [&.active]:dark:text-blue-600">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-5">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 19.128a9.38 9.38 0 0 0 2.625.372 9.337 9.337 0 0 0 4.121-.952 4.125 4.125 0 0 0-7.533-2.493M15 19.128v-.003c0-1.113-.285-2.16-.786-3.07M15 19.128v.106A12.318 12.318 0 0 1 8.624 21c-2.331 0-4.512-.645-6.374-1.766l-.001-.109a6.375 6.375 0 0 1 11.964-3.07M12 6.375a3.375 3.375 0 1 1-6.75 0 3.375 3.375 0 0 1 6.75 0Zm8.25 2.25a2.625 2.625 0 1 1-5.25 0 2.625 2.625 0 0 1 5.25 0Z"></path>
                                    </svg>
                                    <span class="ms-2 hidden xs:block"> {{__('Affiliates')}} </span>
                                </a>
                            </li>
                        </ul>

                        <!-- tab-nav -->
                        <div class="tab-content">
                            <div class="tab-panel active hidden [&.active]:block" id="Overview">
                                <div class="px-6 pb-5 pt-4">
                                    <div class="mb-2 mt-3 flex items-center justify-between md:mt-5">
                                        <div class="w-2/3">
                                            <h2 class="text-lg font-bold text-slate-700 dark:text-white"> {{__('Personal info')}} </h2>
                                        </div>
                                    </div>
                                    <form id="user-update-form" action="{{route('profile.update')}}" method="post">
                                        @csrf
                                        <div class="-mx-3 flex flex-wrap items-center">
                                            <div class="w-full px-3 sm:w-1/3">
                                                <div class="py-2">
                                                    <label for="name"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__('Name')}} </label>
                                                    <div class="relative isolate flex w-full">
                                                        <input value="{{auth()->user()->name}}" id="name" name="name"
                                                               class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"/>
                                                    </div>
                                                    <span class="error error-name text-sm text-rose-600"></span>
                                                </div>
                                            </div>
                                            <!-- col -->
                                            <div class="w-full px-3 sm:w-1/3">
                                                <div class="py-2">
                                                    <label for="surname"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__('Surname')}} </label>
                                                    <div class="relative isolate flex w-full">
                                                        <input value="{{auth()->user()->surname}}" id="surname" name="surname"
                                                               class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"/>
                                                    </div>
                                                    <span class="error error-surname text-sm text-rose-600"></span>
                                                </div>
                                            </div>
                                            <!-- col -->
                                            <div class="w-full px-3 sm:w-1/3">
                                                <div class="py-2">
                                                    <label for="mobile"
                                                           class="mb-2 inline-flex cursor-pointer text-sm font-bold text-slate-600 dark:text-slate-200">{{__('Mobile')}} </label>
                                                    <div class="relative isolate flex w-full">
                                                        <input value="{{auth()->user()->mobile}}" disabled id="mobile"
                                                               class="z-10 w-full rounded-md border-slate-200 bg-white py-2 text-sm/[1.125rem] text-slate-600 placeholder:text-slate-400 focus:border-slate-200 focus:shadow-none focus:outline-none disabled:bg-slate-100 disabled:text-slate-400 dark:border-slate-800 dark:bg-slate-950 dark:text-slate-200 placeholder:dark:text-slate-500"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- col -->
                                            <div class="w-full px-3 py-2">
                                                <button type="button" id="user_edit_button"
                                                        class="inline-flex items-center justify-center gap-3 rounded-md bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                                    {{__('Save')}} </button>
                                            </div>
                                            <!-- col -->
                                        </div>
                                    </form>
                                    <!-- row -->
                                    <hr>
                                    <div class="mb-2 mt-3 flex items-center justify-between md:mt-5">
                                        <div class="w-2/3">
                                            <h2 class="text-lg font-bold text-slate-700 dark:text-white"> {{__('Activity Log')}} </h2>
                                        </div>
                                    </div>
                                    <div class="flex max-w-xl flex-col">
                                    </div>
                                </div>
                            </div>
                            <!-- panel -->
                            <div class="tab-panel hidden [&.active]:block" id="Billings">
                                <div class="px-6 pb-5 pt-4">
                                    <div
                                        class="overflow-x-auto scrollbar-thin scrollbar-track-slate-200 scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                                        <table
                                            class="w-full table-auto border-collapse border-b border-t border-slate-200 text-sm dark:border-slate-800">
                                            <thead class="text-slate-600 dark:text-slate-200">
                                            <tr>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">
                                                    #
                                                </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Order Id')}} </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Amount')}} </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Transaction Type')}} </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Date')}} </th>
                                            </tr>
                                            </thead>
                                            <tbody id="user-body">
                                            @if(count($orders)>0)
                                                @php $i=0 @endphp
                                                @foreach($orders as $item)
                                                    <tr>
                                                        <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                            <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400">{{++$i}} </span>
                                                    </span>
                                                            </div>
                                                        </td>
                                                        <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                            <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400"> {{$item->transaction_id}} </span>
                                                    </span>
                                                            </div>
                                                        </td>
                                                        <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                            <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{number_format($item->amount)}} </span>
                                                    </span>
                                                            </div>
                                                        </td>
                                                        <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                            <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{$item->type === 'self'?'-':__('Referral')}} </span>
                                                    </span>
                                                            </div>
                                                        </td>
                                                        <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                            @if(session()->has('locale') && session()->get('locale') === 'en')
                                                                <span
                                                                    class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200">{{date('Y-m-d',strtotime($item->created_at))}}</span>
                                                                <span
                                                                    class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">{{date('H:i:s',strtotime($item->created_at))}}</span>
                                                            @else
                                                                <span
                                                                    class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{jdate_from_gregorian($item->created_at)}} </span>
                                                                <span
                                                                    class="block text-xs font-medium text-slate-500 dark:text-slate-400"> {{jdate_from_gregorian($item->created_at,'H:i:s')}} </span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5" class="text-center py-3 text-slate-600">{{__('You do not have any transaction yet.')}}</td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                        <!-- table -->
                                        @if(count($orders)>0)
                                            <div class="mt-4" id="pagination-links">
                                                {!! $orders->links('pagination::tailwind') !!}
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <!-- panel -->
                            <div class="tab-panel hidden [&.active]:block" id="Affiliates">
                                <div class="px-6 pb-5 pt-4">
                                    <div
                                        class="overflow-x-auto scrollbar-thin scrollbar-track-slate-200 scrollbar-thumb-slate-600 dark:scrollbar-track-slate-800">
                                        <div class="mb-5 empty:mb-0">
                                            <div
                                                class="flex items-center rounded-md bg-sky-100 px-4 py-3 dark:bg-sky-950">
                                                <div class="ps-3 text-center">
                                                    <h6 class="mb-10 w-10/12 text-center py-2">
                                                        {{ __('Invite your friends and earn lifelong recurring commissions from every purchase they make') }}
                                                        🎁
                                                    </h6>
                                                    <h6 class="text-sm font-bold text-slate-700 dark:text-white">
                                                        {{__('Your Invite Link')}}
                                                    </h6>
                                                    <p class="text-xs py-1 text-slate-600 dark:text-slate-200">{{route('register','code='.auth()->user()->referral_code)}}</p>
                                                    <button class="btn text-[11px] rounded bg-blue-200 text-blue-600 p-2"
                                                            onclick="copyText('{{route('register','code='.auth()->user()->referral_code)}}',$(this))">
                                                        {{__('Copy Link')}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <table
                                            class="w-full table-auto border-collapse border-b border-t border-slate-200 text-sm dark:border-slate-800">
                                            <thead class="text-slate-600 dark:text-slate-200">
                                            <tr>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950">
                                                    #
                                                </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Name')}} </th>
                                                <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:bg-white last:ps-2 last:dark:bg-slate-950"> {{__('Date')}} </th>
                                            </tr>
                                            </thead>
                                            <tbody id="user-body">
                                            @php $i=0 @endphp
                                            @foreach(auth()->user()->referredUsers()->orderBy('created_at','desc')->get() as $affiliate)
                                                <tr>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400">{{++$i}} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400"> {{$affiliate->name}} {{$affiliate->surname}} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        @if(session()->has('locale') && session()->get('locale') === 'en')
                                                            <span
                                                                class="block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200">{{date('Y-m-d',strtotime($affiliate->created_at))}}</span>
                                                            <span
                                                                class="block text-[11px] font-medium text-slate-500 dark:text-slate-400">{{date('H:i:s',strtotime($affiliate->created_at))}}</span>
                                                        @else
                                                            <span
                                                                class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> {{jdate_from_gregorian($affiliate->created_at)}} </span>
                                                            <span
                                                                class="block text-xs font-medium text-slate-500 dark:text-slate-400"> {{jdate_from_gregorian($affiliate->created_at,'H:i:s')}} </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <!-- table -->
                                        <div class="mt-4" id="pagination-links">
                                            {!! $orders->links('pagination::tailwind') !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-content -->
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '#pagination-links a', function (event) {
                event.preventDefault();

                let page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });

            function fetch_data(page) {
                $.ajax({
                    url: "/dashboard/user/settings?page=" + page,
                    success: function (data) {
                        $('#user-body').html('');
                        let i = 0
                        $.each(data.orders.data, function (index, order) {
                            $('#user-body').append(`
                                <tr>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400">${++i} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400"> ${order.order_id} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="block w-max text-sm font-bold bg-gradient-to-r from-blue-600 to-pink-500 bg-clip-text text-transparent"> ${order.plan.name} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> ${new Intl.NumberFormat().format(order.plan.total_words)} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> ${new Intl.NumberFormat().format(order.plan.total_images)} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <div class="flex items-center">
                                                    <span class="flex flex-col gap-x-1 text-slate-400">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> ${new Intl.NumberFormat().format(order.plan.price)} </span>
                                                    </span>
                                                        </div>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <span
                                                            class="mb-0.5 block whitespace-nowrap text-xs font-bold text-slate-600 dark:text-slate-200"> ${new Date(order.created_at).toLocaleDateString('fa-IR-u-nu-latn', {
                                year: 'numeric',
                                month: 'long',
                                day: 'numeric'
                            })} </span>
                                                        <span
                                                            class="block text-xs font-medium text-slate-500 dark:text-slate-400"> ${new Date(order.created_at).toLocaleTimeString('en-US', {
                                hour12: false,
                                timeZone: "Asia/Tehran"
                            })} </span>
                                                    </td>
                                                    <td class="border-t border-slate-200 py-3 pe-5 ps-5 last:sticky last:end-0 last:bg-white last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                                        <span
                                                            class="inline-flex rounded-full px-2 text-[11px] font-bold capitalize bg-emerald-100 dark:bg-emerald-950 text-emerald-500   ">completed</span>
                                                    </td>
                                                </tr>
                            `);
                        });

                        $('#pagination-links').html(data.links);
                    }
                });
            }

            $('#user_edit_button').click(function () {
                ajaxRequests($('#user-update-form'),$(this))
            })
        });
    </script>
@endsection
