@extends('layout.landing.app')

@section('title')
{{__('HosheMan')}} | {{__('Pricing')}}
@endsection
@section('content')
    <section class="relative bg-slate-100 py-10 dark:bg-slate-950">
        <div class="container px-3">
            <div class="flex flex-col items-center justify-center">
                <div class="w-full md:w-5/12">
                    <div class="text-center">
                        <h2 class="mb-2 text-3xl font-bold text-slate-700 dark:text-white"> {{__('Pricing')}} </h2>
                        <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                            <li>{{__('Pricing')}}</li>
                            <li class="mt-0.5 inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="h-2 w-2">
                                    <path fill="currentColor" d="M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z" />
                                </svg>
                            </li>
                            <li>
                                <a href="{{route('home')}}" class="text-blue-500 hover:text-blue-700"> {{__('Home')}} </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- container -->
        <div class="absolute bottom-0 start-0 h-[1px] w-full animate-[animateGradient_5s_ease-in-out_2] bg-gradient-to-r from-transparent from-10% via-blue-100 to-transparent to-90% dark:via-blue-950"></div>
    </section><!-- section -->
    <section class="overflow-hidden bg-white pb-2 pt-10 dark:bg-slate-900">
        <div class="container px-3 -mb-8">
            <div class="relative rounded-t-xl bg-gradient-to-b from-white to-slate-50 p-6 !pb-0 dark:border-gray-800 dark:from-slate-900 dark:to-slate-950 sm:p-10">
                <div class="-m-4 flex flex-wrap items-center">
                    <div class="w-full p-4">
                        <div>
                <div class="pb-6 text-center">
                    <p class="text-base/7 font-semibold text-slate-600 dark:text-slate-300"> {{__('HosheMan price list')}} </p>
                </div>
                <p class="text-justify text-base/7">
                    {{__('With this platform, you have access to the latest version of the GPT model, allowing you to leverage advanced AI for answering questions, content generation, data analysis, and more.')}}
                    {{__('Image generation is available using two models: DALL·E 3 and Stable Diffusion, with costs varying depending on the chosen model.')}}
                </p>
                <p class="text-justify text-base/7 mt-2">{{__('Our business model is Pay-as-you-go, meaning you only pay for what you use. The costs are calculated based on the number of input and output tokens, according to below table.')}}</p>
                <h3 class="font-semibold mt-3">{{__('Example of Chat Cost Calculation:')}}</h3>
                <p class="text-base/7 mt-1">{{__('Suppose you send a chat message containing 300 input tokens, and the GPT-4o model generates a response with 700 tokens.')}}</p>
                <ul style="list-style-type: square; margin-right: 2rem; margin-left: 2rem">
                    <li class="text-base/7">{{__('Input cost: 300 × 1 = 300 Toman')}}</li>
                    <li class="text-base/7">{{__('Output cost: 700 × 3 = 2100 Toman')}}</li>
                    <li class="text-base/7">{{__('Total cost for this chat: 2400 Toman')}}</li>
                </ul>
                <p class="text-justify text-base/7 mt-4"><strong>{{__('💡 Note that each Persian word is considered between 1.5 to 3 tokens.')}}</strong></p>
                <p class="text-justify text-base/7 mt-1"><strong>{{__('💡 To reduce costs, it is recommended to start a new chat for new questions.
Since in a continuous conversation, the model processes the chat history, increasing the number of tokens and overall cost.')}}</strong></p>
                <p class="text-justify text-base/7 mt-1"><strong>{{__('💡 The minimum recharge amount to use the service is 50,000 Tomans.')}}</strong></p>
                <p class="text-justify text-base/7 mt-1"><strong>{{__('💡 If you have any questions, contact our online support or WhatsApp.')}}</strong></p>
            </div>
                    </div>
                </div>
                <div class="text-center mt-8">
                    <h3 class="text-center font-semibold text-2xl mb-3">{{__('Pricing Table')}} <small class="text-xs text-rose-600">({{__('Prices are in Toman')}})</small></h3>
                    <table
                        class="lg:w-1/2 mx-auto mb-4 table-auto border-collapse border border-slate-200 text-sm dark:border-slate-800">
                        <thead class="text-slate-600 dark:text-slate-200">
                        <tr>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:ps-2 last:dark:bg-slate-950">{{__('Service Name')}} </th>
                            <th class="py-2 pe-5 ps-5 text-start last:sticky last:end-0 last:ps-2 last:dark:bg-slate-950">
                                {{__('Service Price')}} </th>
                        </tr>
                        </thead>
                        <tbody id="documents-body">
                        @foreach($prices as $item)
                            <tr>
                                <td class="border-t border-slate-200 text-start py-3 pe-5 ps-5 last:sticky last:end-0 last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                            <span class="text-xs text-slate-500 dark:text-slate-400">
                                                <strong>{{__($item->full_name)}} </strong></span>
                                </td>
                                <td class="border-t border-slate-200 text-start py-3 pe-5 ps-5 last:sticky last:end-0 last:ps-2 dark:border-slate-800 last:dark:bg-slate-950">
                                            <span class="text-xs text-slate-500 dark:text-slate-400">
                                                @if($item->service_name === 'gpt-word-input' || $item->service_name === 'gpt-word')
                                                    <strong>{{number_format($item->rate_per_unit * 1000)}} </strong>
                                                @else
                                                    <strong>{{number_format($item->rate_per_unit)}} </strong>
                                                @endif
                                            </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section><!-- section -->
@endsection
