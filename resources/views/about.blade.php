@extends('layout.landing.app')

@section('title')
{{__('HosheMan')}} | {{__('About Us')}}
@endsection
@section('content')
    <section class="relative bg-slate-100 py-10 dark:bg-slate-950">
        <div class="container px-3">
            <div class="flex flex-col items-center justify-center">
                <div class="w-full md:w-5/12">
                    <div class="text-center">
                        <h2 class="mb-2 text-3xl font-bold text-slate-700 dark:text-white"> {{__('About Us')}} </h2>
                        <ul class="inline-flex items-center gap-2 text-xs font-medium text-slate-500 dark:text-slate-300">
                            <li>{{__('About Us')}}</li>
                            <li class="mt-0.5 inline-flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="h-2 w-2">
                                    <path fill="currentColor" d="M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z" />
                                </svg>
                            </li>
                            <li>
                                <a href="{{route('home')}}" class="text-blue-500 hover:text-blue-700"> {{__('Home')}} </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- container -->
        <div class="absolute bottom-0 start-0 h-[1px] w-full animate-[animateGradient_5s_ease-in-out_2] bg-gradient-to-r from-transparent from-10% via-blue-100 to-transparent to-90% dark:via-blue-950"></div>
    </section><!-- section -->
    <section class="overflow-hidden bg-white pb-2 pt-10 dark:bg-slate-900">
        <div class="container px-3">
            <div class="pb-6 text-center">
                <p class="text-base/7 font-semibold text-slate-600 dark:text-slate-300"> {{__('Hosheman is the most specialized Iranian AI platform.')}} </p>
            </div>
            <div class="pb-16 md:pb-20">
                <ul class="-m-2 flex flex-wrap items-center justify-center">
                    <li class="p-2">
                        <div class="flex h-14 w-14 items-center justify-center rounded-full border border-gray-100 bg-white p-4 shadow shadow-slate-100 dark:border-gray-900 dark:bg-slate-950 dark:shadow-slate-800 sm:h-20 sm:w-20 sm:p-5">
                            <a href="https://wa.me/989919427053" target="_blank">
                                <img class="w-12" src="{{asset('images/whatsapp.webp')}}" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="p-2">
                        <div class="flex h-14 w-14 items-center justify-center rounded-full border border-gray-100 bg-white p-4 shadow shadow-slate-100 dark:border-gray-900 dark:bg-slate-950 dark:shadow-slate-800 sm:h-20 sm:w-20 sm:p-5">
                            <a href="https://www.instagram.com/hosheman_com?igsh=MWhva25wZG55eXpnbw==" target="_blank">
                                <img class="w-12" src="{{asset('images/instagram.webp')}}" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="p-2">
                        <div class="flex h-14 w-14 items-center justify-center rounded-full border border-gray-100 bg-white p-4 shadow shadow-slate-100 dark:border-gray-900 dark:bg-slate-950 dark:shadow-slate-800 sm:h-20 sm:w-20 sm:p-5">
                            <a href="https://t.me/hoshe_man" target="_blank">
                                <img class="w-12" src="{{asset('images/telegram.webp')}}" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="p-2">
                        <div class="flex h-14 w-14 items-center justify-center rounded-full border border-gray-100 bg-white p-4 shadow shadow-slate-100 dark:border-gray-900 dark:bg-slate-950 dark:shadow-slate-800 sm:h-20 sm:w-20 sm:p-5">
                            <a href="https://eitaa.com/hosheman" target="_blank">
                                <img class="w-12" src="{{asset('images/eitaa.webp')}}" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="p-2">
                        <div class="flex h-14 w-14 items-center justify-center rounded-full border border-gray-100 bg-white p-4 shadow shadow-slate-100 dark:border-gray-900 dark:bg-slate-950 dark:shadow-slate-800 sm:h-20 sm:w-20 sm:p-5">
                            <a href="https://rubika.ir/hoshemann" target="_blank">
                                <img class="w-12" src="{{asset('images/rubika.webp')}}" alt="" />
                            </a>
                        </div>
                    </li>
                </ul>
                <div class="-mx-5 -my-3 flex flex-wrap justify-center pt-14">
                    <div class="w-full px-5 py-3 xs:w-8/12 sm:w-6/12 lg:w-4/12 xl:w-3/12">
                        <div class="text-center">
                            <h6 class="mb-2 text-2xl font-bold text-slate-700 dark:text-white"> {{$usage->total_users + 3000 }} </h6>
                            <p class="text-base/7 text-slate-500 dark:text-slate-400"> {{__('Users')}} </p>
                        </div>
                    </div>
                    <div class="w-full px-5 py-3 xs:w-8/12 sm:w-6/12 lg:w-4/12 xl:w-3/12">
                        <div class="text-center">
                            <h6 class="mb-2 text-2xl font-bold text-slate-700 dark:text-white"> {{number_format($usage->total_words + 10000000)}} </h6>
                            <p class="text-base/7 text-slate-500 dark:text-slate-400"> {{__('Generated Words')}} </p>
                        </div>
                    </div>
                    <div class="w-full px-5 py-3 xs:w-8/12 sm:w-6/12 lg:w-4/12 xl:w-3/12">
                        <div class="text-center">
                            <h6 class="mb-2 text-2xl font-bold text-slate-700 dark:text-white"> {{number_format($usage->total_images + $usage->total_edit_images + 9236)}}  </h6>
                            <p class="text-base/7 text-slate-500 dark:text-slate-400"> {{__('Generated Images')}} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
    <section class="overflow-hidden bg-white dark:bg-slate-900">
        <div class="container px-3">
            <div class="relative rounded-t-xl border border-b-0 border-gray-200 bg-gradient-to-b from-white to-slate-50 p-6 !pb-0 dark:border-gray-800 dark:from-slate-900 dark:to-slate-950 sm:p-10">
                <div class="-m-4 flex flex-wrap items-center">
                    <div class="w-full p-4 lg:w-6/12">
                        <div>
                            <p class="mb-6 text-base/7 text-justify text-slate-500 dark:text-slate-300">
                                {{__('We have come together at HosheMan to turn AI into a simple, understandable, and effective tool for everyone. Our mission is to create solutions that can make your daily life easier, elevate businesses to a higher level, and turn your ideas into reality. HosheMan is not just a platform; it is an intelligent companion to accompany you on the path of innovation and progress. By combining local expertise and advanced global technologies, we aim to align AI with the needs and aspirations of Iranians.')}}
                            </p>
                            <p class="mb-6 text-base/7 text-justify text-slate-500 dark:text-slate-300">
                                {{__('With HosheMan, the gap between dreams and reality becomes shorter. Join us to build a better world with the help of AI.')}}
                            </p>
                            <a href="{{route('chatbot','hosheman')}}" class="inline-flex justify-center rounded-lg bg-blue-600 px-7 py-3 mb-2 text-base font-bold text-white transition-all hover:bg-blue-800"> {{__('Experience it now')}} </a>
                        </div>
                    </div>
                    <div class="w-full self-end p-4 lg:w-6/12">
                        <img class="rounded-t-xl border border-b-0 border-gray-200 shadow-lg shadow-blue-100 dark:hidden dark:border-gray-800 dark:shadow-blue-950" src="{{asset('images/about-us.webp')}}" alt="" />
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
@endsection
