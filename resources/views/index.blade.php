@extends('layout.landing.app')

@section('title')
    {{__('HosheMan')}} | {{__('Home')}}
@endsection
@section('content')
    <div class="justify-center gap-3 sm:gap-5 sm:pt-2 fixed" style="z-index: 1000; top: 20rem">
        <div class="relative">
            <a target="_blank" href="https://www.instagram.com/hosheman_com?igsh=MWhva25wZG55eXpnbw=="
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/instagram.webp')}}" loading="lazy" width="35px" height="35px"
                     alt="instagram logo">
            </a>
        </div>
        <div class="relative">
            <a target="_blank" href="https://t.me/hoshe_man"
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/telegram.webp')}}" loading="lazy" width="35px" height="35px"
                     alt="telegram logo">
            </a>
        </div>
        <div class="relative">
            <a href="https://wa.me/989919427053"
               target="_blank"
               class="peer inline-flex rounded-full px-5 py-2 text-sm font-medium text-white transition-all">
                <img src="{{asset('images/whatsapp.webp')}}" loading="lazy" width="35px" height="35px"
                     alt="whatsapp logo">
            </a>
        </div>
        <ul class="menu-head flex flex-col gap-x-6 px-4 xl:flex-row xl:items-center px-5">
            <li class="menu-item has-sub group relative xl:[&amp;:hover>*.sub-menu]:visible xl:[&amp;:hover>*.sub-menu]:translate-y-0 xl:[&amp;:hover>*.sub-menu]:opacity-100">
                <a href="#"
                   class="has-toggle menu-link flex items-center py-2 text-sm font-medium text-slate-700 group-hover:text-blue-600 group-[.active>]:text-blue-600 hover:text-blue-600 dark:text-slate-100 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600 xl:py-3">
                    <img src="{{asset('images/language.svg')}}" width="35px" height="35px" alt="">
                </a>
                <div
                    class="sub-menu hidden rounded-lg border border-slate-200 bg-white transition-all dark:border-slate-800 dark:bg-slate-950 xl:invisible xl:absolute xl:!block xl:w-40 xl:-translate-y-2 xl:transition-all">
                    <ul class="relative py-2">
                        <li class="menu-item group relative">
                            <a href="language/fa"
                               class="menu-link flex items-center px-4 py-2 text-xs font-medium text-slate-600 group-[.active>]:text-blue-600 hover:text-blue-600 dark:text-slate-200 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600">
                                <span>{{ __('Persian') }}</span>
                            </a>
                        </li>
                        <li class="menu-item group relative">
                            <a href="language/en"
                               class="menu-link flex items-center px-4 py-2 text-xs font-medium text-slate-600 group-[.active>]:text-blue-600 hover:text-blue-600 dark:text-slate-200 group-[.active>]:dark:text-blue-600 hover:dark:text-blue-600">
                                <span>{{ __('English') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>

    <div class="container justify-center mx-3 hidden" id="install-container">
        <div
            class="flex items-center justify-center text-xs gap-2 bg-slate-100 p-2 border border-slate-500 rounded-full fixed"
            style="z-index: 1000; bottom: 1rem; left: 50%; transform: translateX(-50%);">
            <img src="{{asset('images/favicon.png')}}" class="h-10" alt="logo">
            <button id="install-button">{{__('Install App')}}</button>
        </div>
    </div>

    <section
        class="relative overflow-hidden bg-gradient-to-br from-white to-blue-100 py-24 dark:from-slate-950 dark:to-blue-950">
        <div class="container px-3">
            <div class="mx-auto max-w-3xl text-center">
                <div
                    class="mb-3 inline-flex rounded-full bg-gradient-to-r from-blue-600 to-green-500 px-2 py-0.5 text-sm font-medium text-white">
                    {{__('AI')}}
                </div>
                <h1 class="text-2xl font-bold text-slate-800 dark:text-white sm:text-3xl md:text-4xl lg:text-[2.5rem]">
                <span
                    class="mb-3 block text-4xl text-blue-400 md:text-5xl lg:mb-6 lg:text-6xl xl:text-7xl">{{__('HosheMan')}}</span>
                    <span class="leading-loose sm:leading-relaxed md:leading-[1.4]">{{__('Generator of')}}</span>
                    <span
                        class="rounded-lg bg-white px-2 py-2 text-[.8em] text-orange-500 shadow-sm">{{__('Text')}}</span>
                    ،
                    <span
                        class="whitespace-nowrap rounded-lg bg-white px-2 py-2 text-[.8em] text-lime-600 shadow-sm">{{__('Image')}}</span>
                    {{__('And')}}
                    <span
                        class="whitespace-nowrap rounded-lg bg-white px-2 py-2 text-[.8em] text-pink-600 shadow-sm">{{__('Video')}}</span>

                </h1>
                <h6 class="mt-8 inline-block w-max rounded-full bg-gradient-to-r from-purple-500 to-blue-500 bg-clip-text text-lg font-bold text-transparent">
                    {{__('Welcome to the fascinating world of AI')}}</h6>
            </div>
            <ul class="-mb-36 mt-4 flex justify-center gap-7">
                <li class="relative z-10">
                    <a class="-me-7 mt-16 flex -rotate-12 flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/landing-1.webp')}}" loading="lazy" width="225" height="124"
                             alt="user dashboard"/>
                    </a>
                </li>
                <li>
                    <a class="mt-8 flex flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/documents.webp')}}" width="225" height="124" loading="lazy"
                             alt="documents"/>
                    </a>
                </li>
                <li class="relative z-10">
                    <a class="-ms-7 mt-16 flex rotate-12 flex-col rounded-xl bg-white p-2 text-center">
                        <img class="w-56 rounded-t-xl border border-slate-100"
                             src="{{asset('images/profile-image.webp')}}" width="225" height="124"
                             alt="user profile" loading="lazy"/>
                    </a>
                </li>
            </ul>
        </div>
        <div class="absolute bottom-3 z-10 flex w-full justify-center sm:bottom-4">
            <div
                class="group relative inline-block rounded-full bg-white p-px text-sm/6 font-semibold text-slate-600 shadow-2xl shadow-slate-900">
            <span class="absolute inset-0 overflow-hidden rounded-full">
                <span
                    class="absolute inset-0 rounded-full bg-[image:radial-gradient(75%_100%_at_50%_0%,rgba(56,189,248,0.6)_0%,rgba(56,189,248,0)_75%)] opacity-0 transition-opacity duration-500 group-hover:opacity-100"></span>
            </span>
                <span
                    class="absolute -bottom-0 left-[1.125rem] h-px w-[calc(100%-2.25rem)] bg-gradient-to-r from-emerald-400/0 via-emerald-400/90 to-emerald-400/0 transition-opacity duration-500 group-hover:opacity-40"></span>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section class="bg-white py-16 dark:bg-slate-950 lg:py-20">
        <div class="container px-3">
            <div class="pt-0">
                <div class="-m-3 flex flex-wrap items-center justify-center md:-m-6 md:flex-row-reverse">
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <img-comparison-slider
                            class="slider-with-shadows rounded-lg border border-slate-200 dark:border-slate-800">
                            <img slot="first" src="{{asset('images/backgrounds/wrong.webp')}}" width="472" height="472"
                                 loading="lazy" alt="wrong"/>
                            <img slot="second" src="{{asset('images/backgrounds/sample.webp')}}" width="472"
                                 height="472" alt="sample"/>
                            <div slot="handle"
                                 class="relative flex h-14 w-14 items-center justify-center gap-2 rounded-full border-2 border-white bg-slate-300 bg-opacity-60 p-2 shadow-sm backdrop-blur-sm">
                                <div
                                    class="h-0 w-0 border-y-8 border-e-[10px] border-b-transparent border-e-white border-t-transparent"></div>
                                <div
                                    class="h-0 w-0 border-y-8 border-s-[10px] border-b-transparent border-s-white border-t-transparent"></div>
                            </div>
                        </img-comparison-slider>
                    </div>
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <div
                            class="mb-2 inline-flex rounded bg-gradient-to-r from-yellow-600 to-violet-500 px-2 py-0.5 text-[11px] font-medium text-white">
                            {{__('Creating background for a photo')}}
                        </div>
                        <h3 class="mb-4 text-3xl font-bold !leading-tight text-slate-800 dark:text-white sm:text-[2.5rem] md:text-3xl lg:text-[2.5rem]">
                            {{__('Create attractive backgrounds for your products')}}</h3>
                        <p class="text-sm/loose text-slate-500 dark:text-slate-400">{{__('Upload your product photo without any equipment and create an attractive background with just one click.')}}</p>
                        <div class="mt-5">
                            <a href=""
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Try it now!')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-12">
                <div class="-m-3 flex flex-wrap items-center justify-center md:-m-6">
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <img-comparison-slider
                            class="slider-with-shadows rounded-lg border border-slate-200 dark:border-slate-800">
                            <img slot="first" src="{{asset('images/before.webp')}}" width="472" height="342"
                                 loading="lazy" alt="before"/>
                            <img slot="second" src="{{asset('images/after.webp')}}" width="472" height="342"
                                 loading="lazy" alt="after"/>
                            <div slot="handle"
                                 class="relative flex h-14 w-14 items-center justify-center gap-2 rounded-full border-2 border-white bg-slate-300 bg-opacity-60 p-2 shadow-sm backdrop-blur-sm">
                                <div
                                    class="h-0 w-0 border-y-8 border-e-[10px] border-b-transparent border-e-white border-t-transparent"></div>
                                <div
                                    class="h-0 w-0 border-y-8 border-s-[10px] border-b-transparent border-s-white border-t-transparent"></div>
                            </div>
                        </img-comparison-slider>
                    </div>
                    <div class="w-full p-3 sm:w-4/5 md:w-1/2 md:p-6 lg:w-2/5">
                        <div
                            class="mb-2 inline-flex rounded bg-gradient-to-r from-blue-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                            {{__('Creating images')}}
                        </div>
                        <h3 class="mb-4 text-3xl font-bold !leading-tight text-slate-800 dark:text-white sm:text-[2.5rem] md:text-3xl lg:text-[2.5rem]">
                            {{__('Turn your ideas into images with the powerful DALLE-3 and STABLE DIFFUSION models.')}} </h3>
                        <p class="text-sm/loose text-slate-500 dark:text-slate-400">{{__("We've prepared an exciting canvas for you to write your ideas, and AI will create them for you.")}}</p>
                        <div class="mt-5">
                            <a target="_blank" href=""
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">
                                {{__('Create your first image!')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-12">
                <div class="flex flex-wrap items-center justify-center">
                    <div class="lg:w-4/5">
                        <div class="mx-auto text-center sm:w-2/3 md:w-1/2">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                {{__('HosheMan Robots')}} </h3>
                            <p class="text-sm/loose text-slate-500 dark:text-slate-400">{{__("We've prepared a variety of personalized robots with strong prompt writing for you, so you won't have to worry anymore.")}}</p>
                        </div>
                        <div class="-m-3 flex flex-wrap justify-center pt-6">
                            <div class="w-full p-3 sm:w-4/5 md:w-1/2">
                                <a target="_blank" href=""
                                   class="inline-flex">
                                    <img class="rounded-lg border border-slate-200 dark:border-slate-800"
                                         src="{{asset('images/bots-image.webp')}}" width="490" height="292"
                                         loading="lazy" alt="bots image"/>
                                </a>
                            </div>
                            <div class="p-3 max-md:hidden md:w-1/2">
                                <a href=""
                                   class="inline-flex h-full">
                                    <img class="rounded-lg border border-slate-200 dark:border-slate-800"
                                         src="{{asset('images/bots-details.webp')}}" width="490" height="292"
                                         loading="lazy" alt="chat area"/>
                                </a>
                            </div>
                        </div>
                        <div class="mt-8 text-center">
                            <a target="_blank" href=""
                               class="inline-flex rounded-full bg-blue-600 px-5 py-2 text-sm font-medium text-white transition-all hover:bg-blue-800">{{__("Let's go!")}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Tools" class="pt-12">
                <div class="flex flex-wrap items-center justify-center">
                    <div class="w-full lg:w-4/5">
                        <div class="mx-auto w-full pb-8 text-center md:w-3/4 lg:w-1/2">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                {{__('HosheMan has various tools for every profession.')}}</h3>
                            <div class="flex flex-wrap justify-center gap-2">
                                <div
                                    class="mb-2 inline-flex rounded bg-blue-200 px-2 py-0.5 text-[11px] font-medium text-blue-600">
                                    {{__('Programming')}}
                                </div>
                                <div
                                    class="mb-2 inline-flex rounded bg-pink-200 px-2 py-0.5 text-[11px] font-medium text-pink-600">
                                    {{__('Speech to Text')}}
                                </div>
                                <div
                                    class="mb-2 inline-flex rounded bg-green-200 px-2 py-0.5 text-[11px] font-medium text-green-600">
                                    {{__('Article Generation')}}
                                </div>
                            </div>
                        </div>
                        <div class="-m-4 flex flex-wrap justify-center text-center">
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-blue-100 group-hover:dark:shadow-blue-950"
                                        src="{{asset('images/code-gen-blank.webp')}}" loading="lazy" alt="coding page"/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        {{__('AI Programmer')}}</h6>
                                </a>
                            </div>
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                        src="{{asset('images/audio.webp')}}" loading="lazy" alt="audio page"/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        {{__('Speech to Text AI')}} </h6>
                                </a>
                            </div>
                            <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                                <a target="_blank" href=""
                                   class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                    <img
                                        class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-green-100 group-hover:dark:shadow-green-950"
                                        src="{{asset('images/copywrite-gen.webp')}}" loading="lazy" alt="copy write"/>
                                    <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                        {{__('AI Article Writer')}} </h6>
                                </a>
                            </div>
                        </div>

                        <div class="w-full py-10 text-center">
                            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                                {{__('Targeted Professions')}}</h3>

                            <div class="max-lg:grid-cols-2 max-md:grid-cols-1 grid grid-cols-3 gap-4">
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-blue-600 bg-blue-200 rounded-2xl hover:shadow-[#cba15326]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Instagram Administrators')}}</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-pink-600 rounded-2xl bg-pink-200 hover:shadow-[#ab7fe621]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Product Designers')}}</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-green-600 rounded-2xl bg-green-200 hover:shadow-[#57cbc624]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Entrepreneurs')}}</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-orange-500 rounded-2xl bg-orange-200 hover:shadow-[#7f8fe624]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Graphic Designers')}}</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-emerald-600 rounded-2xl bg-emerald-100 hover:shadow-[#6bac6524]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Students and Teachers')}}</h3>
                                </div>
                                <div
                                    class="lqd-color-box flex items-center py-5 gap-4 rounded-[15px] transition-all hover:shadow-lg hover:-translate-y-2 text-red-600 rounded-2xl bg-red-200 hover:shadow-[#ef793a1f]">
                                <span
                                    class="mx-3 w-6 h-6 border border-8 border-white rounded-full shadow-lg"></span>
                                    <h3 class="text-xl text-inherit">{{__('Programmers')}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section id="Features" class="bg-slate-100 py-16 dark:bg-slate-900 lg:py-20">
        <div class="container px-3 text-center">
            <div
                class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                {{__('Features')}}
            </div>
            <div class="flex flex-wrap items-center justify-center">
                <div class="lg:w-4/5">
                    <div class="xxl:w-1/2 mx-auto w-full pb-8 text-center xl:w-2/3">
                        <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">
                            {{__('Features of the HosheMan website')}}
                        </h3>
                    </div>
                    <div class="-m-4 flex flex-wrap justify-center text-center">
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/pro-dashboard.webp')}}" loading="lazy" alt="dashboard"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Professional Dashboard')}} </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/language.webp')}}" loading="lazy" alt="support"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Support for over 50 languages')}} </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/payment.webp')}}" loading="lazy" alt="payment"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Secure payment with Zibal')}} </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/referral.webp')}}" loading="lazy" alt="referral"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Ability to invite friends')}} </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/save-docs.webp')}}" loading="lazy" alt="documents saving"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Download and save generated content')}} </h6>
                            </a>
                        </div>
                        <div class="w-full p-4 sm:w-4/5 md:w-1/2 lg:w-1/3">
                            <a href="#"
                               class="group flex flex-col rounded-xl border border-slate-200 bg-white p-2 text-center dark:border-slate-800 dark:bg-slate-950">
                                <img
                                    class="h-full w-full rounded-xl border border-slate-100 transition-all duration-300 group-hover:-translate-y-3 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950"
                                    src="{{asset('images/support.webp')}}" loading="lazy" alt="support image"/>
                                <h6 class="relative z-10 pb-2 pt-4 text-base font-bold text-slate-600 dark:text-slate-200">
                                    {{__('Fast Support')}} </h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section class="overflow-hidden bg-slate-100 pb-2 pt-16 dark:bg-slate-900 md:pt-20 lg:pt-24 xl:pt-28">
        <div class="xxl:w-1/2 mx-auto w-full pb-8 text-center xl:w-2/3">
            <div class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                {{__('Blog')}}
            </div>
            <h3 class="mb-3 text-[2.5rem] font-bold leading-tight text-slate-800 dark:text-white">{{__('Last Blog Posts')}}</h3>
        </div>
        <div class="container px-3">
            <div class="-m-4 flex flex-wrap items-center justify-center mb-5">
                @foreach($posts as $post)
                    <div class="p-4 md:w-4/5 lg:w-1/3">
                        <div
                            class="rounded-2xl border border-slate-100 bg-white p-7 shadow dark:border-slate-950 dark:bg-slate-950">
                            <div class="pb-4 group">
                                <img class="rounded-lg transition-all duration-300 group-hover:scale-[1.15] group-hover:shadow-xl group-hover:shadow-pink-100 group-hover:dark:shadow-pink-950" src="{{ $post['image_url']}}"
                                     alt="{{ $post['title']['rendered'] }}">
                            </div>
                            <p class="text-base/7 font-light italic text-slate-500 dark:text-slate-300">
                                {!! Str::words(strip_tags($post['excerpt']['rendered']), 20, '...') !!}
                            </p>
                            <div class="flex items-center pt-5">
                                <div class="ms-5">
                                    <a href="{{ $post['link'] }}" target="_blank" class="rounded-md p-2 text-base/7 text-emerald-600 hover:bg-emerald-800 hover:text-white">{{__('Read more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div><!-- container -->
    </section>

    <section id="faq" class="bg-slate-100 overflow-hidden py-16 dark:bg-slate-900 md:pt-20 lg:pt-24 xl:pt-28">
        <div class="container px-3 text-center">
            <div
                class="mb-2 inline-flex rounded-full bg-gradient-to-r from-green-600 to-pink-500 px-2 py-0.5 text-[11px] font-medium text-white">
                {{__('FAQs')}}
            </div>
            <div class="flex flex-wrap items-center justify-center pb-8 lg:pb-10">
                <div class="text- mx-auto text-center sm:w-2/3 md:w-3/5 lg:w-2/5">
                    <h3 class="mb-3 text-3xl font-bold leading-tight text-slate-700 dark:text-white"> {{__('Have any questions?')}} </h3>
                    <p class="text-sm/loose text-slate-500 dark:text-slate-400">{{__("If you can't find the answer to your question here, send a message to support.")}}</p>
                </div>
            </div>
            <div class="-m-3 flex flex-wrap justify-center md:-m-4">
                <div class="w-full p-3 md:p-4 xl:w-3/4">
                    <div class="accordion flex flex-col gap-3">
                        <div
                            class="accordion-item  active  group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                            <button
                                class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                <span
                                    class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{__('How does HosheMan work?')}} </span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                     class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                    <path fill="currentColor"
                                          d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"></path>
                                </svg>
                            </button><!-- accordion-toggle -->
                            <div
                                class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400"
                                style="display: block;">
                                <div class="max-w-3xl">
                                    <p class="text-base/7 text-justify">
                                        {{__("You don't need to purchase monthly plans here! You can top up your wallet as much as you like, and whenever you use any feature, the cost will be deducted from your wallet according to the")}}
                                        <a href=""
                                           class="text-rose-500">{{__('pricing of that specific section')}}</a>{{__("This way, you don't have to worry about monthly fees or wasting your money.")}}
                                    </p>
                                </div>
                            </div><!-- accordion-body -->
                        </div><!-- accordion-item -->
                        <div
                            class="accordion-item  group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                            <button
                                class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                <span
                                    class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{__("Why should I use this site? I’ll just use the original ChatGPT!")}} </span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                     class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                    <path fill="currentColor"
                                          d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"></path>
                                </svg>
                            </button><!-- accordion-toggle -->
                            <div
                                class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400"
                                style="display: block;">
                                <div class="max-w-3xl">
                                    <p class="text-base/7 text-justify"> به چند دلیل :
                                        <br>
                                        {{__("1- The OpenAI site has restricted us, and you can't directly verify your user account.")}}
                                        <br>
                                        {{__("2- The version 4 of this AI is paid, and due to the price of the dollar in our country, it's not affordable for many people.")}}
                                        <br>
                                        {{__("3- With our site, you can access several global AI models.")}}


                                        {{__('Should I say more? :)')}} </p>
                                </div>
                            </div><!-- accordion-body -->
                        </div><!-- accordion-item -->
                        <div
                            class="accordion-item  group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                            <button
                                class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                <span
                                    class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{__("Can I use it for free?")}} </span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                     class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                    <path fill="currentColor"
                                          d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"></path>
                                </svg>
                            </button><!-- accordion-toggle -->
                            <div
                                class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400"
                                style="display: none;">
                                <div class="max-w-3xl">
                                    <p class="text-base/7 text-justify"> {{__("Designing and launching this site has cost our team a lot :) But don't worry, to help you trust it, after signing up, your wallet will be credited with 15,000 Toman so you can test it and be sure of its quality.")}} </p>
                                </div>
                            </div><!-- accordion-body -->
                        </div><!-- accordion-item -->
                        <div
                            class="accordion-item  group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                            <button
                                class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                <span
                                    class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{__('Can it be used without a VPN?')}} </span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                     class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                    <path fill="currentColor"
                                          d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"></path>
                                </svg>
                            </button><!-- accordion-toggle -->
                            <div
                                class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400"
                                style="display: none;">
                                <div class="max-w-3xl">
                                    <p class="text-base/7 text-justify"> {{__("Yes! We created this site so you don't have to worry about sanctions or filtering, and you can use it with peace of mind.")}} </p>
                                </div>
                            </div><!-- accordion-body -->
                        </div><!-- accordion-item -->
                        <div
                            class="accordion-item  group rounded-2xl border border-slate-100 bg-white shadow dark:border-slate-950 dark:bg-slate-950">
                            <button
                                class="accordion-toggle flex w-full items-center justify-between px-6 py-4 text-start font-bold text-slate-600 dark:text-slate-200">
                                <span
                                    class="block text-base font-bold text-slate-600 group-[.active]:text-slate-700 dark:text-slate-300 group-[.active]:dark:text-white"> {{__('Do you have an app?')}} </span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                     class="ms-2 h-2.5 transition-all group-[.active]:rotate-180">
                                    <path fill="currentColor"
                                          d="M233.4 406.6c12.5 12.5 32.8 12.5 45.3 0l192-192c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L256 338.7 86.6 169.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l192 192z"></path>
                                </svg>
                            </button><!-- accordion-toggle -->
                            <div
                                class="accordion-body px-6 pb-5 text-base text-slate-500 group-[.active]:block dark:text-slate-400"
                                style="display: none;">
                                <div class="max-w-3xl">
                                    <p class="text-base/7 text-justify">{{__("Not yet! But you can easily use it on any device, as its user interface is compatible with all devices :)
We will be launching an app soon though...")}} </p>
                                </div>
                            </div><!-- accordion-body -->
                        </div><!-- accordion-item -->
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->

    <section class="overflow-hidden bg-slate-100 dark:bg-slate-900">
        <div class="container px-3">
            <div
                class="relative rounded-t-xl border border-b-0 border-gray-200 bg-gradient-to-b from-slate-100 to-white p-6 dark:border-gray-800 dark:from-slate-900 dark:to-slate-950 sm:p-10">
                <div class="mx-auto max-w-3xl text-center">
                    <h1 class="text-[2rem] font-bold text-slate-800 dark:text-white">
                    <span
                        class="mb-6 block text-2xl !leading-snug text-slate-800 dark:text-white sm:text-3xl md:text-4xl lg:text-[2rem] xl:text-5xl">
                        @if(session()->has('locale') && session()->get('locale') === 'en')
                            Work smart with <span class="text-cyan-500">HosheMan</span>!
                        @else
                            با <span class="text-cyan-500">هوش من</span> <br> هوشمندانه کار کن!</span>
                        @endif
                    </h1>
                </div>
            </div>
        </div><!-- container -->
    </section><!-- section -->
@endsection
