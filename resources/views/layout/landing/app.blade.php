<!DOCTYPE html>
<html lang="en" class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{__('HosheMan, the most specialized Iranian AI platform')}}">
    <title>@yield('title')</title>
    <link rel="icon" type="image/svg+xml" href="{{asset('images/favicon.png')}}">
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/toastr.min.css')}}">
    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef"/>
    <link rel="apple-touch-icon" href="{{ asset('icons/logo-512.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('styles')
    @if(auth()->user())
        <link rel="stylesheet" href="{{asset('assets/css/plugins/datatables.min.css')}}">
    @endif
    <script type="text/javascript" async>
        !function () {
            var i = "JnNmdJ", a = window, d = document;

            function g() {
                var g = d.createElement("script"), s = "https://www.goftino.com/widget/" + i,
                    l = localStorage.getItem("goftino_" + i);
                g.async = !0, g.src = l ? s + "?o=" + l : s;
                d.getElementsByTagName("head")[0].appendChild(g);
            }

            "complete" === d.readyState ? g() : a.attachEvent ? a.attachEvent("onload", g) : a.addEventListener("load", g, !1);
        }();
    </script>

</head>

@if(session()->has('locale') && session()->get('locale') === 'en')
    <body class="min-w-[320px] bg-slate-100 dark:bg-slate-900">
@else
    <body class="min-w-[320px] bg-slate-100 dark:bg-slate-900" dir="rtl">
@endif
        <div id="root">
            <input type="hidden" id="locale" value="{{session()->has('locale')?session()->get('locale'):'fa'}}">
            <div
                    class="flex min-h-screen max-w-full flex-col overflow-x-hidden pt-[calc(theme(space.16)+theme(space.1))]"
                    id="#pagecontent">
                @if(auth()->user())
                    @include('layout.user.header')
                @else
                    @include('layout.landing.header')
                @endif
                @php
                    $discount=\App\Models\DiscountCode::query()->where('is_public',true)->where('expires_at','>',now())->first();
                    if ($discount)
                        $userUsed=\App\Models\DiscountRedemption::query()->where('user_id',auth()->id())->where('discount_code_id',$discount->id)->first();
                    else
                        $userUsed=false;
                @endphp
                @if($discount && !$userUsed)
                    <div class="bg-rose-100 rounded-md text-center p-2 px-3 mt-8 text-sm sm:w-1/2 w-full mx-auto">
                        <p class="text-rose-600 mb-2">برای گرفتن {{$discount->percentage}}% شارژ بیشتر همین الان کیف
                            پولت رو شارژ کن</p>
                        <a href="{{route('wallet.charge')}}" class="bg-rose-500 text-white px-2 mt-2 rounded-md">شارژ
                            کیف پول</a>
                    </div>
                @elseif(auth()->user())
                    @if(auth()->user()->firstDiscount())
                        <div class="bg-rose-100 rounded-md text-center p-2 px-3 mt-8 text-sm sm:w-1/2 w-full mx-auto">
                            <p class="text-rose-600 mb-2">برای گرفتن 20% شارژ بیشتر همین الان کیف پولت رو شارژ کن</p>
                            <a href="{{route('wallet.charge')}}" class="bg-rose-500 text-white px-2 mt-2 rounded-md">شارژ
                                کیف پول</a>
                        </div>
                    @endif
                @endif
                @yield('content')
            </div>
        </div>

        <!-- footer -->
        @if(Request::is('/') || Request::is('about-us') || Request::is('term-of-use') || Request::is('pricing'))
            @include('layout.landing.footer')
        @endif
        <!-- end footer -->

        <script src="{{asset('assets/js/jquery.js')}}"></script>
        <script src="{{asset('assets/js/functions/forms.js')}}"></script>
        <script src="{{asset('assets/js/translations.js')}}"></script>
        <script src="{{asset('assets/js/bundle.js?v1.0.0')}}"></script>
        <script src="{{asset('assets/js/scripts.js?v1.0.0')}}"></script>
        <script src="{{asset('assets/js/plugins/sweetalert2.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/toastr.min.js') }}"></script>
        @if(auth()->user())
            <script src="{{asset('assets/js/functions/myFunctions.js')}}"></script>
            <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
        @endif
        @yield('scripts')

        <script src="{{ asset('/sw.js') }}"></script>
        <script>
            $(document).ready(function () {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-bottom-center",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 500,
                    "timeOut": 2000,
                    "extendedTimeOut": 1000
                }

                @if(session('success'))
                    toastr.success("{{ __(session("success")) }}")
                @elseif(session('message') || session('error'))
                    toastr.error("{{ __(session("error")) }}")
                @endif

                if ("serviceWorker" in navigator) {
                    navigator.serviceWorker.register("/sw.js?v=2").then(
                        (registration) => {
                            console.log("Service worker registration succeeded:", registration);
                        },
                        (error) => {
                            console.error(`Service worker registration failed: ${error}`);
                        },
                    );
                } else {
                    console.error("Service workers are not supported.");
                }

                let deferredPrompt;
                window.addEventListener('beforeinstallprompt', (e) => {
                    deferredPrompt = e;
                });

                const installContainer = document.getElementById('install-container');
                // بررسی وضعیت نصب
                if (!localStorage.getItem('appInstalled')) {
                    installContainer.classList.remove('hidden'); // مخفی کردن دکمه اگر قبلاً نصب شده باشد
                }

                const installApp = document.getElementById('install-button');
                installApp.addEventListener('click', async () => {
                    if (deferredPrompt !== null) {
                        deferredPrompt.prompt();
                        const {outcome} = await deferredPrompt.userChoice;
                        if (outcome === 'accepted') {
                            deferredPrompt = null;
                        }
                    }
                });

                // رویداد appinstalled
                window.addEventListener('appinstalled', () => {
                    localStorage.setItem('appInstalled', 'true'); // ذخیره وضعیت نصب
                    installContainer.classList.add('hidden'); // مخفی کردن دکمه پس از نصب
                });
            })
        </script>
</body>
    </body>
</html>
