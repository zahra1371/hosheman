<section
    class="overflow-hidden border-t border-gray-200 bg-slate-50 py-16 dark:border-gray-800 dark:bg-slate-950 md:py-20">
    <div class="container px-3">
        <div class="-m-3 flex flex-wrap md:-m-4">
            <div class="w-full p-3 md:p-4 lg:w-4/12 xl:w-3/12">
                <div class="pb-3">
                    <a href="{{route('home')}}">
                        <div class="flex items-center">
                            <img class="h-16 dark:hidden" src="{{asset('images/logo.webp')}}" alt="logo"/>
                        </div>
                    </a>
                    <div class="">
                        <p class="text-base/7 text-slate-500 dark:text-slate-300">{{__('The most specialized Iranian AI platform')}}</p>
                    </div>
                </div>
            </div>
            <div class="w-6/12 p-3 sm:w-3/12 md:p-4 lg:ms-auto lg:w-2/12">
                <h6 class="mb-3 text-base font-bold text-slate-700 dark:text-white"> {{__('Tools')}} </h6>
                <ul>
                    <li>
                        <a href="{{route('aitools','graphics')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('AI Image Generator')}}</a>
                    </li>
                    <li>
                        <a href="{{route('chatbot','hosheman')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('AI Chat Bot')}}</a>
                    </li>
                    <li>
                        <a href="{{route('aitools','voiceover')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('AI Speech to Text')}}</a>
                    </li>
                    <li>
                        <a href="{{route('aitools.list')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('AI Tools')}}</a>
                    </li>
                    <li>
                    </li>
                </ul>
            </div>
            <div class="w-6/12 p-3 sm:w-3/12 md:p-4 lg:w-2/12">
                <h6 class="mb-3 text-base font-bold text-slate-700 dark:text-white"> {{__('Pages')}} </h6>
                <ul>
                    <li>
                        <a href="{{route('home')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('Home')}}</a>
                    </li>
                    <li>
                        <a href="{{route('about')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('About Us')}}</a>
                    </li>
                    <li>
                        <a href="{{route('terms')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('Terms Of Use')}}</a>
                    </li>
                    <li>
                        <a href="{{route('pricing')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600">{{__('Pricing')}}</a>
                    </li>
                    <li>
                        <a href="{{route('blog')}}"
                           class="inline-flex text-sm/7 text-slate-500 transition-all hover:text-blue-600 dark:text-slate-400 hover:dark:text-blue-600" target="_blank">{{__('Blog')}}</a>
                    </li>
                </ul>
            </div>
            <div class="w-full p-3 sm:w-6/12 md:p-4 lg:w-3/12">
                <div class="flex items-center gap-2">
                    <a referrerpolicy='origin' target='_blank'
                       href='https://trustseal.enamad.ir/?id=510090&Code=pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G'><img
                            referrerpolicy='origin'
                            src='https://trustseal.enamad.ir/logo.aspx?id=510090&Code=pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G'
                            alt='' style='cursor:pointer' code='pNPFhPk8cp6gbxgOHPK0GceELPY0Jd6G'></a>

                </div>
            </div>
        </div>
        <div class="flex items-center justify-center mt-8">
            <p class="rounded-full bg-blue-200 w-full md:w-1/3 p-5 text-center">
                {{__('All rights to this platform belong to the HosheMan')}}
            </p>
        </div>
    </div><!-- container -->
</section><!-- section -->
