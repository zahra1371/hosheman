<?php

namespace App\Helpers\Classes;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StabilityAIHelper
{
    protected $apiKey;
    protected $baseUrl;

    public function __construct()
    {
        $this->apiKey = env('STABILITY_TOKEN', 'your-default-api-key');
        $this->baseUrl = 'https://api.stability.ai/v2beta';

    }

    public function generateRealImage($prompt, $aspectRatio = '1:1', $outputFormat = 'png')
    {
        try {
            $response = Http::withHeaders([
                'Authorization' => "Bearer {$this->apiKey}",
                'Accept' => 'image/*',
            ])->asMultipart()->post("$this->baseUrl/stable-image/generate/ultra", [
                [
                    'name' => 'none',
                    'contents' => ''
                ],
                [
                    'name' => 'prompt',
                    'contents' => $prompt
                ],
                [
                    'name' => 'aspect_ratio',
                    'contents' => $aspectRatio
                ],
                [
                    'name' => 'output_format',
                    'contents' => $outputFormat
                ],
            ]);

            if ($response->successful()) {
                $nameprompt = mb_substr($prompt, 0, 15);
                $nameprompt = explode(' ', $nameprompt)[0];

                $nameOfImage = Str::random(12) . '-STD-' . Str::slug($nameprompt) . '.png';

                //save file on local storage or aws s3
                Storage::disk('liara')->put($nameOfImage, $response->body());
                return ['status'=>200,'nameOfImage'=>$nameOfImage];
            } else {
                dd($response, $response->body());
            }
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    public function upscale($imagePath, $outputFormat = 'png')
    {
        try {
            $url = "$this->baseUrl/stable-image/upscale/fast";

            $response = Http::withHeaders([
                'Authorization' => "Bearer {$this->apiKey}",
                'Accept' => 'image/*',
            ])->attach('image', file_get_contents($imagePath), basename($imagePath))
                ->asMultipart()->post($url, [
                    'output_format' => $outputFormat,
                ]);

            return $this->handleResponse($response);
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    public function removeBackground($img, $outputFormat = 'png')
    {
        try {
            $url = "$this->baseUrl/stable-image/edit/remove-background";

            $client = new Client([
                'base_uri' => $url,
                'headers' => [
                    'content-type' => 'multipart/form-data',
                    'Authorization' => 'Bearer ' . $this->apiKey,
                    'accept' => 'application/json'
                ],
            ]);
            $payload = [
                'image' => $img,
            ];

            $multipart = [];
            foreach ($payload as $key => $value) {
                if ($key == 'image') {
                    $multipart[] = ['name' => $key, 'contents' => $value, 'filename' => 'image.png'];
                } else {
                    $multipart[] = ['name' => $key, 'contents' => $value];
                }
            }
            $payload = $multipart;

            $response = $client->post('remove-background', [
                "headers" => [
                    "accept" => "application/json",
                ],
                'multipart' => $payload,
            ]);


            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                $body = $response->getBody();
                return base64_decode(json_decode($body)->image);
            } elseif ($statusCode === 413) {
                error_log("AIController::deleteBackground()\n" . $response->getBody());
                return ['status' => '413', 'message' => 'حجم فایل باید کمتر از 10 مگابایت باشد.'];
            } else {
                error_log("AIController::deleteBackground()\n" . $response->getBody());
                return ['status' => '500', 'message' => 'خطایی بوجود اومده لطفا به پشتیبانی اطلاع بده.'];
            }

        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    public function generate3D($imagePath)
    {
        try {
            $url = "$this->baseUrl/3d/stable-fast-3d";

            $response = Http::withHeaders([
                'Authorization' => "Bearer {$this->apiKey}",
            ])->attach('image', file_get_contents($imagePath), basename($imagePath))
                ->asMultipart()->post($url);

            return $this->handleResponse($response);
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    public function sketch($imagePath, $prompt, $controlStrength = 0.7, $outputFormat = 'png')
    {
        try {
            $url = "$this->baseUrl/stable-image/control/sketch";

            $response = Http::withHeaders([
                'Authorization' => "Bearer {$this->apiKey}",
                'Accept' => 'image/*',
            ])->attach('image', file_get_contents($imagePath), basename($imagePath))
                ->asMultipart()->post($url, [
                    'prompt' => $prompt,
                    'control_strength' => $controlStrength,
                    'output_format' => $outputFormat,
                ]);

            return $this->handleResponse($response);
        } catch (\Exception $e) {
            return $this->handleException($e);
        }
    }

    private function handleResponse($response)
    {
        if ($response->successful()) {
            return [
                'status' => 'success',
                'image' => $response->body(),
            ];
        }

        return [
            'status' => 'error',
            'image' => null,
            'message' => $response->body(),
        ];
    }

    private function handleException($e)
    {
        return [
            'status' => 'error',
            'image' => null,
            'message' => $e->getMessage(),
        ];
    }
}
