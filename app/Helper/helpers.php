<?php

use App\Models\Activity;
use App\Models\Usage;
use Illuminate\Support\Facades\Log;

if (!function_exists('convertPersianNumbers')) {
    function convertPersianNumbers($string)
    {
        // Persian digits array
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        // English digits array
        $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        // Check if the string contains any Persian digits
        if (preg_match('/[۰-۹]/', $string)) {
            // Convert Persian numbers to English
            return str_replace($persian, $english, $string);
        }

        // If the string contains only English digits, return it as is
        return $string;
    }
}

if (!function_exists('errorLog')) {
    function errorLog($exception, $path = null): void
    {
        if (is_string($exception))
            Log::error($exception);
        else
            Log::error("An error occurred at $path", [
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString(),
            ]);
    }
}

if (!function_exists('createActivity')) {
    function createActivity($user_id, $activity_type, $activity_title, $url)
    {
        $activityEntry = new Activity();
        $activityEntry->user_id = $user_id;
        $activityEntry->type = $activity_type;
        $activityEntry->title = $activity_title;
        $activityEntry->url = $url;
        $activityEntry->save();
    }
}

if (!function_exists('jdate_from_gregorian')) {
    function jdate_from_gregorian($input, $format = '%d, %B %Y')
    {
        return \Morilog\Jalali\Jalalian::fromDateTime($input)->format($format);
    }
}
function countWords($text): int
{
    $encoding = mb_detect_encoding($text);
    if ($encoding === 'UTF-8') {
        // Count Chinese words by splitting the string into individual characters
        $words = preg_match_all('/\p{Han}|\p{L}+|\p{N}+/u', $text);
    } else {
        // For other languages, use str_word_count()
        $words = str_word_count($text, 0, $encoding);
    }
    return (int)$words;
}

if (!function_exists('totalUsage')) {
    function totalUsage($credit, $type = 'total_words'): void
    {
        $usage = Usage::query()->first();
        $usage[$type] += $credit;
        $usage->save();
    }
}
