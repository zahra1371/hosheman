<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromptLibrary extends Model
{
    protected $table="prompt_libraries";
}
