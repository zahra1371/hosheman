<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kavenegar;

class Token extends Model
{
    use HasFactory;

    protected $table = 'tokens';
    protected $fillable=['user_id','code','is_valid',];
    const EXPIRATION_TIME = 2; // minutes

    public function __construct(array $attributes = [])
    {
        // Ensure a code is always generated
        $attributes['code'] = $attributes['code'] ?? $this->generateCode();
        parent::__construct($attributes);
    }

    /**
     * Generate a random numeric code with a default length of 6.
     *
     * @param int $codeLength
     * @return string
     */
    public function generateCode(int $codeLength = 6): string
    {
        $min = pow(10, $codeLength - 1);
        $max = pow(10, $codeLength) - 1;
        return (string) mt_rand($min, $max);
    }

    /**
     * Define the relationship with the User model.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if the token is valid (not used and not expired).
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return !$this->isUsed() && !$this->isExpired();
    }

    /**
     * Check if the token has been used.
     *
     * @return bool
     */
    public function isUsed(): bool
    {
        return (bool) $this->used;
    }

    /**
     * Check if the token has expired.
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->created_at->diffInMinutes(now()) > static::EXPIRATION_TIME;
    }

    /**
     * Send the token code to the associated user's mobile number.
     *
     * @return bool
     * @throws \Exception If no user is attached to the token.
     */
    public function sendCode(): bool
    {
//        return true;
        if (!$this->user) {
            throw new \Exception("No user attached to this token.");
        }

        $this->code = $this->code ?? $this->generateCode();

        try {
            $receptor = $this->user->mobile; // User's mobile number
            Kavenegar::VerifyLookup($receptor, $this->code, '', '', 'verify');
        } catch (\Kavenegar\Exceptions\ApiException $e) {
           errorLog($e);
        } catch (\Kavenegar\Exceptions\HttpException $e) {
            // Log the error or handle it appropriately
            errorLog($e);
        }

        return true;
    }
}

