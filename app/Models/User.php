<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'name',
        'surname',
        'mobile',
        'is_admin',
        'has_seen_popup',
        'free_words',
        'free_images',
        'wallet_balance',
        'referral_code',
        'referred_by',
    ];

    public function referrer()
    {
        return $this->belongsTo(User::class, 'referred_by');
    }

    public function referredUsers()
    {
        return $this->hasMany(User::class, 'referred_by');
    }

    public function walletTransactions()
    {
        return $this->hasMany(WalletTransaction::class);
    }

    public function fullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function discount()
    {
        return $this->hasOne(DiscountCode::class);
    }

    public function firstDiscount(): bool
    {
        if ($this->discount()->first())
            return Carbon::now()->lessThan($this->discount()->first()->expires_at) && !$this->discount()->first()->is_used;
        else
            return false;
    }
}
