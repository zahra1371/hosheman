<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserChatBot extends Model
{
    protected $table = 'user_chat_bots';
    protected $fillable = ['user_id', 'bot_id', 'title', 'total_words'];


    public function messages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ChatMessage::class, 'user_chat_id');
    }

    public function chatBot()
    {
        return $this->belongsTo(AiModel::class, 'bot_id');
    }
}
