<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceRate extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'service_name',
        'rate_per_unit',
    ];
}
