<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DiscountRedemption extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'discount_code_id',
    ];

}
