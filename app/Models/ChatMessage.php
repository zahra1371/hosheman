<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;

    protected $fillable = ['user_chat_id', 'input','main_input','image_path', 'response', 'words', 'credits'];

    public function userChatBot(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(UserChatBot::class, 'user_chat_id', 'id');
    }
}
