<?php

namespace App\Console\Commands;

use App\Models\UserTool;
use Carbon\Carbon;
use Illuminate\Console\Command;

class deleteOldImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:delete-old';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete images older than 2 weeks';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userImages=UserTool::query()->where('type', 'image')
            ->where('path','!=','deleted')
            ->where('created_at', '<=', Carbon::now()->subDays(14)->toDateTimeString())->get();
        foreach ($userImages as $userImage){
            $userImage->path='deleted';
            $userImage->save();
        }

        $this->info('images delete executed successfully!');;
    }
}
