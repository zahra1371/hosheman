<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\AiModel;
use App\Models\ChatMessage;
use App\Models\ServiceRate;
use App\Models\UserChatBot;
use App\Models\UserTool;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use PhpOffice\PhpWord\IOFactory as WordIOFactory;
use PhpOffice\PhpSpreadsheet\IOFactory as ExcelIOFactory;

class ChatBotController extends Controller
{
    public function chatBots(): View
    {
        $chatBots = AiModel::query()->where('is_active', 1)->get();
        return view('user.chatbot.list', compact('chatBots'));
    }

    public function chatBot($slug): View
    {
        try {
            $chatBot = AiModel::query()->where('slug', $slug)->first();
            $previous_chats = UserChatBot::query()->where('user_id', auth()->id())->where('bot_id', $chatBot->id)->orderBy('created_at', 'desc')->get();
            $last_chat = UserChatBot::query()->where('user_id', auth()->id())->where('bot_id', $chatBot->id)->orderBy('created_at', 'desc')->first();

            if ($last_chat == null)
                $last_chat = $this->startNewChat($chatBot->id);

            return view('user.chatbot.chat', compact('chatBot', 'previous_chats', 'last_chat'));
        } catch (\Exception $exception) {
            errorLog($exception, 'ChatBotController , chatBot function : ');
            return redirect()->route('user.chatbot.list')->with('error', __("An issue has occurred. Please try again later."));
        }
    }

    public function startNewChat($id)
    {
        try {
            $chatBot = AiModel::query()->find($id);
            $user_chat_bot = UserChatBot::query()->create([
                'user_id' => auth()->id(),
                'bot_id' => $chatBot->id,
                'title' => $chatBot->description,
                'total_words' => 0
            ]);

            ChatMessage::query()->create([
                'user_chat_id' => $user_chat_bot->id,
                'input' => null,
                'response' => $chatBot->first_message,
                'is_user' => false,
                'words' => 0,
                'credits' => 0
            ]);

            return $user_chat_bot->load('messages', 'chatBot');
        } catch (\Exception $exception) {
            error_log($exception, 'ChatBotController , startNewChat function : ');
            return false;
        }
    }

    public function sendChat(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                // Ensure user has sufficient wallet balance
                if (\auth()->user()->wallet_balance < 100) {
                    return response()->json(['error' => __("Your balance is insufficient for chatting. Please top up your wallet.")], 422);
                }

                // Validate prompt or file presence
                if (empty($request->prompt) && !$request->hasFile('file')) {
                    return response()->json(['error' => __("Please write a question or upload a file.")], 422);
                }

                // Validate file if present
                if ($request->hasFile('file')) {
                    $validation = Validator::make($request->all(), [
                        'file' => 'required|file|max:10240|mimes:png,jpg,jpeg,txt,doc,docx,xls,xlsx',
                    ], [
                        'file.max' => __("The file size must be less than 10 MB."),
                        'file.mimes' => __("The file format is not supported."),
                    ]);

                    if ($validation->fails()) {
                        return response()->json(['status' => 422, 'errors' => $validation->errors()->first()]);
                    }
                }

                // Handle file upload and content extraction
                $fileContent = null;
                if ($request->hasFile('file')) {
                    $uploadedFile = $request->file('file');
                    $extension = $uploadedFile->getClientOriginalExtension();

                    // Save the file to Liara storage
                    $filePath = 'chats/' . \auth()->user()->id . '-' . now()->timestamp . '-' . $uploadedFile->getClientOriginalName();
                    Storage::disk('liara')->put($filePath, file_get_contents($uploadedFile->getRealPath()));

                    // Extract content based on file type
                    $fileContent = match ($extension) {
                        'txt' => file_get_contents($uploadedFile->getRealPath()),
                        'pdf' => $this->extractTextFromPDF($filePath),
                        'doc', 'docx' => $this->extractTextFromWord($uploadedFile),
                        'xls', 'xlsx' => $this->extractTextFromExcel($uploadedFile),
                        default => null,
                    };

                    if (in_array($extension, ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx']) && !$fileContent) {
                        return response()->json(['error' => __("The file content is unreadable. Please upload a valid file.")], 422);
                    }
                }

                // Combine prompt and file content
                $combinedInput = $request->prompt;
                if ($fileContent) {
                    $combinedInput .= "\n\n--- محتوای فایل ---\n" . $fileContent;
                }


                // Token limit management
                $maxTokens = 8000; // Adjust based on the OpenAI model
                $tokenCount = $this->countTokens($combinedInput);

                if ($tokenCount > $maxTokens) {
                    $combinedInput = $this->truncateContent($combinedInput, $maxTokens);
                }

                // Save chat message
                $chat_id = $request->get('chat_id');
                $message = ChatMessage::create([
                    'user_chat_id' => $chat_id,
                    'input' => $combinedInput,
                    'main_input' => $request->prompt,
                    'image_path' => $filePath ?? null,
                    'response' => null,
                    'is_user' => true,
                    'words' => str_word_count($combinedInput),
                    'credits' => 0,
                ]);

                return $message->id;
            } catch (\Exception $exception) {
                errorLog($exception, 'ChatBotController , sendChat function : ');
                return response($exception->getMessage(), 500);
            }
        } elseif ($request->isMethod('get')) {
            $inputCount = 0;
            $openaiApiKey = env('OPENAI_API_KEY');
            $chat_id = $request->get('chat_id');
            $message_id = $request->get('message_id');
            $message = ChatMessage::query()->find($message_id);
            $prompt = $message->input;
            $inputCount += countWords($message->input);

            // Retrieve chat history
            $history = [];
            $chat = UserChatBot::query()->whereId($chat_id)->first();

            $history[] = ['role' => 'system', 'content' => 'You are a helpful assistant. Always respond concisely. Your answer must be at most 500 tokens.'];

            // Add previous messages to history
            $lastMessages = $chat->messages()
                ->whereNotNull('input')
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get()
                ->reverse();

            foreach ($lastMessages as $msg) {
                $history[] = ['role' => 'user', 'content' => $msg->input ?? ''];
                $inputCount += $msg->input ? countWords($msg->input) : 0;
                if ($msg->response) {
                    $history[] = ['role' => 'assistant', 'content' => $msg->response];
                    $inputCount += countWords($msg->response);
                } else {
                    $history[] = ['role' => 'assistant', 'content' => ''];
                }
            }

            // Use the Vision model if an image is present
            $chat_bot = $message->image_path ? 'gpt-4o-mini' : 'chatgpt-4o-latest';

            // Prepare the image for OpenAI (if an image exists)
            $extension = pathinfo(basename($message->image_path), PATHINFO_EXTENSION);
            $imagePath = null;
            if ($message->image_path && in_array($extension, ['jpg', 'png', 'jpeg'])) {
                $imagePath = Storage::disk('liara')->url($message->image_path); // Read image from liara disk
            }

            // Send request to OpenAI
            return response()->stream(function () use ($request, $openaiApiKey, $chat_bot, $history, $message_id, $prompt, $imagePath) {
                $client = new Client();
                $url = 'https://api.openai.com/v1/chat/completions';
                $headers = [
                    'Authorization' => 'Bearer ' . $openaiApiKey,
                ];

                // Prepare the payload for OpenAI
                $messages = $history;
                $messages[] = ['role' => 'user', 'content' => []];

                // Add the user's prompt (if any)
                if ($prompt) {
                    $messages[count($messages) - 1]['content'][] = ['type' => 'text', 'text' => $prompt];
                }

                // Add the image (if any)
                if ($imagePath) {
                    $messages[count($messages) - 1]['content'][] = [
                        'type' => 'image_url',
                        'image_url' => [
                            'url' => $imagePath, // Include base64-encoded image
                        ],
                    ];
                }

                $postData = [
                    'headers' => $headers,
                    'json' => [
                        'model' => $chat_bot,
                        'messages' => $messages,
                        'stream' => true,
                        'max_tokens' => 900,
                        'stream_options' => ['include_usage' => true]
                    ],
                ];

                $response = $client->post($url, $postData);
                $responsedText = '';

                $outputTokens = 0;
                $inputTokens = 0;
                foreach (explode("\n", $response->getBody()->getContents()) as $chunk) {
                    $chunk = trim($chunk);

                    // اگر این پیام نشان‌دهنده پایان داده‌ها نباشد
                    if (!empty($chunk) && $chunk !== 'data: [DONE]') {
                        $jsonData = json_decode(substr($chunk, 6), true);

                        // بررسی وجود محتوای پاسخ
                        if (isset($jsonData['choices'][0]['delta']['content'])) {
                            $message = $jsonData['choices'][0]['delta']['content'];

                            $messageFix = str_replace(["\r\n", "\r", "\n"], '<br/>', $message);
                            $responsedText .= $message;

                            // تولید متن تصادفی برای جایگزینی
                            $string_length = Str::length($messageFix);
                            $needChars = 6000 - $string_length;
                            $random_text = Str::random($needChars);

                            echo PHP_EOL;
                            echo 'data: ' . $messageFix . '/**' . $random_text . "\n\n";
                            flush();
                            usleep(1000);
                        }

                        // دریافت اطلاعات توکن‌ها در آخرین بخش
                        if (isset($jsonData['usage'])) {
                            $inputTokens = $jsonData['usage']['prompt_tokens'] ?? 0;
                            $outputTokens = $jsonData['usage']['completion_tokens'] ?? 0;
                            $totalTokens = $jsonData['usage']['total_tokens'] ?? 0;
                            errorLog("total: $totalTokens - input: $inputTokens - output: $outputTokens");
                            flush();
                            usleep(1000);
                        }
                    }
                }

                // Save the response and update user credits
                $message = ChatMessage::query()->whereId($message_id)->first();
                $message->response = $responsedText;
                $message->credits = $outputTokens;
                $message->words = $inputTokens;
                $message->save();

                // Deduct user wallet balance
                $word_rate = ServiceRate::query()->where('service_name', 'gpt-word')->first();
                $input_word_rate = ServiceRate::query()->where('service_name', 'gpt-word-input')->first();
                $user = Auth::user();
                if (($word_rate->rate_per_unit * ($outputTokens + $inputTokens)) >= $user->wallet_balance)
                    $user->wallet_balance = 0;
                else
                    $user->wallet_balance = $user->wallet_balance - ($word_rate->rate_per_unit * $outputTokens) - ($inputTokens * $input_word_rate->rate_per_unit);
                $user->save();

                totalUsage($outputTokens);

                // Update chat word count
                $chat_id = $message->user_chat_id;
                $chat = UserChatBot::query()->whereId($chat_id)->first();
                $chat->total_words += $outputTokens;
                $chat->save();

                echo 'data: [DONE]';
                echo "\n\n";
                flush();
                usleep(1000);
            }, 200, [
                'Cache-Control' => 'no-cache',
                'X-Accel-Buffering' => 'no',
                'Content-Type' => 'text/event-stream',
            ]);
        }
    }

    /**
     * Count tokens in the content
     */
    private function countTokens($content): int
    {
        return str_word_count($content); // Replace with a tokenizer if required
    }

    /**
     * Truncate content to fit within token limit
     */
    private function truncateContent($content, $maxTokens): string
    {
        $tokens = explode(' ', $content);
        return implode(' ', array_slice($tokens, 0, $maxTokens));
    }

    /**
     * Extract text from PDF file
     */
    private function extractTextFromPDF($file): ?string
    {
        try {
// Get the file contents from Liara Storage
            $fileContents = Storage::disk('liara')->get($file);

// Define a local temporary file path
            $localTempFilePath = sys_get_temp_dir() . '/' . uniqid() . '.pdf';

// Save the file contents to the local temporary file
            file_put_contents($localTempFilePath, $fileContents);

            $binaryPath = 'C:\\Program Files\\xpdf-tools\\bin64\\pdftotext.exe'; // Windows
            // $binaryPath = '/usr/bin/pdftotext'; // Linux/macOS

            // Extract text from the PDF
            $text = (new Pdf($binaryPath))
                ->setPdf($localTempFilePath) // Path to the PDF file
                ->setOptions([
                    'enc' => 'UTF-8', // Encoding
                ])
                ->text();

            dd($text); // Display the extracted text
        } catch (\Exception $exception) {
            dd($exception);
        }

    }


    /**
     * Extract text from Word file
     */
    private function extractTextFromWord($file): ?string
    {
        try {
            $content = '';
            $phpWord = WordIOFactory::load($file->getRealPath());
            foreach ($phpWord->getSections() as $section) {
                foreach ($section->getElements() as $element) {
                    if (method_exists($element, 'getText')) {
                        $content .= $element->getText() . "\n";
                    }
                }
            }
            return $content;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Extract text from Excel file
     */
    private function extractTextFromExcel($file): ?string
    {
        try {
            $spreadsheet = ExcelIOFactory::load($file->getRealPath());
            $sheetData = $spreadsheet->getActiveSheet()->toArray();

            $content = '';
            foreach ($sheetData as $row) {
                $content .= implode("\t", $row) . "\n"; // Join cells with tabs
            }
            return $content;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getChat(Request $request)
    {
        try {
            $chat = UserChatBot::query()->find($request->get('chat_id'));
            return $chat->load('messages');
        } catch (\Exception $exception) {
            errorLog($exception, 'ChatBotController , getChat function : ');
        }
    }

    public function contentDelete($id): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            $chat = UserChatBot::query()->where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
            $chat->messages()->delete();
            $chat->delete();
            return back()->with(['message' => __('Deleted successfully'), 'type' => 'success']);
        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'errors' => __('There is an error in sending the SMS. Please try again later.')]);
        }
    }
}
