<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Tool;
use App\Models\User;
use App\Models\UserTool;
use App\Models\WalletTransaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function dashboard(): View
    {
        $image_tool = Tool::query()->where('type', 'image')->pluck('id')->toArray();
        $documents = UserTool::query()->where('user_id', auth()->id())->whereNotIn('tool_id', $image_tool)->orderBy('created_at', 'desc')->take(5)->with('tool.category')->get();
        return view('user.dashboard', compact('documents'));
    }

    public function walletChargeForm(): View
    {
        return view('user.wallet-charge-form');
    }

    public function documents(): \Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse
    {
        try {
            $image_tool = Tool::query()->where('type', 'image')->pluck('id')->toArray();
            $documents = UserTool::query()->where('user_id', auth()->id())->whereNotIn('tool_id', $image_tool)->orderBy('created_at', 'desc')->with('tool.category')->paginate(10);
            return view('user.documents', compact('documents'));
        } catch (\Exception $exception) {
            error_log($exception);
            return redirect()->route('user.dashboard')->with('error', __("An issue has occurred. Please try again later."));
        }
    }

    public function viewDocument($id)
    {
        try {
            $document = UserTool::query()->where('user_id', auth()->id())->findOrFail($id);
            return view('user.view-document', compact('document'));
        } catch (\Exception $exception) {
            errorLog($exception, 'UserController , viewDocument function : ');
        }
    }

    public function updateDocument(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            UserTool::query()->where('user_id', auth()->id())->findOrFail($request->id)->update([
                'title' => $request->title,
                'output' => $request->output
            ]);
            return response()->json(['status' => 200, 'message' => __("Successfully updated."), 'reload' => true]);
        } catch (\Exception $exception) {
            errorLog($exception, 'UserController , updateDocument function : ');
            return response()->json(['status' => 500, 'errors' => 'error']);
        }
    }


    public function profile(): \Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse
    {
        try {
            $orders = WalletTransaction::query()->where('user_id', auth()->id())->where('status','Success')->orderBy('created_at', 'desc')->paginate(5);
            return view('user.profile', compact('orders'));
        } catch (\Exception $exception) {
            errorLog($exception, 'UserController , profile function : ');
            return redirect()->back()->with('error', __("An issue has occurred. Please try again later."));
        }
    }

    public function updateProfile(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'surname' => 'required',
            ]);

            if ($validation->fails())
                return response()->json(['status' => 422, 'errors' => $validation->errors()]);

            User::query()->where('id', auth()->id())->update([
                'name' => request('name'),
                'surname' => request('surname'),
            ]);

            return response()->json(['status' => 200, 'message' => __("Your information has been successfully updated."), 'reload' => true]);
        } catch (\Exception $exception) {
            errorLog($exception, 'UserController , updateProfile function : ');
            return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
        }
    }

    public function changeFirstLogin(): \Illuminate\Http\JsonResponse
    {
        try {
            \auth()->user()->has_seen_popup = true;
            \auth()->user()->save();
            return response()->json(['status' => 200,'url'=>route('chatbot','hosheman')]);
        } catch (\Exception $exception) {
            errorLog($exception,'UserController , changeFirstLogin function : ');
            return response()->json(['status' => 500, 'errors' => $exception]);
        }
    }
}
