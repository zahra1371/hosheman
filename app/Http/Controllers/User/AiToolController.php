<?php

namespace App\Http\Controllers\User;

use App\Helpers\Classes\StabilityAIHelper;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\ServiceRate;
use App\Models\Tool;
use App\Models\ToolCategory;
use App\Models\UserTool;
use getID3;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Mockery\Exception;
use OpenAI;
use OpenAI\Laravel\Facades\OpenAI as FacadesOpenAI;
use PhpOffice\PhpWord\IOFactory as WordIOFactory;


class AiToolController extends Controller
{
    public function aiTools(): View
    {
        $tool_categories = ToolCategory::query()->orderBy('created_at', 'desc')->get();
        return view('user.aitool.list', compact('tool_categories'));
    }

    public function toolList($slug): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        try {
            $category = ToolCategory::query()->where('name', $slug)->first();
            $tools = Tool::query()->where('category_id', $category->id)->where('is_active', true)->orderBy('id')->get();
            $first_toos = Tool::query()->where('category_id', $category->id)->firstOrFail();
            $languages = Language::all();
            if ($slug === 'graphics') {
                $graphics = UserTool::query()->where('user_id', \auth()->id())->whereIn('type', ['image', 'video'])->where('path', 'liara')->orderBy('created_at', 'desc')->take(10)->get();
                return view('user.aitool.main', compact('tools', 'first_toos', 'languages', 'graphics'));
            } else
                return view('user.aitool.main', compact('tools', 'first_toos', 'languages'));
        } catch (Exception $exception) {
            errorLog($exception);
            return redirect()->back();
        }
    }

    public function generate(Request $request)
    {
        try {
            if ($request->isMethod('post')) {
                $tool = Tool::query()->where('slug', $request->post_type)->first();
                if ($tool->type === 'text' || $tool->type === 'code') {
                    $rate = ServiceRate::query()->where('service_name', 'gpt-word')->first();
                    $final_rate = $rate->rate_per_unit * 100;
                } elseif ($tool->type === 'image') {
                    if ($request->post_type === 'ai_image_generator') {
                        if ($request->model === 'std')
                            $rate = ServiceRate::query()->where('service_name', 'real_image')->first();
                        else
                            $rate = ServiceRate::query()->where('service_name', 'dall-e-3')->first();
                        $final_rate = $rate->rate_per_unit * 1;
                    } else {
                        $rate = ServiceRate::query()->where('service_name', $request->post_type)->first();
                        $final_rate = $rate->rate_per_unit * 1;
                    }
                } elseif ($tool->type === 'audio') {
                    $rate = ServiceRate::query()->where('service_name', $request->post_type)->first();
                    $final_rate = $rate->rate_per_unit * 500;
                } elseif ($tool->type === 'video') {
                    $rate = ServiceRate::query()->where('service_name', "$request->duration-seconds-video")->first();
                    $final_rate = $rate->rate_per_unit * 1;
                }

                if (\auth()->user()->wallet_balance <= $final_rate)
                    return response()->json(['status' => 500, 'errors' => __('Your balance is insufficient for this section. Please top up your wallet.')]);

                $prompt = '';
                $language = $request->language;
                switch ($request->post_type) {
                    case 'instagram_captions':
                        $title = $request->title;
                        $prompt = "Write instagram post caption about $title. caption must be $request->number_of_words words. Creativity is 0.99 between 0 and 1. Language is $language. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'instagram_hashtags':
                        $keywords = $request->keywords;
                        $prompt = "Write instagram hashtags for $keywords. Language is $language. Generate $request->number_of_results different instagram hashtags";
                        break;
                    case 'instagram_stories':
                        $prompt = "رشته استوری جذاب و خلاقانه برای $request->description  بنویس، $request->number_of_results تا استوری باید بشه و کاربر ترغیب بشه تا آخر استوری ها رو ببینه و تعامل داشته باشه و  منجر به $request->cta بشه.استوری ها باید به زبان $language باشند";
                        break;
                    case 'instagram_reels':
                        switch ($request->main_goal) {
                            case 'follow':
                                $goal = "فالوو کردن";
                                break;
                            case 'save':
                                $goal = "ذخیره ریلز";
                                break;
                            case "comment":
                                $goal = "گذاشتن کامنت";
                                break;
                            case "share":
                                $goal = "ارسال برای دیگران";
                                break;
                            case "like":
                                $goal = "لایک کردن";
                                break;

                        }
                        if ($request->details !== '' && $request->details !== null)
                            $prompt = "یک سناریو ریلز خلاقانه و جذاب درمورد $request->description بنویس. سناریو باید قلاب ویدئویی قوی و جذاب و قلاب متنی جذب کننده داشته باشد تا کاربر جذب شده و تا آخر ویدئو را ببیند. ویدئو $request->player بازیگر داره و مدت زمان ریلز باید $request->duration  ثانیه باشه. هدف ریلز $goal.  (منظور از قلاب ویدئویی صحنه یا حرکت غیرمنتظره ای هست که اول ویدئو اتفاق میفته و باعث کنجکاوی کاربر بشه و تا آخر ویدئو رو ببینه.)  هست. برای نوشتن سناریو این توضیحات هم در نظر بگیر :  $request->details.";
                        else
                            $prompt = "یک سناریو ریلز خلاقانه و جذاب درمورد $request->description بنویس. سناریو باید قلاب ویدئویی قوی و جذاب و قلاب متنی جذب کننده داشته باشد تا کاربر جذب شده و تا آخر ویدئو را ببیند. ویدئو $request->player بازیگر داره و مدت زمان ریلز باید $request->duration ثانیه باشه. هدف ریلز $goal  هست. (منظور از قلاب ویدئویی صحنه یا حرکت غیرمنتظره ای هست که اول ویدئو اتفاق میفته و باعث کنجکاوی کاربر بشه و تا آخر ویدئو رو ببینه.)";
                        break;
                    case 'instagram_calendar':
                        $description = $request->description;
                        $follower_target = $request->follower_target;
                        $prompt = "لطفا یک تقویم محتوایی ۳۰ روزه برای پیج اینستاگرام من در زمینه $description بنویس. هدف من این است که در پایان این ۳۰ روز به $follower_target فالوور برسم. شامل انواع مختلف پست‌ها مثل عکس، ویدیو و ریلز باشه و پیشنهادات مرتبط با هشتگ‌ها و زمان مناسب برای پست کردن هم ارائه بده.همچنین از ایده‌های ترند اینستاگرام هم استفاده کن.. توجه داشته باش که زبان محتوا باید $language باشد.";
                        break;
                    case 'instagram_campaign':
                        $description = $request->description;
                        switch ($request->gender_target) {
                            case "men":
                                $gender = "آقایان";
                                break;
                            case "women":
                                $gender = "خانم ها";
                                break;
                            case "both":
                                $gender = "خانم ها و آقایان";
                        }
                        $prompt = "شما یک استراتژیست بازاریابی حرفه‌ای در زمینه رسانه‌های اجتماعی هستید که بیش از ۱۵ سال تجربه در ایجاد کمپین‌های فروش جذاب و مؤثر، به‌ویژه برای اینستاگرام دارید. تخصص شما در درک رفتار مخاطبان، ساخت روایت‌های جذاب و استفاده از عناصر بصری برای افزایش تبدیل‌ها است.

وظیفه شما این است که یک کمپین فروش برای صفحه اینستاگرام من طراحی کنید که به طور مؤثر محصول یا خدمات من را تبلیغ کند. موارد زیر را در نظر بگیرید:

نام کسب‌وکار: $request->brand_name
توضیح محصول/خدمات: $description
مخاطب هدف: $gender
اهداف کمپین (مانند افزایش آگاهی از برند، جذب سرنخ، فروش): فروش
مدت زمان کمپین: $request->campagin_duration روز
پیام‌ها یا تم‌های کلیدی که باید برجسته شوند: $request->details
لطفاً مطمئن شوید که کمپین شامل عناصر بصری و ترکیبی از انواع پست‌ها، مانند استوری، ریلز و پست‌های ثابت باشد. همچنین، هشتگ‌های مرتبط و فراخوان به اقدام (CTA) را که برای مخاطب هدف شما جذاب باشد، در نظر بگیرید.";

                        break;
                    case 'blog_section':
                        $description = $request->description;
                        $prompt = " Write me blog section about $description. Maximum $request->maximum_length words. Creativity is 0.99 between 0 and 1. Language is $language. Generate $request->number_of_results different blog sections. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'blog_post_ideas':
                        $description = $request->description;
                        $prompt = "Write blog post article ideas about $description. Creativity is 0.99 between 0 and 1. Language is $language. Generate $request->number_of_results different blog post ideas. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'blog_intros':
                        $title = $request->title;
                        $description = $request->description;
                        $prompt = "Write blog post intro about title: $title. And the description is $description. Maximum $request->maximum_length words. Creativity is 0.99 between 0 and 1. Language is $language. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'blog_conclusion':
                        $title = $request->title;
                        $description = $request->description;
                        $prompt = "Write blog post conclusion about title: $title. And the description is $description.Maximum $request->maximum_length words. Creativity is 0.99 between 0 and 1. Language is $language. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'youtube_video_description':
                        $title = $request->title;
                        $prompt = "write youtube video description about $title. Maximum $request->maximum_length words. Creativity is 0.99 between 0 and 1. Language is $language. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'youtube_video_title':
                        $description = $request->description;
                        $prompt = "Craft captivating, attention-grabbing video titles about $description for YouTube video. Creativity is 0.99 between 0 and 1. Language is $language. Generate $request->number_of_results different youtube video titles. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'youtube_video_tag':
                        $title = $request->title;
                        $prompt = "Generate tags and keywords about $title for youtube video. Creativity is 0.99 between 0 and 1. Language is $language. Generate $request->number_of_results different youtube tags. Tone of voice must be $request->tone_of_voice";
                        break;
                    case 'summarize_text':
                        if ($request->has('file')) {
                            $uploadedFile = $request->file('file');
                            $extension = $uploadedFile->getClientOriginalExtension();

                            if (!in_array($extension, ['docx', 'doc'])) {
                                return response()->json(['error' => __("The file format is not supported.")], 422);
                            }
                            $text = $this->extractTextFromWord($uploadedFile);

                        } else
                            $text = $request->text;
                        $prompt = "Summarize the following text with key details: $text. The summary should be no longer than $request->maximum_length words and set the creativity to 0.99 in terms of creativity.";
                        break;
                    case "article_generator":
                        $prompt = "title: $request->title , words: $request->total_words , degree: $request->degree , degree: $request->degree , language: $request->language";
                        break;
                }

                if ($tool->type == 'text' || $tool->type == 'youtube') {
                    return $this->generateText($request, $prompt, $tool);
                } elseif ($tool->type == 'audio') {
                    $validation = Validator::make($request->all(), [
                        'file' => 'required|file|mimes:mp3,m4a|max:20480', // Max size in KB (20MB = 20480KB)
                    ], [
                        'file.required' => __('You need to upload a file.'),
                        'file.mimes' => __('The file format must be mp3, m4a, or mp4.'),
                        'file.max' => __('The file size must be less than 20 MB.'),
                    ]);

                    if ($validation->fails())
                        return response()->json(['status' => 422, 'errors' => $validation->errors()->first()]);

                    $file = $request->file('file');
                    return $this->audioToText($file, $tool);
                } elseif ($tool->type === 'image') {
                    return $this->generateImage($request, $tool);
                } elseif ($tool->type == 'code') {
                    return $this->generateCode($request, $tool);
                } elseif ($tool->type == 'video') {
                    $validation = Validator::make($request->all(), [
                        'file' => 'required|file|mimes:png,jpg,jpeg', // Max size in KB (20MB = 20480KB)
                        'description' => 'required', // Max size in KB (20MB = 20480KB)
                    ], [
                        'file.required' => __('You need to upload an image.'),
                        'file.mimes' => __('The file format must be png, jpeg, or jpg.'),
                    ]);

                    if ($validation->fails())
                        return response()->json(['status' => 422, 'errors' => $validation->errors()->first()]);
                    return $this->videoGenerator($request, $tool);

                }
            } else {
                return $this->generateText($request);
            }
        } catch (Exception $exception) {
            errorLog($exception, "AiToolController , generate function : ");
            return response()->json(['error' => __('An issue has occurred. Please try again later.')], 500);
        }
    }

    public function contentQuality(Request $request)
    {
        try {
            $response_quality = false;
            if ($request->quality == 'is-ok')
                $response_quality = true;
            UserTool::query()->where('id', $request->id)->update(['response_quality' => $response_quality]);
        } catch (Exception $exception) {
            errorLog($exception);
            return response()->json(['error' => __('An issue has occurred. Please try again later.')], 500);
        }
    }

    public function generateImage(Request $request, $tool)
    {
        try {
            if ($tool->slug !== 'ai_image_generator') {
                $validation = Validator::make($request->all(), [
                    'file' => 'required|file|mimes:png,jpg,jpeg|max:10240', // Max size in KB (10MB = 10240KB)
                ], [
                    'file.required' => __('You need to upload an image.'),
                    'file.mimes' => __('The image format must be jpg, png, or jpeg.'),
                    'file.max' => __('The image size must be less than 10 MB.'),
                ]);

                if ($validation->fails())
                    return response()->json(['status' => 422, 'errors' => $validation->errors()->first()]);
            }
            if ($tool->slug === 'ai_image_generator' && $request->model === 'dalle-3') {
                return $this->dalleGenerator($request, $tool);
            } elseif ($tool->slug === 'ai_image_generator' && $request->model === 'std') {
                return $this->stdGenerator($request, $tool);
            } elseif ($tool->slug === 'ai_generate_background') {
                return $this->generateBackground($request, $tool);
            } elseif ($tool->slug === 'ai_delete_background') {
                return $this->deleteBackground($request, $tool);
            } elseif ($tool->slug === 'ai_sketch') {
                return $this->AISketch($request, $tool);
            } elseif ($tool->slug === 'ai_erase_objects') {
                return $this->eraseObjects($request, $tool);
            } elseif ($tool->slug === 'ai_outpaint') {
                return $this->outPaint($request, $tool);
            } elseif ($tool->slug === 'ai_recolor') {
                return $this->recolor($request, $tool);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController , generateImage function : ');
            return response()->json(['status' => 'error', 'message' => __('There is an error in sending the SMS. Please try again later.')]);
        }
    }

    public function contentDownload($id): \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\StreamedResponse
    {
        try {
            $file = UserTool::query()->where('id', $id)->where('user_id', \auth()->user()->id)->first();
            if ($file->type === 'video')
                return Storage::disk('liara')->download("/videos/$file->output");

            return Storage::disk('liara')->download(str_replace('/hosheman/', '', $file->output));
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController , contentDownload function : ');
            return response()->json(['status' => 500, 'errors' => __('There is an error in sending the SMS. Please try again later.')]);
        }
    }

    public function contentDelete($id): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            $post = UserTool::query()->where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
            if ($post->type === 'image' || $post->type === 'video') {
                $post->path = 'deleted';
                $post->save();
            } else
                $post->delete();
            return back()->with(['message' => __('Deleted successfully'), 'type' => 'success']);
        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'errors' => __('There is an error in sending the SMS. Please try again later.')]);
        }
    }

    private function dalleGenerator($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';
            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }
            set_time_limit(120);

            $size = $request['size'];
            $prompt = $request['description'];
            $style = $request['image_style'];
            $lighting = $request['image_lighting'];
            $mood = $request['image_mode'];

            if ($style != null) {
                $prompt .= ' ' . $style . ' style.';
            }
            if ($lighting != null) {
                $prompt .= ' ' . $lighting . ' lighting.';
            }
            if ($mood != null) {
                $prompt .= ' ' . $mood . ' mood.';
            }
            $model = 'dall-e-3';
            $response = FacadesOpenAI::images()->create([
                'model' => $model,
                'prompt' => $prompt,
                'size' => $size,
                'response_format' => 'b64_json',
                'quality' => 'standard',
                'n' => 1,
            ]);
            $image_url = $response['data'][0]['b64_json'];
            $contents = base64_decode($image_url);
            $nameprompt = mb_substr($prompt, 0, 15);
            $nameprompt = explode(' ', $nameprompt)[0];

            $nameOfImage = Str::random(12) . '-DALL-E-' . Str::slug($nameprompt) . '.png';

            //save file on local storage or aws s3
            Storage::disk('liara')->put($nameOfImage, $contents);

            $post = UserTool::query()->create([
                'title' => __($tool->name),
                'tool_id' => $tool->id,
                'user_id' => Auth::id(),
                'input' => $prompt,
                'input_translate' => $prompt,
                'output' => $nameOfImage,
                'type' => 'image',
                'credits' => 1,
                'path' => 'liara',
            ]);

            // Release the lock
            Cache::lock($lockKey)->release();
            $image_rate = ServiceRate::query()->where('service_name', 'dall-e-3')->first();
            if ($image_rate->rate_per_unit >= \auth()->user()->wallet_balance)
                \auth()->user()->wallet_balance = 0;
            else
                \auth()->user()->wallet_balance -= $image_rate->rate_per_unit;
            \auth()->user()->save();

            totalUsage(1, 'total_images');
            return response()->json(['type' => 'image', 'post' => $post]);

        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController , dalleGenerator function : ');
            if (Str::contains($exception->getMessage(), 'This request has been blocked by our content filters') || Str::contains($exception->getMessage(), 'Your request was rejected as a result of our safety system')) {
                return response()->json(['status' => 500, 'errors' => __('This request has been blocked by our content filters')]);
            } else
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function stdGenerator($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';
            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }
            set_time_limit(120);

            $helper = new StabilityAIHelper();
            $prompt = $this->translateImagePrompt($request->description);
            if ($request->size == '1024x1024')
                $aspectRatio = '1:1';
            elseif ($request->size == '1792x1024')
                $aspectRatio = '16:9';
            else
                $aspectRatio = '9:16';

            $response = $helper->generateRealImage($prompt, $aspectRatio);

            if ($response['status'] == 200) {
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => $request->description,
                    'input_translate' => $prompt,
                    'output' => $response['nameOfImage'],
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();
                $image_rate = ServiceRate::query()->where('service_name', 'real_image')->first();
                if ($image_rate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $image_rate->rate_per_unit;
                \auth()->user()->save();

                totalUsage(1, 'total_images');
                return response()->json(['type' => 'image', 'post' => $post]);
            } else {
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
            }
        } catch (Exception $exception) {
            errorLog($exception, 'AiToolController , dalleGenerator function : ');
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function generateText($request, $prompt = null, $tool = null)
    {
        try {
            if ($request->isMethod('post')) {
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => $prompt,
                    'output' => null,
                    'type' => 'text',
                    'credits' => 0,
                ]);
                if ($post->tool_id === 2) {
                    $post->path = countWords($post->input);
                    $post->save();
                }

                return response()->json(['status' => 200, 'message' => 'done', 'data' => ['post' => $post]]);

            } elseif ($request->isMethod('get')) {
                $openai_api_key = env('OPENAI_API_KEY');
                $post = UserTool::query()->where('id', $request->get('post_id'))->first();
                try {
                    $client = OpenAI::factory()
                        ->withApiKey($openai_api_key)
                        ->make();
                    if ($post->tool_id === 4) {
                        $sections = json_decode($post->input, true);
                        return $this->articleGenerator($sections, $post, $client);
                    } else
                        $result = $client->chat()->createStreamed([
                            'model' => 'chatgpt-4o-latest',
                            'messages' => [
                                [
                                    'role' => 'user',
                                    'content' => $post->input,
                                ]
                            ],
                            'stream' => true,
                        ]);

                } catch (\Exception $exception) {
                    errorLog($exception);
                    return [json_encode(['message' => __('An issue has occurred. Please try again later.')])];
                }

                return response()->stream(function () use ($result, $post) {
                    $output = '';
                    foreach ($result as $response) {
                        if (isset($response->choices[0]->delta->content)) {
                            $message = str_replace("\n", ' <br> ', $response->choices[0]->delta->content);
                            $output = $output . $message;
                            echo "event: data\n";
                            echo 'data: ' . json_encode(['message' => $message]) . "\n\n";
                            ob_flush();
                            flush();
                        }
                    }
                    $wordCount = countWords($output);
                    $word_rate = ServiceRate::query()->where('service_name', 'gpt-word')->first();
                    $total_price = 0;
                    if ($post->tool_id === 2) {
                        $total_price = ($wordCount * $word_rate->rate_per_unit) + $post->path * 0.5;
                    } else {
                        $total_price = ($wordCount * $word_rate->rate_per_unit);
                    }
                    if ($total_price >= \auth()->user()->wallet_balance)
                        \auth()->user()->wallet_balance = 0;
                    else
                        \auth()->user()->wallet_balance -= $total_price;
                    \auth()->user()->save();

                    $post->output = $output;
                    $post->credits = $wordCount;
                    $post->save();

                    totalUsage($wordCount);
                    // Send stop event with metadata
                    echo "event: stop\n";
                    echo 'data: ' . json_encode([
                            'post' => $post,
                        ]) . "\n\n";
                    ob_flush();
                    flush();
                }, 200, [
                    'Content-Type' => 'text/event-stream',
                    'Cache-Control' => 'no-cache',
                    'Connection' => 'keep-alive',
                ]);
            }
        } catch (Exception $exception) {
            errorLog($exception, 'AiToolController , generateText function : ');
            return response()->json(['status' => 500, 'message' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function audioToText($file, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $path = 'upload/audio/';
            $file_name = Str::random(4) . '-' . Str::slug(\auth()->user()->fullName()) . '-audio.' . $file->getClientOriginalExtension();

            //Audio Extension Control
            $file_types = ['mp3', 'mp4', 'mpeg', 'mpga', 'm4a', 'wav', 'webm'];
            if (!in_array(Str::lower($file->getClientOriginalExtension()), $file_types)) {
                $data = [
                    'errors' => [__('The audio file format is invalid.')],
                ];
                return response()->json($data, 419);
            }

            $file->move($path, $file_name);
            $absoluteFilePath = public_path($path . $file_name);

            // Get file duration in seconds
            $duration = $this->getFileDuration($absoluteFilePath);
            $response = FacadesOpenAI::audio()->transcribe([
                'file' => fopen($path . $file_name, 'r'),
                'model' => 'whisper-1',
                'response_format' => 'text',
            ]);
            $text = $response->text;

            $post = UserTool::query()->create([
                'title' => __($tool->name),
                'tool_id' => $tool->id,
                'user_id' => Auth::id(),
                'input' => $path . $file_name,
                'output' => $text,
                'type' => 'text',
                'credits' => countWords($text),
            ]);

            $voice_rate = ServiceRate::query()->where('service_name', 'ai_speech_to_text')->first();
            if (($voice_rate->rate_per_unit * $duration) >= \auth()->user()->wallet_balance)
                \auth()->user()->wallet_balance = 0;
            else
                \auth()->user()->wallet_balance -= ($voice_rate->rate_per_unit * $duration);
            \auth()->user()->save();

            totalUsage($post->credits);

            return response()->json(['type' => 'audio', 'post' => $post]);
        } catch (Exception $exception) {
            errorLog($exception);
            return response()->json(['error' => __('An issue has occurred. Please try again later.')], 500);
        }
    }

    private function getFileDuration($filePath): ?int
    {
        try {
            $getID3 = new getID3();
            $fileInfo = $getID3->analyze($filePath);
            if (isset($fileInfo['playtime_seconds'])) {
                return (int)round($fileInfo['playtime_seconds']); // Duration in seconds
            }
            return null; // Duration not found
        } catch (\Exception $e) {
            errorLog($e);
            return null;
        }
    }

    private function translateImagePrompt($prompt)
    {
        try {
            $openai_api_key = env('OPENAI_API_KEY');
            $client = OpenAI::factory()
                ->withApiKey($openai_api_key)
                ->make();
            $response = $client->chat()->create([
                'model' => 'chatgpt-4o-latest', // Use the appropriate model (e.g., gpt-4 or gpt-3.5-turbo)
                'messages' => [
                    [
                        'role' => 'user',
                        'content' => "if the following text is not english, translate it to English and if it is english write it again for me :\n\n{$prompt}"
                    ]
                ],
            ]);

            $translatedText = $response->choices[0]->message->content;
            return $translatedText;
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    private function generateBackground($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $imageName = $uploadedFile->getClientOriginalName();
            $filePath = $uploadedFile->getRealPath();
            $fileContents = file_get_contents($filePath);
            $base64Image = base64_encode($fileContents);

            $size = explode('x', $request['size']);
            $description = $request['description'] ? $this->translateImagePrompt($request['description']) : null;
            $params = [
                'images' => [$base64Image],
                'description' => $description,
                'theme' => $request['imageStyle'] ?? 'Surprise me',
                'transforms' => [],
                'autoresize' => true,
                'height' => (int)$size[1],
                'width' => (int)$size[0]
            ];

            $apiToken = env('PEBBLELY_TOKEN');
            $endpointUrl = "https://api.pebblely.com/create-background/v2/";

            // Make API request
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'X-Pebblely-Access-Token' => $apiToken,
            ])->post($endpointUrl, $params);

            // Handle response
            if ($response->status() === 200) {
                $image_b64 = $response->json()['data'];
                $image_data = base64_decode($image_b64);
                $nameprompt = mb_substr($imageName, 0, 5);
                $nameprompt = explode(' ', $nameprompt)[0];

                $nameOfImage = Str::random(12) . '-PEBBLELY-' . Str::slug($nameprompt) . '.png';

                //save file on local storage or aws s3
                Storage::disk('liara')->put($nameOfImage, $image_data);
            } else {
                errorLog($response->body(), 'AiController , generateBackground function : ');
                return response()->json(['status' => 500, 'errors' => __('There is an error in sending the SMS. Please try again later.')]);
            }

            $post = UserTool::query()->create([
                'title' => __($tool->name),
                'tool_id' => $tool->id,
                'user_id' => Auth::id(),
                'input' => $request['description'] ?? $request['imageStyle'],
                'input_translate' => $description,
                'output' => $nameOfImage,
                'type' => 'image',
                'credits' => 1,
                'path' => 'liara',
            ]);

            // Release the lock
            Cache::lock($lockKey)->release();
            $image_rate = ServiceRate::query()->where('service_name', 'ai_generate_background')->first();
            if ($image_rate->rate_per_unit >= \auth()->user()->wallet_balance)
                \auth()->user()->wallet_balance = 0;
            else
                \auth()->user()->wallet_balance -= $image_rate->rate_per_unit;
            \auth()->user()->save();

            totalUsage(1, 'total_change_back');

            return response()->json(['type' => 'image', 'post' => $post]);
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController, createBackground function');
            return response()->json(['status' => 500, 'errors' => __('There is an error in sending the SMS. Please try again later.')]);
        }
    }

    private function deleteBackground($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $filePath = $uploadedFile->getRealPath();
            $fileName = $uploadedFile->getClientOriginalName();

            // Prepare API request
            $baseUri = "https://api.stability.ai/v2beta/stable-image/edit/remove-background";
            $apiToken = env('STABILITY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept' => 'image/*',
            ])->attach(
                'image', file_get_contents($filePath), $fileName
            )->post($baseUri, [
                'output_format' => 'png',
            ]);

            // Handle API response
            if ($response->status() === 200) {
                // Save the processed image
                $imageContents = $response->body();
                $namePrompt = mb_substr($fileName, 0, 5);
                $namePrompt = explode(' ', $namePrompt)[0];
                $nameOfImage = Str::random(12) . '-STD-' . Str::slug($namePrompt) . '.png';

                // Save file to storage (local or liara)
                Storage::disk('liara')->put($nameOfImage, $imageContents);

                // Create a record in the UserTool table
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => 'remove background',
                    'output' => $nameOfImage,
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                // Deduct credits from user's wallet
                $imageRate = ServiceRate::query()->where('service_name', 'ai_delete_background')->first();
                if ($imageRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $imageRate->rate_per_unit;
                \auth()->user()->save();

                totalUsage(1, 'total_edit_images');
                return response()->json(['type' => 'image', 'post' => $post]);
            } elseif ($response->status() === 413) {
                return response()->json(['status' => 500, 'errors' => __('The file size must be less than 10 MB.')]);
            } else {
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
            }
        } catch (\Exception $exception) {
            // Log the exception and return a generic error response
            errorLog($exception, 'AiToolController, deleteBackground function');
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function AISketch($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $filePath = $uploadedFile->getRealPath();
            $fileName = $uploadedFile->getClientOriginalName();

            $prompt = $this->translateImagePrompt($request->description);
            $params = [
                'prompt' => $prompt,
                'control_strength' => 0.7,
                'output_format' => 'png',
            ];
            // Prepare API request
            $baseUri = "https://api.stability.ai/v2beta/stable-image/control/sketch";
            $apiToken = env('STABILITY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept' => 'image/*',
            ])->attach(
                'image', file_get_contents($filePath), $fileName
            )->post($baseUri, $params);

            // Handle API response
            if ($response->status() === 200) {
                // Save the processed image
                $imageContents = $response->body();
                $namePrompt = mb_substr($fileName, 0, 5);
                $namePrompt = explode(' ', $namePrompt)[0];
                $nameOfImage = Str::random(12) . '-sketch-' . Str::slug($namePrompt) . '.png';

                // Save file to storage (local or liara)
                Storage::disk('liara')->put($nameOfImage, $imageContents);

                // Create a record in the UserTool table
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => 'sketch : ' . $request->description,
                    'output' => $nameOfImage,
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                // Deduct credits from user's wallet
                $imageRate = ServiceRate::query()->where('service_name', 'ai_sketch')->first();
                if ($imageRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $imageRate->rate_per_unit;
                \auth()->user()->save();
                totalUsage(1, 'total_edit_images');

                return response()->json(['type' => 'image', 'post' => $post]);
            } else {
                errorLog($response->body(), 'AiToolController, AISketch function');
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController, AISketch function');
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function eraseObjects($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $maskFile = $request->file('mask');
            $filePath = $uploadedFile->getRealPath();
            $fileName = $uploadedFile->getClientOriginalName();

            // Prepare API request
            $baseUri = "https://api.stability.ai/v2beta/stable-image/edit/erase";
            $apiToken = env('STABILITY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept' => 'image/*',
            ])->attach(
                'image', file_get_contents($filePath), $fileName
            )->attach(
                'mask', file_get_contents($maskFile->getRealPath()),
                $maskFile->getClientOriginalName()
            )->post($baseUri);

            // Handle API response
            if ($response->status() === 200) {
                // Save the processed image
                $imageContents = $response->body();
                $namePrompt = mb_substr($fileName, 0, 5);
                $namePrompt = explode(' ', $namePrompt)[0];
                $nameOfImage = Str::random(12) . '-erase-' . Str::slug($namePrompt) . '.png';

                // Save file to storage (local or liara)
                Storage::disk('liara')->put($nameOfImage, $imageContents);

                // Create a record in the UserTool table
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => 'erase objects',
                    'output' => $nameOfImage,
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                // Deduct credits from user's wallet
                $imageRate = ServiceRate::query()->where('service_name', 'erase_objects')->first();
                if ($imageRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $imageRate->rate_per_unit;

                \auth()->user()->save();

                totalUsage(1, 'total_edit_images');

                return response()->json(['type' => 'image', 'post' => $post]);
            } else {
                errorLog($response->body(), 'AiToolController, AISketch function');
                return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController, AISketch function');
            return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
        }
    }

    private function outPaint($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $maskFile = $request->file('mask');
            $filePath = $uploadedFile->getRealPath();
            $fileName = $uploadedFile->getClientOriginalName();
            // Get the required 'left' and 'down' parameters from the request, if available
            $params = [
                'left' => (int)$request->input('left', 200),
                'right' => (int)$request->input('right', 200),
                'up' => (int)$request->input('up', 200),
                'down' => (int)$request->input('down', 200),
                'creativity' => 1
            ];

            // Prepare API request
            $baseUri = "https://api.stability.ai/v2beta/stable-image/edit/outpaint";
            $apiToken = env('STABILITY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept' => 'image/*',
            ])->attach(
                'image', file_get_contents($filePath), $fileName
            )->post($baseUri, $params);

            // Handle API response
            if ($response->status() === 200) {
                // Save the processed image
                $imageContents = $response->body();
                $namePrompt = mb_substr($fileName, 0, 5);
                $namePrompt = explode(' ', $namePrompt)[0];
                $nameOfImage = Str::random(12) . '-erase-' . Str::slug($namePrompt) . '.png';

                // Save file to storage (local or liara)
                Storage::disk('liara')->put($nameOfImage, $imageContents);

                // Create a record in the UserTool table
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => 'erase objects',
                    'output' => $nameOfImage,
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                // Deduct credits from user's wallet
                $imageRate = ServiceRate::query()->where('service_name', 'ai_outpaint')->first();
                if ($imageRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $imageRate->rate_per_unit;

                \auth()->user()->save();

                totalUsage(1, 'total_edit_images');

                return response()->json(['type' => 'image', 'post' => $post]);
            } else {
                errorLog($response->body(), 'AiToolController, AISketch function');
                if (Str::contains($response->body(), 'unsupported aspect ratio')) {
                    return response()->json(['status' => 500, 'errors' => __("The image dimensions are not within the specified range.")]);
                } else
                    return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController, AISketch function');
            return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
        }
    }

    private function recolor($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_image_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the image, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $filePath = $uploadedFile->getRealPath();
            $fileName = $uploadedFile->getClientOriginalName();

            $prompt = $this->translateImagePrompt($request->input('prompt'));
            $select_prompt = $this->translateImagePrompt($request->input('select_prompt'));

            $params = [
                'prompt' => $prompt,
                'select_prompt' => $select_prompt,
            ];

            // Prepare API request
            $baseUri = "https://api.stability.ai/v2beta/stable-image/edit/search-and-recolor";
            $apiToken = env('STABILITY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Accept' => 'image/*',
            ])->attach(
                'image', file_get_contents($filePath), $fileName
            )->post($baseUri, $params);

            // Handle API response
            if ($response->status() === 200) {
                // Save the processed image
                $imageContents = $response->body();
                $namePrompt = mb_substr($fileName, 0, 5);
                $namePrompt = explode(' ', $namePrompt)[0];
                $nameOfImage = Str::random(12) . '-erase-' . Str::slug($namePrompt) . '.png';

                // Save file to storage (local or liara)
                Storage::disk('liara')->put($nameOfImage, $imageContents);

                // Create a record in the UserTool table
                $post = UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => 'recolor',
                    'output' => $nameOfImage,
                    'type' => 'image',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                // Deduct credits from user's wallet
                $imageRate = ServiceRate::query()->where('service_name', 'ai_recolor')->first();
                if ($imageRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $imageRate->rate_per_unit;

                \auth()->user()->save();

                totalUsage(1, 'total_edit_images');

                return response()->json(['type' => 'image', 'post' => $post]);
            } else {
                errorLog($response->body(), 'AiToolController, recolor function');
                return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'AiToolController, recolor function');
            return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
        }
    }

    private function generateCode($request, $tool): \Illuminate\Http\JsonResponse
    {
        try {
            $description = $request->prompt;
            $prompt = "Write a code about $description";
            $response = FacadesOpenAI::chat()->create([
                'model' => 'chatgpt-4o-latest',
                'messages' => [
                    ['role' => 'user', 'content' => $prompt],
                ],
            ]);

            $total_used_tokens = $response->usage->totalTokens;

            $post = UserTool::query()->create([
                'title' => __($tool->name),
                'tool_id' => $tool->id,
                'user_id' => Auth::id(),
                'input' => $prompt,
                'output' => $response->choices[0]->message->content,
                'type' => 'code',
                'credits' => $total_used_tokens,
            ]);

            $chunks = $this->splitCodeAndDescription($post->output);

            $codeRate = ServiceRate::query()->where('service_name', 'gpt-word')->first();
            if (($codeRate->rate_per_unit * $total_used_tokens) >= \auth()->user()->wallet_balance)
                \auth()->user()->wallet_balance = 0;
            else
                \auth()->user()->wallet_balance -= ($codeRate->rate_per_unit * $total_used_tokens);

            \auth()->user()->save();

            totalUsage($codeRate->rate_per_unit * $total_used_tokens);

            return response()->json(['type' => 'code', 'chunks' => $chunks, 'post' => $post]);
        } catch (Exception $exception) {
            errorLog($exception, 'AiToolController , generateCode function :');
            return response()->json(['status' => 500, 'errors' => __("An issue has occurred. Please try again later.")]);
        }
    }

    private function extractTextFromWord($file): ?string
    {
        try {
            $content = '';
            $phpWord = WordIOFactory::load($file->getRealPath());
            foreach ($phpWord->getSections() as $section) {
                foreach ($section->getElements() as $element) {
                    if (method_exists($element, 'getText')) {
                        $content .= $element->getText() . "\n";
                    }
                }
            }
            return $content;
        } catch (\Exception $e) {
            errorLog($e, 'AiToolController , extractTextFromWord function :');
            return null;
        }
    }

    // 📌 دریافت تمام بخش‌های مقاله بدون حلقه بی‌نهایت
    private function articleGenerator($post, $client): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $parts = explode(',', $post->input);
        $data = [];
        foreach ($parts as $part) {
            [$key, $value] = explode(':', $part, 2);
            $data[trim($key)] = trim($value);
        }

        $title = $data['title'] ?? null;
        $words = $data['words'] ?? null;
        $degree = $data['degree'] ?? null;
        $language = $data['language'] ?? null;

        $abstract = $words * 5 / 100;
        $introduction = $words * 15 / 100;
        $main = $words * 20 / 100;

        return response()->stream(function () use ($post, $client, $title, $introduction, $abstract, $main, $degree, $words) {
            $prompts = [
                "یک مقاله آکادمیک معتبر با منابع علمی معتبر و ارجاعات دقیق در سبک استناد [APA/MLA/Chicago] درباره «$title » بنویس. این مقاله باید متناسب با سطح [$degree] باشد و [$words ] کلمه داشته باشد. ساختار مقاله شامل چکیده، مقدمه، بدنه (با بخش‌های منطقی و تحلیل علمی)، نتیجه‌گیری و لیست منابع باشد. از مقالات علمی، کتب دانشگاهی و منابع معتبر برای استناد استفاده کن. مقاله باید دقیق، مستند و به‌روز باشد. بخش های مختلف را به تفکیک برات میفرستم تا بنویسی.یک چکیده $abstract کلمه ای درباره موضوع داده شده بنویس. ",
                " خب با توجه به چکیده ای که نوشتی : [history] حالا یک مقدمه $introduction کلمه ای درباره همان موضوع بنویس. ",
                " چکیده و مقدمه ای که نوشتی این هست :[history] . حالا بخش اول مقاله را در $main کلمه بنویس.",
                " چکیده و مقدمه و بدنه ی اول که نوشتی این هست : [history]. حالا بخش دوم مقاله را در $main کلمه بنویس.",
                " چکیده و مقدمه و بدنه اول و دوم که نوشتی این هست :[history] . حالا بخش سوم مقاله را در $main کلمه بنویس.",
                " چکیده و مقدمه و بدنه ها این هاست: [history] , حالا نتیجه گیری رو بنویس.",
                "برای مقاله ای که نوشتی یک لیست از منابع علمی معتبر (۳ تا ۵ سال اخیر) که در این مقاله استفاده شده‌اند، به فرمت استاندارد آکادمیک ارائه کن : [history] » "
            ];

            $output = '';
            $pre_output = '';
            $total_input = 0;

//            foreach ($prompts as $key => $value) {
            for ($i = 0; $i < 7; $i++) {
                $prompt = str_replace('history', $pre_output, $prompts[$i]);
                $total_input += countWords($prompt);
                $result = $client->chat()->createStreamed([
                    'model' => 'chatgpt-4o-latest',
                    'messages' => [
                        ["role" => "system", "content" => "You are an expert academic writer. follow a structured academic style."],
                        ['role' => 'user', 'content' => $prompt]
                    ],
                ]);

                foreach ($result as $response) {
                    if (isset($response->choices[0]->delta->content)) {
                        $message = str_replace("\n", ' <br> ', $response->choices[0]->delta->content);
                        $output = $output . $message;
                        echo "event: data\n";
                        echo 'data: ' . json_encode(['message' => $message]) . "\n\n";
                        ob_flush();
                        flush();
                    }
                }
                $pre_output .= $output;
            }

            // ذخیره‌ی خروجی و بستن استریم
            $this->updatePostData($post, $output, $total_input);

            echo "event: stop\n";
            echo 'data: ' . json_encode(['post' => $post]) . "\n\n";
            ob_flush();
            flush();
        }, 200, [
            'Content-Type' => 'text/event-stream',
            'Cache-Control' => 'no-cache',
            'Connection' => 'keep-alive',
        ]);
    }

// 📌 ذخیره خروجی و مدیریت کیف پول
    private function updatePostData($post, $output, $total_input): void
    {
        $wordCount = countWords($output);
        $word_rate = ServiceRate::query()->where('service_name', 'gpt-word')->first();
        $cost = $wordCount * $word_rate->rate_per_unit;
        $input_cost = $total_input * 0.5;

        if ($cost + $input_cost >= auth()->user()->wallet_balance) {
            auth()->user()->wallet_balance = 0;
        } else {
            auth()->user()->wallet_balance -= ($cost + $input_cost);
        }
        auth()->user()->save();

        $post->output .= $output;
        $post->credits += $wordCount;
        $post->save();

        totalUsage($wordCount);
    }

    private function videoGenerator($request, $tool): false|\Illuminate\Http\JsonResponse
    {
        try {
            $lockKey = 'generate_video_lock';

            // Attempt to acquire lock
            if (!Cache::lock($lockKey, 10)->get()) {
                // Failed to acquire lock, another process is already running
                return response()->json(['message' => __("We're creating the video, please be patient.")], 409);
            }

            $uploadedFile = $request->file('file');
            $fileName = 'video_image-' . time() . '-' . $uploadedFile->getClientOriginalName();

            Storage::disk('liara')->put($fileName, file_get_contents($uploadedFile->getRealPath()));
            $imageUrl = Storage::disk('liara')->url($fileName);

            $prompt = $request->description;
            $params = [
                'promptImage' => $imageUrl,
                'promptText' => $prompt,
                'duration' => $request->duration,
                'ratio' => $request->size,
                'model' => 'gen3a_turbo',
                'watermark' => false,
            ];

            // Prepare API request
            $baseUri = "https://api.dev.runwayml.com/v1/image_to_video";
            $apiToken = env('RUNWAY_TOKEN');

            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $apiToken,
                'Content-Type' => 'application/json',
                'X-Runway-Version' => '2024-11-06'
            ])->post($baseUri, $params);

            $user = \auth()->user()->id;

            Storage::disk('local')->append('runway.txt', "userId : $user - response is : $response");

            $response = json_decode($response);
            // Handle API response
            if ($response->id) {
                $input = ['fileName' => $fileName, 'prompt' => $prompt, 'duration' => $request->duration];

                // Create a record in the UserTool table
                UserTool::query()->create([
                    'title' => __($tool->name),
                    'tool_id' => $tool->id,
                    'user_id' => Auth::id(),
                    'input' => json_encode($input),
                    'input_translate' => $prompt,
                    'output' => $response->id,
                    'type' => 'video',
                    'credits' => 1,
                    'path' => 'liara',
                ]);

                // Release the lock
                Cache::lock($lockKey)->release();

                return response()->json(['type' => 'video', 'video_id' => $response->id]);
            } else {
                errorLog($response->body(), 'AiToolController, videoGenerator function');
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
            }
        } catch (Exception $e) {
            errorLog($e, 'AiToolController , videoGenerator function :');
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    private function splitCodeAndDescription($text): array
    {
        // Split content into code blocks and descriptions using regex
        $pattern = '/(```.*?```)/s'; // Regex for code block detection (anything between triple backticks)
        $chunks = [];

        // Split content into parts based on the presence of code blocks
        $parts = preg_split($pattern, $text, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        foreach ($parts as $part) {
            if (preg_match('/^```(.*)```$/s', $part)) {
                // It's a code block, so mark it as code
                $chunks[] = [
                    'type' => 'code',
                    'content' => trim($part)  // Preserve newlines within the code
                ];
            } else {
                // It's a description (non-code)
                $chunks[] = [
                    'type' => 'description',
                    'content' => nl2br(trim($part))  // Preserve newlines in descriptions
                ];
            }
        }

        return $chunks;
    }

    public function videoCheck(Request $request)
    {
        try {
            $apiToken = env('RUNWAY_TOKEN');
            $response = Http::withHeaders([
                'Authorization' => "Bearer $apiToken",
                'X-Runway-Version' => '2024-11-06',
            ])->get("https://api.dev.runwayml.com/v1/tasks/$request->id");

            $data = $response->json();

            if ($data['status'] === 'SUCCEEDED') {
                $videoUrl = $response['output'][0];
                $videoContent = Http::get($videoUrl)->body();
                $fileName = 'hosheman_' . time() . '.mp4';
                $path = 'videos/' . $fileName;
                Storage::disk('liara')->put($path, $videoContent);

                $post = UserTool::query()->where('output', $request->id)->first();
                $post->output = $fileName;
                $post->save();

                // Deduct credits from user's wallet
                $input = json_decode($post->input);
                $videoRate = ServiceRate::query()->where('service_name', "$input->duration-seconds-video")->first();
                if ($videoRate->rate_per_unit >= \auth()->user()->wallet_balance)
                    \auth()->user()->wallet_balance = 0;
                else
                    \auth()->user()->wallet_balance -= $videoRate->rate_per_unit;
                \auth()->user()->save();

                totalUsage(1, 'total_videos');

                return response()->json(['status' => 200, 'message' => __("Your video has been successfully created."), 'post' => $post]);
            }
        } catch (\Exception $exception) {
            errorLog($exception, 'UserController , videoCheck function : ');

        }
    }

}
