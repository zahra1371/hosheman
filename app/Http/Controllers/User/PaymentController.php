<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        $amount = (int)str_replace(',', '', request('amount'));
        $request->merge(['amount'=>$amount]);
        $validation = Validator::make($request->all(), [
            'amount' => ['required', 'numeric', 'min:50000'],
        ]);

        if ($validation->fails())
            return redirect()->back()->withErrors($validation->errors())->withInput();

        switch ($request->gateway){
            case 'zibal':
                return ZibalController::payment($request->amount);

        }
    }
}
