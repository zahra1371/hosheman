<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\DiscountCode;
use App\Models\DiscountRedemption;
use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class ZibalController extends Controller
{
    public static function payment($amount)
    {
        $gateway = 'zibal';
        $amount = (int)str_replace(',', '', $amount);
        $main_amount = $amount;
        $amount = $amount + ($amount * 10 / 100);

        try {
            $invoice = (new Invoice)->amount($amount);
            return Payment::via($gateway)->purchase($invoice, function ($driver, $transactionId) use ($invoice, $amount, $main_amount, $gateway) {
                $payment = new WalletTransaction();
                $payment->user_id = auth()->user()->id;
                $payment->transaction_id = (string)$transactionId;
                $payment->amount = $main_amount;
                $payment->status = 'Waiting';
                $payment->save();
            })->pay()->render();
        } catch (\Exception $exception) {
            errorLog($exception);
            return redirect()->back()->with('error', __("An error occurred while connecting to the payment gateway."));
        }
    }

    public function paymentSuccess(Request $request)
    {
        if ($request->has('success') && !$request->success) {
            return redirect()->route('wallet.charge')->with(
                'error', __("Your payment was not successful. If the money has been deducted from your account and is not refunded within 48 hours, please contact support."),
            );
        } else {
            $transaction_id = ($request->has('success') && $request->success) ? $request->trackId : null;

            try {
                $transaction = WalletTransaction::query()->where('transaction_id', $transaction_id)->firstOrFail();
                $user = Auth::user();
                $receipt = Payment::via('zibal')->amount((int)$transaction->amount)->transactionId($transaction_id)->verify();

                $transaction->status = 'Success';
                $transaction->save();

                $discount = DiscountCode::query()->where('is_public', true)->where('expires_at', '>', now())->first();
                $userUsed = null;
                if ($discount)
                    $userUsed = DiscountRedemption::query()->where('user_id', auth()->id())->where('discount_code_id', $discount->id)->first();
                if ($discount && !$userUsed) {
                    $prize = (int)$transaction->amount * $discount->percentage / 100;
                    $user->wallet_balance = $user->wallet_balance + (int)$transaction->amount + $prize;
                    DiscountRedemption::query()->create([
                        'user_id' => $user->id,
                        'discount_code_id' => $discount->id
                    ]);
                } else if ($user->firstDiscount()) {
                    $prize = (int)$transaction->amount * 0.2;
                    $user->wallet_balance = $user->wallet_balance + (int)$transaction->amount + $prize;
                    $user->discount->is_used = true;
                    $user->discount->order_id = $transaction->id;
                    $user->discount->save();
                } else
                    $user->wallet_balance = $user->wallet_balance + (int)$transaction->amount;
                $user->save();


                if ($user->referred_by) {
                    $affiliate_earnings = ($transaction->amount * 10) / 100;
                    $user->referrer->wallet_balance += $affiliate_earnings;
                    $user->referrer->save();
                    WalletTransaction::query()->create([
                        'user_id' => $user->referred_by,
                        'transaction_id' => $transaction_id,
                        'amount' => $affiliate_earnings,
                        'status' => 'Success',
                        'type' => 'referral'
                    ]);
                }

                createActivity($user->id, __('credit increase'), __('increase amount :') . $transaction->amount, null);

                return redirect()->route('user.dashboard')->with('success', __("Your payment was successful."));

            } catch (\Exception $exception) {
                errorLog($exception);
                return redirect()->route('wallet.charge')->with([
                    'message' => __("Your payment was not successful. If the money has been deducted from your account and is not refunded within 48 hours, please contact support."),
                ]);
            }
        }
    }
}
