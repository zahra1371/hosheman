<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\ServiceRate;
use App\Models\Usage;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;


class IndexController extends Controller
{
    public function index(): View
    {
        try {
            $posts = Http::get('https://hosheman.com/blog/wp-json/wp/v2/posts?per_page=3&_fields=id,title,excerpt,link,featured_media')->json();

            foreach ($posts as &$post) {
                if (!empty($post['featured_media'])) {
                    $mediaResponse = Http::get("https://hosheman.com/blog/wp-json/wp/v2/media/{$post['featured_media']}");
                    if ($mediaResponse->successful()) {
                        $post['image_url'] = $mediaResponse->json()['source_url'] ?? null;
                    }
                }
            }

//            $posts=null;
//            if ($response->successful()) {
//                $posts = $response->json();
//            }
            return view('index',compact('posts'));
        }catch (\Exception $exception){
            dd($exception);
        }

    }

    public function about(): View
    {
        $usage=Usage::query()->first();
        return view('about',compact('usage'));
    }

    public function terms(): View
    {
        return view('terms');
    }

    public function pricing(): View
    {
        $prices=ServiceRate::query()->get();
        return view('pricing',compact('prices'));
    }
}
