<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\DiscountCode;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

class AuthenticationController extends Controller
{
    public function loginForm(): View
    {
        return view('auth.login');
    }

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $validation = Validator::make($request->all(), [
            'mobile' => 'required',
        ]);

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);


        try {
            $user = User::query()->where('mobile', $request->mobile)->first();

            if (!$user)
                return response()->json(['status' => 422, 'errors' => ['mobile' => __('No user has registered with this mobile number.')]]);

            if ($user->mobile === '44786596102')
                $token = 639857;
            else
                $token = Token::query()->create([
                    'user_id' => $user->id
                ]);

            if ($user->mobile !== '44786596102') {
                if ($token->sendCode()) {
                    session()->put("code_id", $token->id);
                    session()->put("user_id", $user->id);
                    return response()->json(['status' => 200, 'message' => __('Please enter the SMS code.'), 'url' => '/verify-mobile-code']);
                }
                $token->delete();
                return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
            } else {
                session()->put('safe', true);
                return response()->json(['status' => 200, 'message' => __('Please enter the SMS code.'), 'url' => '/verify-mobile-code']);
            }

        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    public function registerForm(Request $request): View
    {
        $code = $request->code;
        return view('auth.register', compact('code'));
    }

    public function registerStore(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {

        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'regex:/^09[0-9]{9}$/', 'unique:' . User::class],
        ]);

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);

        try {
            $register_code = null;
            if ($request->code != null) {
                $inviter = User::query()->where('referral_code', $request->code)->first();
                $register_code = $inviter?->id;
            }


            $user = User::query()->create([
                'name' => $request->name,
                'surname' => $request->surname,
                'mobile' => convertPersianNumbers($request->mobile),
                'referral_code' => Str::upper(Str::random(19)),
                'referred_by' => $register_code,
            ]);

            $discount = DiscountCode::query()->where('is_public', true)->where('expires_at', '>', now())->first();
            if (!$discount)
                DiscountCode::query()->create([
                    'user_id' => $user->id,
                    'code' => DiscountCode::generateCode(),
                    'percentage' => 20, // مقدار تخفیف (مثلاً ۲۰ درصد)
                    'expires_at' => Carbon::now()->addHours(24), // ۲۴ ساعت اعتبار
                ]);

            totalUsage(1, 'total_users');

            $token = Token::query()->create([
                'user_id' => $user->id
            ]);

            if ($token->sendCode()) {
                session()->put("code_id", $token->id);
                session()->put("user_id", $user->id);
                return response()->json(['status' => 200, 'message' => __('Please enter the SMS code.'), 'url' => '/verify-mobile-code']);
            }
            $token->delete();
            return redirect()->route('login.form')->withErrors([
                __('There is an error in sending the SMS. Please try again later.')
            ]);
        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'errors' => __('An issue has occurred. Please try again later.')]);
        }
    }

    public function verifyForm(): View
    {
        if (session()->has('safe') && session()->get('safe')) {
            $token_time = 120;
            $mobile = '44786596102';
        } else {
            $token = Token::query()->where('user_id', session()->get('user_id'))->find(session()->get('code_id'));
            if ($token) {
                $token_time = $token->isExpired() ? false : Carbon::now()->diffInSeconds($token->created_at);
                $mobile = $token->user->mobile;
            }
        }
        return view('auth.verify_mobile_code', compact('token_time', 'mobile'));


    }

    public function verifyMobileCode(Request $request): \Illuminate\Http\JsonResponse
    {
        $digits = array_filter($request->digits, static function ($var) {
            return $var !== null;
        });

        $request->merge(['digits' => $digits]);

        $validation = Validator::make($request->all(), [
            'digits' => 'present|array|size:6'
        ]);

        if ($validation->fails())
            return response()->json(['status' => 422, 'errors' => $validation->errors()]);

        try {
            $code = implode('', $request->digits);

            if (session()->has('safe') && session()->get('safe')) {
                if ((int)$code === 639857) {
                    $user = User::query()->where('mobile', '44786596102')->first();
                    auth()->login($user);
                    $request->session()->regenerate();

                    $url = session('url.intended') ?? route('user.dashboard');
                    return response()->json(['status' => 200, 'message' => __('You have successfully logged in.'), 'url' => $url]);
                } else {
                    return response()->json(['status' => 422, 'errors' => ['digits' => __('The code is incorrect.')]]);
                }
            } else {
                if (!session()->has('code_id') || !session()->has('user_id'))
                    response()->json(['status' => 300, 'url' => 'login']);

                $token = Token::query()->where('user_id', session()->get('user_id'))->find(session()->get('code_id'));

                if (!$token || empty($token->id))
                    return response()->json(['status' => 422, 'errors' => ['digits' => __('The code does not exist.')]]);

                if (!$token->isValid())
                    return response()->json(['status' => 422, 'errors' => ['digits' => __('The code has expired.')]]);

                if ($token->code !== $code)
                    return response()->json(['status' => 422, 'errors' => ['digits' => __('The code is incorrect.')]]);

                $token->used = true;
                $token->save();

                $user = User::query()->find(session()->get('user_id'));
                $rememberMe = session()->get('remember');
                auth()->login($user, $rememberMe);
                $request->session()->regenerate();

                $select_url=$user->has_seen_popup?route('chatbot','hosheman'):route('user.dashboard');
                $url = session('url.intended') ?? $select_url;
                return response()->json(['status' => 200, 'message' => __('You have successfully logged in.'), 'url' => $url]);
            }

        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'message' => __('An issue has occurred. Please try again later.')]);
        }
    }

    public function resendCode(): \Illuminate\Http\JsonResponse
    {
        try {
            $token = Token::query()->create([
                'user_id' => session()->get('user_id')
            ]);

            if ($token->sendCode()) {
                session()->put("code_id", $token->id);
                session()->put("user_id", session()->get('user_id'));
                return response()->json(['status' => 200, 'message' => __('The code has been sent to you.'), 'reload' => true]);
            } else {
                return response()->json(['status' => 500, 'message' => __('An issue has occurred. Please try again later.')]);
            }
        } catch (\Exception $exception) {
            errorLog($exception);
            return response()->json(['status' => 500, 'message' => __('An issue has occurred. Please try again later.')]);
        }
    }

    public function logout(Request $request): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
    {
        try {
            Auth::logout(); // خروج کاربر از سیستم

            // حذف کوکی remember_me که باعث باقی ماندن لاگین می‌شود
            Cookie::queue(Cookie::forget('remember_web_'.Auth::getDefaultDriver()));
            $request->session()->invalidate(); // حذف سشن کاربر
            $request->session()->regenerateToken(); // جلوگیری از حملات CSRF

            return redirect('/login')->with('status', 'شما با موفقیت خارج شدید.');
        }catch (\Exception $exception){
            errorLog($exception);
            return redirect()->back();
        }

    }

}
