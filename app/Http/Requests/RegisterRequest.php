<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'mobile' => ['required','regex:/^09[0-9]{9}$/', 'unique:' . User::class],
        ];
    }

    public function messages(): array
    {
        return [
            'mobile.required' => 'موبایل الزامی است.',
            'mobile.regex' => 'موبایل باید 11 رقمی و با 09 شروع شود.',
        ];
    }
}
