<?php

use App\Http\Controllers\Auth\AuthenticationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

Route::middleware('web')->group(function () {
    // register routes
    Route::get('register', [AuthenticationController::class, 'registerForm'])
        ->name('register.form');
    Route::post('register', [AuthenticationController::class, 'registerStore'])
        ->name('register');

    // login routes
    Route::get('login', [AuthenticationController::class, 'loginForm'])
        ->name('login.form');
    Route::post('login', [AuthenticationController::class, 'login'])
        ->name('login');

    // verify mobile code routes
    Route::get('verify-mobile-code', [AuthenticationController::class, 'verifyForm'])
        ->name('verify.form');
    Route::post('verify-mobile-code', [AuthenticationController::class, 'verifyMobileCode'])
        ->name('verify');

    // resend verify code routes
    Route::get('resend-code', [AuthenticationController::class, 'resendCode'])
        ->name('resend-code');
});

Route::middleware('web')->group(function () {
    Route::post('logout', [AuthenticationController::class, 'logout'])->name('logout');

});
