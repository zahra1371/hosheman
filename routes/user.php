<?php

use App\Http\Controllers\User\AiToolController;
use App\Http\Controllers\User\ChatBotController;
use App\Http\Controllers\User\PaymentController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\ZibalController;
use Illuminate\Support\Facades\Route;

Route::prefix('user')->middleware(['web','auth'])->group(function(){
    Route::get('dashboard',[UserController::class,'dashboard'])->name('user.dashboard');

    Route::get('wallet/charge',[UserController::class,'walletChargeForm'])->name('wallet.charge');
    Route::post('wallet/charge',[PaymentController::class,'payment'])->name('wallet.payment');
    Route::get('verify-payment', [ZibalController::class, 'paymentSuccess']);

    Route::get('chat-bots',[ChatBotController::class,'chatBots'])->name('chatbots.list');
    Route::get('chat-bot/{slug}',[ChatBotController::class,'chatBot'])->name('chatbot');
    Route::get('chat-bot/new-chat/{id}',[ChatBotController::class,'startNewChat']);
    Route::post('chat/send-message',[ChatBotController::class,'sendChat']);
    Route::get('chat/send-message',[ChatBotController::class,'sendChat']);
    Route::get('chat/get-chat',[ChatBotController::class,'getChat']);
    Route::get('chat/delete/{id}',[ChatBotController::class,'contentDelete']);


    Route::get('ai-tools',[AiToolController::class,'aiTools'])->name('aitools.list');
    Route::get('ai-tools/{slug}',[AiToolController::class,'toolList'])->name('aitools');

    Route::post('content/generate',[AiToolController::class,'generate'])->name('aitools.generate');
    Route::get('content/generate',[AiToolController::class,'generate']);
    Route::post('content/quality',[AiToolController::class,'contentQuality']);
    Route::get('content/download/{id}',[AiToolController::class,'contentDownload'])->name('content.download');
    Route::get('content/delete/{id}',[AiToolController::class,'contentDelete']);
    Route::post('video-check',[AiToolController::class,'videoCheck'])->name('video.check');


    Route::get('documents',[UserController::class,'documents'])->name('documents');
    Route::get('document/view/{id}',[UserController::class,'viewDocument'])->name('document.view');
    Route::post('document/update',[UserController::class,'updateDocument'])->name('document.update');

    Route::get('change-first-login',[UserController::class,'changeFirstLogin']);
    Route::get('profile',[UserController::class,'profile'])->name('profile');
    Route::post('profile/update',[UserController::class,'updateProfile'])->name('profile.update');

});
