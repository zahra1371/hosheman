<?php

use App\Http\Controllers\Landing\IndexController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;


Route::middleware('web')->group(function () {
    Route::get('/', [IndexController::class, 'index'])->name('home');

    Route::get('/about-us', [IndexController::class, 'about'])->name('about');

    Route::get('/term-of-use', [IndexController::class, 'terms'])->name('terms');

    Route::get('/pricing', [IndexController::class, 'pricing'])->name('pricing');

    Route::get('/blog', function () {
        return redirect('https://hosheman.com/blog');
    })->name('blog');

    Route::get('language/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);

        return redirect()->back();
    })->name('language');
});

