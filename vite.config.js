import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/jquery.js',
                'resources/css/app.css',
                'resources/js/app.js',
                'resources/js/bundle.js',
                'resources/js/forms.js',
                'resources/js/sweetalert2.js',
            ],
            refresh: true,
        }),
    ],
});
