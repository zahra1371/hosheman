<?php

namespace Database\Seeders;

use App\Models\ToolCategory;
use Illuminate\Database\Seeder;

class ToolCategorySeeder extends Seeder
{
    public function run():void{
        ToolCategory::query()->create([
            'name'=>'instagram',
            'fa_name'=>'اینستاگرام',
            'description'=>'تولید کپشن، هشتگ، سناریو استوری، سناریو ریلز،قلاب برای کاور و ...'
        ]);

        ToolCategory::query()->create([
            'name'=>'website',
            'fa_name'=>'وبسایت',
            'description'=>'ابزار تولید مقالات وبلاگ، توضیحات متا و ...',
        ]);

        ToolCategory::query()->create([
            'name'=>'youtube',
            'fa_name'=>'یوتیوب',
            'description'=>'ابزار تولید کپشن ویدئو، عنوان ویدئو و ...',
        ]);

        ToolCategory::query()->create([
            'name'=>'general',
            'fa_name'=>'عمومی',
            'description'=>'ابزار مناسب برای دانش آموزان، معلمان و ...',
        ]);

        ToolCategory::query()->create([
            'name'=>'development',
            'fa_name'=>'توسعه دهندگان',
            'description'=>'ابزار مناسب برای برنامه نویسان و توسعه دهندگان',
        ]);

        ToolCategory::query()->create([
            'name'=>'voiceover',
            'fa_name'=>'صداگذاری',
            'description'=>'ابزار مناسب تبدیل گفتار به متن و بالعکس',
        ]);

        ToolCategory::query()->create([
            'name'=>'graphics',
            'fa_name'=>'گرافیک',
            'description'=>'ابزار مناسب ساخت عکس، ویدئو و ویرایش تصاویر',
        ]);

        ToolCategory::query()->create([
            'name'=>'file',
            'fa_name'=>'فایل',
            'description'=>'این ابزار از طریق یک فایل بهت اطلاعات میده',
        ]);
    }
}
