<?php

namespace Database\Seeders;

use App\Models\Tool;
use App\Models\ToolCategory;
use Illuminate\Database\Seeder;

class ToolSeeder extends Seeder
{
    public function run(): void
    {
        $instagram = ToolCategory::query()->where('name', 'instagram')->first();
        $website = ToolCategory::query()->where('name', 'website')->first();
        $youtube = ToolCategory::query()->where('name', 'youtube')->first();
        $general = ToolCategory::query()->where('name', 'general')->first();
        $development = ToolCategory::query()->where('name', 'development')->first();
        $voiceover = ToolCategory::query()->where('name', 'voiceover')->first();
        $graphics = ToolCategory::query()->where('name', 'graphics')->first();
        $file = ToolCategory::query()->where('name', 'file')->first();

        Tool::query()->create([
            'name' => 'Youtube Video Description',
            'slug'=>'youtube_video_description',
            'type' => 'text',
            'is_active' => true,
            'description' => 'Elevate your YouTube content with compelling video descriptions. Generate engaging descriptions effortlessly and increase views.',
            'image' => 'youtube-video-description',
            'category_id' => $youtube->id,
        ]);

        Tool::query()->create([
            'name' => 'AI YouTube',
            'type' => 'youtube',
            'slug'=>'youtube_video',
            'is_active' => false,
            'description' => 'Simply turn your Youtube videos into Blog post.',
            'image' => 'ai-youtube',
            'category_id' => $youtube->id
        ]);

        Tool::query()->create([
            'name' => 'Youtube Video Title',
            'slug'=>'youtube_video_title',
            'type' => 'text',
            'is_active' => true,
            'description' => 'Get more views with attention-grabbing video titles. Create unique, catchy titles that entice viewers.',
            'image' => 'youtube-video-title',
            'category_id' => $youtube->id
        ]);

        Tool::query()->create([
            'name' => 'Youtube Video Tag',
            'slug'=>'youtube_video_tag',
            'type' => 'text',
            'is_active' => true,
            'description' => "Improve your YouTube video's discoverability with relevant video tags. Boost views and engagement.",
            'image' => 'youtube-video-tag',
            'category_id' => $youtube->id
        ]);

        Tool::query()->create([
            'name' => 'Newsletter Generator',
            'slug'=>'newsletter_generator',
            'type' => 'text',
            'is_active' => false,
            'description' => 'Generate engaging newsletters easily with personalized content that resonates with your audience, driving growth and engagement.',
            'image' => 'newsletter-generator',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Meta Description',
            'slug'=>'meta_description',
            'type' => 'text',
            'is_active' => false,
            'description' => 'Get more clicks with compelling meta descriptions. Generate unique, SEO-friendly meta descriptions that attract customers and boost traffic.',
            'image' => 'meta-description',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Post Title Generator',
            'slug'=>'post_title_generator',
            'type' => 'text',
            'is_active' => false,
            'description' => 'Get captivating post titles instantly with our title generator. Boost engagement and save time.',
            'image' => 'post-title-generator',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Blog Section',
            'slug'=>'blog_section',
            'type' => 'text',
            'is_active' => true,
            'description' => 'Effortlessly create blog sections with AI. Get unique, engaging content and save time.',
            'image' => 'blog-section',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Blog Post Ideas',
            'slug'=>'blog_post_ideas',
            'type' => 'text',
            'is_active' => true,
            'description' => 'Unlock your creativity with unique blog post ideas. Generate endless inspiration and take your content to the next level.',
            'image' => 'blog-post-ideas',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Blog Intros',
            'slug'=>'blog_intros',
            'type' => 'text',
            'is_active' => true,
            'description' => "Set the tone for your blog post with captivating intros. Grab readers' attention and keep them engaged.",
            'image' => 'blog-intros',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Blog Conclusion',
            'slug'=>'blog_conclusion',
            'type' => 'text',
            'is_active' => true,
            'description' => "End your blog posts on a high note. Craft memorable conclusions that leave a lasting impact.",
            'image' => 'blog-conclusion',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'Paragraph Generator',
            'slug'=>'paragraph_generator',
            'type' => 'text',
            'is_active' => false,
            'description' => "Generate a paragraph with keywords and description. Never struggle with writer's block again. Generate flawless paragraphs that captivate readers.",
            'image' => 'paragraph-generator',
            'category_id' => $website->id
        ]);

        Tool::query()->create([
            'name' => 'AI Speech to Text',
            'slug'=>'ai_speech_to_text',
            'type' => 'audio',
            'is_active' => true,
            'description' => "The AI app that turns audio speech into text with ease.",
            'image' => 'ai-speech-to-text',
            'category_id' => $voiceover->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram Captions',
            'slug'=>'instagram_captions',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your Instagram game with captivating captions. Generate unique captions that engage followers and increase your reach.",
            'image' => 'instagram-captions',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram Hashtags',
            'slug'=>'instagram_hashtags',
            'type' => 'text',
            'is_active' => true,
            'description' => "Boost your Instagram reach with relevant hashtags. Generate optimal, trending hashtags and increase your visibility.",
            'image' => 'instagram-hashtags',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram Story',
            'slug'=>'instagram_story',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your Instagram game with captivating captions. Generate unique captions that engage followers and increase your reach.",
            'image' => 'instagram-story',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram Reel',
            'slug'=>'instagram_reel',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your Instagram game with captivating captions. Generate unique captions that engage followers and increase your reach.",
            'image' => 'instagram-reel',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram Calendar',
            'slug'=>'instagram_calendar',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your Instagram game with captivating captions. Generate unique captions that engage followers and increase your reach.",
            'image' => 'instagram-calendar',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'Instagram campaign',
            'slug'=>'instagram_campaign',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your Instagram game with captivating captions. Generate unique captions that engage followers and increase your reach.",
            'image' => 'instagram-campaign',
            'category_id' => $instagram->id
        ]);

        Tool::query()->create([
            'name' => 'AI Code Generator',
            'slug'=>'ai_code_generator',
            'type' => 'code',
            'is_active' => true,
            'description' => "Create custom code in seconds! Leverage our state-of-the-art AI technology to quickly and easily generate code in any language.",
            'image' => 'ai-code-generator',
            'category_id' => $development->id
        ]);

        Tool::query()->create([
            'name' => 'AI Vision',
            'slug'=>'ai_vision',
            'type' => 'text',
            'is_active' => true,
            'description' => "Elevate your visual analytics with our AI Vision platform. Harness the power of machine learning for real-time image recognition and data insights. Enhance efficiency and decision-making.",
            'image' => 'ai-vision',
            'category_id' => $file->id
        ]);

        Tool::query()->create([
            'name' => 'File Analyzer',
            'slug'=>'file_analyzer',
            'type' => 'text',
            'is_active' => true,
            'description' => "Simply upload a file (PDF, CSV, .doc or .docx) and extract key insights or summarize the entire document.",
            'image' => 'file-analyzer',
            'category_id' => $file->id
        ]);

        Tool::query()->create([
            'name' => 'AI Article Wizard Generator',
            'slug'=>'ai_article_wizard',
            'type' => 'text',
            'is_active' => true,
            'description' => "Create custom article instantly with our article wizard generator. Boost engagement and save time.",
            'image' => 'ai-article-wizard',
            'category_id' => $general->id
        ]);

        Tool::query()->create([
            'name' => 'Article Generator',
            'slug'=>'article_generator',
            'type' => 'text',
            'is_active' => true,
            'description' => "Instantly create unique articles on any topic. Boost engagement, improve SEO, and save time.",
            'image' => 'article-generator',
            'category_id' => $general->id
        ]);

        Tool::query()->create([
            'name' => 'Summarize Text',
            'slug'=>'summarize_text',
            'type' => 'text',
            'is_active' => true,
            'description' => "Effortlessly condense large text into shorter summaries. Save time and increase productivity.",
            'image' => 'summarize-text',
            'category_id' => $general->id
        ]);

        Tool::query()->create([
            'name' => 'AI Image Generator',
            'slug'=>'ai_image_generator',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Generate wonderful images with AI.',
            'image' => 'ai-image-generator',
            'category_id' => $graphics->id
        ]);

        Tool::query()->create([
            'name' => 'AI Delete Background',
            'slug'=>'ai_delete_background',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Removes the background while preserving foreground',
            'image' => 'ai-delete-background',
            'category_id' => $graphics->id
        ]);

        Tool::query()->create([
            'name' => 'AI Generate Background',
            'slug'=>'ai_generate_background',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Generate amazing backgrounds for your images.',
            'image' => 'ai-generate-background',
            'category_id' => $graphics->id
        ]);

        Tool::query()->create([
            'name' => 'AI Sketch',
            'slug'=>'ai_sketch',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Use a sketch or line art to guide generation.',
            'image' => 'ai-sketch',
            'category_id' => $graphics->id
        ]);

        Tool::query()->create([
            'name' => 'AI Outpaint',
            'slug'=>'ai_outpaint',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Inserts additional content in an image to fill in the space in any direction.',
            'image' => 'ai-outpaint',
            'category_id' => $graphics->id
        ]);

        Tool::query()->create([
            'name' => 'AI Recolor',
            'slug'=>'ai_recolor',
            'type' => 'image',
            'is_active' => '1',
            'description' => 'Use simple words to change the color of an object.',
            'image' => 'ai-recolor',
            'category_id' => $graphics->id
        ]);
    }
}
