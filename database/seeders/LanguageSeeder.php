<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\Tool;
use App\Models\ToolCategory;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    public function run(): void
    {
        Language::query()->create([
            'name' => 'Persian',
            'fa_name' => 'فارسی'
        ]);

        Language::query()->create([
            'name' => 'Arabic',
            'fa_name'=>'عربی'
        ]);

        Language::query()->create([
            'name' => 'Chinese',
            'fa_name' => 'چینی'
        ]);

        Language::query()->create([
            'name' => 'English',
            'fa_name' => 'انگلیسی'
        ]);

        Language::query()->create([
            'name' => 'French',
            'fa_name'=>'فرانسوی'
        ]);

        Language::query()->create([
            'name' => 'German',
            'fa_name' => 'آلمانی'
        ]);

        Language::query()->create([
            'name' => 'Greek',
            'fa_name'=>'یونانی'
        ]);

        Language::query()->create([
            'name' => 'Hindi',
            'fa_name'=>'هندی'
        ]);

        Language::query()->create([
            'name' => 'Hungarian',
            'fa_name'=>'مجارستانی'
        ]);

        Language::query()->create([
            'name' => 'Italian',
            'fa_name'=>'ایتالیایی'
        ]);

        Language::query()->create([
            'name' => 'Japanese',
            'fa_name'=>'ژاپنی'
        ]);

        Language::query()->create([
            'name' => 'Korean',
            'fa_name'=>'کره ای'
        ]);

        Language::query()->create([
            'name' => 'Norwegian',
            'fa_name'=>'نروژی'
        ]);

        Language::query()->create([
            'name' => 'Portuguese',
            'fa_name'=>'پرتغالی'
        ]);

        Language::query()->create([
            'name' => 'Romanian',
            'fa_name'=>'رومانیایی'
        ]);

        Language::query()->create([
            'name' => 'Russian',
            'fa_name'=>'روسی'
        ]);

        Language::query()->create([
            'name' => 'Spanish',
            'fa_name'=>'اسپانیایی'
        ]);

        Language::query()->create([
            'name' => 'Turkish',
            'fa_name'=>'ترکی'
        ]);
    }
}
