<?php

namespace Database\Seeders;

use App\Models\ServiceRate;
use Illuminate\Database\Seeder;

class ServiceRateSeeder extends Seeder
{
    public function run(): void
    {
        ServiceRate::query()->create([
            'service_name'=>'gpt-word',
            'rate_per_unit'=>4,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'dall-e-3',
            'rate_per_unit'=>12500,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'real_image',
            'rate_per_unit'=>10500,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_delete_background',
            'rate_per_unit'=>3200,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_generate_background',
            'rate_per_unit'=>22000,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_sketch',
            'rate_per_unit'=>4700,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_outpaint',
            'rate_per_unit'=>6250,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_recolor',
            'rate_per_unit'=>7800,
        ]);

        ServiceRate::query()->create([
            'service_name'=>'ai_speech_to_text',
            'rate_per_unit'=>7,
        ]);
    }
}
