<?php

namespace Database\Seeders;

use App\Models\AiModel;
use Illuminate\Database\Seeder;

class AiModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AiModel::query()->create([
            'name'=>'default',
            'slug'=>'hosheman',
            'image'=>'hosheman',
            'description'=>'چت با هوش من',
            'first_message'=>'سلام! من هوش مصنوعی دستیار شما هستم. آماده‌ام تا به شما در هر موضوعی کمک کنم؛ از نوشتن و ترجمه تا تولید محتوا و برنامه‌ریزی. چطور می‌توانم کمک کنم؟',
        ]);

        AiModel::query()->create([
            'name'=>'Finance Expert',
            'slug'=>'finance-expert',
            'image'=>'finance',
            'description'=>'چت کارشناس امور مالی',
            'first_message'=>'سلام! من هوش مصنوعی کارشناس امور مالی شما هستم. می‌توانم به شما در مدیریت بودجه، برنامه‌ریزی سرمایه‌گذاری، تحلیل هزینه‌ها، و پاسخ به سوالات مالی کمک کنم. از کجا شروع کنیم؟',
        ]);

        AiModel::query()->create([
            'name'=>'Nutritionist',
            'slug'=>'nutritionist',
            'image'=>'nutritionist',
            'description'=>'چت با کارشناس تغذیه',
            'first_message'=>'سلام! من هوش مصنوعی کارشناس تغذیه شما هستم. اینجا هستم تا به شما در طراحی برنامه غذایی، راهنمایی در انتخاب مواد غذایی سالم و دستیابی به اهداف تغذیه‌ای کمک کنم. هدفتون چیه؟ کاهش وزن، افزایش انرژی، یا فقط تغذیه بهتر؟',
        ]);

        AiModel::query()->create([
            'name'=>'Career Counselor',
            'slug'=>'career-counselor',
            'image'=>'career-counselor',
            'description'=>'چت با مشاور شغلی',
            'first_message'=>'سلام! من هوش مصنوعی مشاور شغلی شما هستم. می‌توانم در یافتن شغل مناسب، بهبود رزومه، آمادگی برای مصاحبه و برنامه‌ریزی مسیر شغلی کمک کنم. هدفتون چیه؟ تغییر شغل، ارتقای مهارت، یا پیدا کردن مسیر شغلی جدید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Time Management Consultant',
            'slug'=>'time-management-consultant',
            'image'=>'time-management-consultant',
            'description'=>'چت با مشاور مدیریت زمان',
            'first_message'=>'سلام! من هوش مصنوعی مشاور مدیریت زمان شما هستم. اینجا هستم تا به شما در برنامه‌ریزی روزانه، اولویت‌بندی کارها، و بهبود بهره‌وری کمک کنم. آیا می‌خواهید یک برنامه موثر تنظیم کنیم یا روی بهینه‌سازی زمانتان کار کنیم؟',
        ]);

        AiModel::query()->create([
            'name'=>'Language Tutor',
            'slug'=>'language-tutor',
            'image'=>'english',
            'description'=>'چت با مدرس زبان',
            'first_message'=>'سلام! من هوش مصنوعی مربی زبان شما هستم. اینجا هستم تا به شما در یادگیری و تمرین زبان کمک کنم. چه چیزی می‌خواهید یاد بگیرید یا تمرین کنید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Content Creator',
            'slug'=>'content-creator',
            'image'=>'content-creator',
            'description'=>'چت با مشاور تولید محتوا',
            'first_message'=>'سلام! من هوش مصنوعی دستیار تولید محتوای شما هستم. آیا به کمک برای نوشتن متن، طراحی کپشن، یا ایجاد ایده‌های جدید نیاز دارید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Interior Designer',
            'slug'=>'interior-designer',
            'image'=>'interior-designer',
            'description'=>'چت با طراح داخلی',
            'first_message'=>'سلام! من هوش مصنوعی دستیار طراحی داخلی شما هستم. آیا به کمک برای مشاوره طراحی، انتخاب رنگ‌ها، یا ایده‌های خلاقانه برای فضای خود نیاز دارید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Parenting Coach',
            'slug'=>'parenting-coach',
            'image'=>'coach',
            'description'=>'چت با متخصص کوچینگ',
            'first_message'=>'سلام! من هوش مصنوعی دستیار متخصص کوچینگ شما هستم. آیا به کمک برای رسیدن به اهداف شخصی، توسعه مهارت‌ها، یا مشاوره در مورد مسیر شغلی و زندگی نیاز دارید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Fitness Trainer',
            'slug'=>'fitness-trainer',
            'image'=>'fitness-trainer',
            'description'=>'چت با مربی تناسب اندام',
            'first_message'=>'سلام! من هوش مصنوعی مربی تناسب اندام شما هستم. می‌توانم در برنامه‌ریزی ورزشی و راهنمایی‌های تغذیه به شما کمک کنم. امروز می‌خواهید روی چه چیزی کار کنید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Travel Advisor',
            'slug'=>'travel-advisor',
            'image'=>'travel-advisor',
            'description'=>'چت با تور لیدر',
            'first_message'=>'سلام! من هوش مصنوعی دستیار تور لیدر شما هستم. آیا به کمک برای برنامه‌ریزی سفر، انتخاب مقاصد گردشگری یا دریافت اطلاعات درباره جاذبه‌های دیدنی نیاز دارید؟',
        ]);

        AiModel::query()->create([
            'name'=>'Event Planner',
            'slug'=>'event-planner',
            'image'=>'event-planner',
            'description'=>'چت با مشاور برنامه ریزی',
            'first_message'=>'سلام! من هوش مصنوعی دستیار مشاور برنامه‌ریزی شما هستم. آیا به کمک برای تنظیم برنامه‌های روزانه، مدیریت زمان یا تعیین اولویت‌ها و اهداف خود نیاز دارید؟',
        ]);
    }
}
