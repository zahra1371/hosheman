<?php

namespace Database\Seeders;

use App\Models\Tool;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChatMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Fetch all data from the old table
        $oldData = DB::table('user_orders')->get();

        foreach ($oldData as $row) {
            // Insert into the new table with selected columns
            DB::table('wallet_transactions')->insert([
                'id' => $row->id,
                'user_id' => $row->user_id,
                'transaction_id' => $row->order_id,
                'type' => 'self',
                'status' => $row->status,
                'amount' => $row->price,
                'description' => null,
                'created_at' => $row->created_at,
                'updated_at' => $row->updated_at,
            ]);
        }


//        $oldData = DB::table('user_openai')->get();
//
//        foreach ($oldData as $row) {
//            $tool = Tool::query()->where('id', $row->openai_id)->first();
//            $path = $tool->type === 'image' ? 'liara' : null;
//            $output = $tool->type === 'image' ? $row->output : $row->response;
//            // Insert into the new table with selected columns
//            DB::table('user_tools')->insert([
//                'id' => $row->id,
//                'title' => __($tool->name),
//                'user_id' => $row->user_id,
//                'tool_id' => $row->openai_id,
//                'input' => $row->input?? 'empty input',
//                'input_translate' => $row->main_input,
//                'output' => $output,
//                'type' => $tool->type,
//                'credits' => $row->credits,
//                'path' => $path,
//                'response_quality' => null,
//                'folder_id' => $row->folder_id,
//                'created_at' => $row->created_at,
//                'updated_at' => $row->updated_at,
//            ]);
//        }


//        $oldData = DB::table('user_openai_chat')->get();
//
//        foreach ($oldData as $row) {
//            // Insert into the new table with selected columns
//            DB::table('user_chat_bots')->insert([
//                'id'           => $row->id,
//                'user_id'         => $row->user_id,
//                'bot_id'         => $row->openai_chat_category_id,
//                'title'         => $row->title,
//                'total_words'         => $row->total_credits,
//                'created_at'   => $row->created_at,
//                'updated_at'   => $row->updated_at,
//            ]);
//        }


//        $oldData = DB::table('old_users')->get();
//
//        foreach ($oldData as $row) {
//            // Insert into the new table with selected columns
//            DB::table('users')->insert([
//                'id'           => $row->id,
//                'name'         => $row->name,
//                'surname'         => $row->surname,
//                'mobile'         => $row->mobile??null,
//                'email'         => $row->email??null,
//                'is_admin'        => false,
//                'has_seen_popup'     => $row->first_login,
//                'wallet_balance'        => 0,
//                'referral_code'      => $row->affiliate_code,
//                'referred_by'      => $row->affiliate_id,
//                'created_at'   => $row->created_at,
//                'updated_at'   => $row->updated_at,
//            ]);
//        }
    }
}
