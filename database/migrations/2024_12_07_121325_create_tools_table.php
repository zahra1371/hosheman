<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tools', function (Blueprint $table) {
            $table->id();
            $table->string('name'); // نام ابزار
            $table->string('slug'); // نام ابزار
            $table->string('type'); // نوع ابزار، مثلاً instagram, voice, image
            $table->boolean('is_active')->default(true);
            $table->text('description')->nullable(); // توضیح ابزار
            $table->text('image')->nullable();
            $table->foreignId('category_id')->constrained('tool_categories')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tools');
    }
};
