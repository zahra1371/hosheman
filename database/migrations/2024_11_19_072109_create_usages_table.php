<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('total_users')->default(0);
            $table->unsignedBigInteger('total_words')->default(0);
            $table->unsignedBigInteger('total_images')->default(0);
            $table->unsignedBigInteger('total_edit_images')->default(0);
            $table->unsignedBigInteger('total_videos')->default(0);
            $table->unsignedBigInteger('total_change_back')->default(0);
            $table->unsignedBigInteger('total_sales')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usages');
    }
};
