<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_tools', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade'); // کاربر استفاده‌کننده
            $table->foreignId('tool_id')->constrained('tools')->onDelete('cascade'); // ابزار استفاده‌شده
            $table->text('input'); // داده‌های ورودی (مثلاً موضوع استوری)
            $table->text('input_translate')->nullable(); // برای عکس ها ممکن نیاز به ترجمه باشه
            $table->text('output')->nullable(); // نتیجه تولیدشده
            $table->text('type')->nullable(); // مدل ساخت متن یا عکس
            $table->string('credits')->default(0); // هزینه این استفاده
            $table->string('path')->nullable(); // مسیر ذخیره عکس
            $table->boolean('response_quality')->nullable();
            $table->foreignId('folder_id')->nullable()->constrained('folders')->onDelete('cascade'); // اسم فولدر دسته بندی کاربر
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tool_usages');
    }
};
