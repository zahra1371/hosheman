<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique(); // کد تخفیف
            $table->tinyInteger('percentage'); // مقدار تخفیف
            $table->boolean('is_used')->default(false); // فقط برای کدهای اختصاصی
            $table->boolean('is_public')->default(false); // آیا عمومی است؟
            $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('cascade'); // برای کدهای اختصاصی
            $table->foreignId('order_id')->nullable()->constrained('wallet_transactions')->onDelete('cascade'); // برای کدهای اختصاصی
            $table->timestamp('expires_at'); // تاریخ انقضا
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('discount_codes');
    }
};
